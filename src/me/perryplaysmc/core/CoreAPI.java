package me.perryplaysmc.core;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import me.perryplaysmc.core.logging.LogType;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.handlers.BaseOfflineUser;
import me.perryplaysmc.user.handlers.BaseUser;
import me.perryplaysmc.user.handlers.RankUtils;
import me.perryplaysmc.utils.helper.PluginHelper;
import me.perryplaysmc.utils.string.TimeUtil;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.helper.ban.BanData;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.world.BlockHelper;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerEvent;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings("all")
public class CoreAPI {
    
    private static Core core;
    private static List<UUID> baltops;
    private static BlockHelper blockHelper;
    
    static {
        baltops = new ArrayList<>();
        blockHelper = new BlockHelper();
    }
    
    protected CoreAPI(Core core) {
        this.core = core;
        core.load();
    }
    
    public static List<String> toStringList(Enum... enums) {
        List<String> ret = new ArrayList<>();
        for(Enum e : enums)
            ret.add(e.name());
        return ret;
    }
    
    public static List<UUID> getBalanceTops() {
        return baltops;
    }
    
    public static void setCore(Core newCore) {
        core = newCore;
    }
    
    public static boolean isSet() {
        return core!=null;
    }
    
    public static Config getMessages() {
        return core.getMessages();
    }
    
    public static Config getSettings() {
        return core.getSettings();
    }
    
    public static Config getCells() {
        return core.getCells();
    }
    
    public static Config getChat() {
        return core.getChat();
    }
    
    public static Config getWorlds() {
        return core.getWorlds();
    }
    
    public static Config getPermissions() {
        return core.getPermissions();
    }
    
    public static Config getLocations() {
        return core.getLocations();
    }
    
    public static Config getBanFile() {
        return core.getBanFile();
    }
    
    public static PluginHelper getPluginHelper() {
        return core.getPluginHelper();
    }
    
    public static BlockHelper getBlockHelper() {
        return blockHelper;
    }
    
    public static CommandSource getCommandSource(String name) {
        if(getUser(name) != null) {
            return getUser(name);
        }
        if(name.equalsIgnoreCase("server") || name.equalsIgnoreCase("console")) {
            return getConsole();
        }
        return null;
    }
    
    public static List<String> createList(int start, int stop) {
        List<String> ret = new ArrayList<>();
        for(int i = start; i < (stop+1); i++) {
            if(!ret.contains(i+""))ret.add(i+"");
        }
        return ret;
    }
    
    public static String findName(Object obj) {
        Class<?> o = obj.getClass();
        Method m = null;
        try {
            m = o.getMethod("getRealName");
        } catch (NoSuchMethodException e) {
            try {
                m = o.getMethod("getName");
            } catch (NoSuchMethodException e1) {
                try {
                    m = o.getMethod("name");
                } catch (NoSuchMethodException e2) {
                    try {
                        m = o.getMethod("getContents");
                    } catch (NoSuchMethodException e3) {
                    }
                }
            }
        }
        try {
            return String.valueOf(m.invoke(obj));
        } catch (Exception e) {
            return obj.toString();
        }
    }
    public static Set<String> findNamesAsSet(Collection<?> objs) {
        List<String> ret = new ArrayList<>();
        for(Object obj : objs) {
            Class<?> o = obj.getClass();
            Method m = null;
            try {
                m = o.getMethod("getRealName");
            } catch (NoSuchMethodException e) {
                try {
                    m = o.getMethod("getName");
                } catch (NoSuchMethodException e1) {
                    try {
                        m = o.getMethod("name");
                    } catch (NoSuchMethodException e2) {
                    }
                }
            }
            if(m!=null) {
                try {
                    if(!ret.contains(String.valueOf(m.invoke(obj))))
                        ret.add(String.valueOf(m.invoke(obj)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new HashSet<>(ret);
    }
    
    public static List<String> findNames(Collection<?> objs) {
        List<String> ret = new ArrayList<>();
        for(Object obj : objs) {
            Class<?> o = obj.getClass();
            Method m = null;
            try {
                m = o.getMethod("getRealName");
            } catch (NoSuchMethodException e) {
                try {
                    m = o.getMethod("getName");
                } catch (NoSuchMethodException e1) {
                    try {
                        m = o.getMethod("name");
                    } catch (NoSuchMethodException e2) {
                    }
                }
            }
            if(m!=null) {
                try {
                    if(!ret.contains(String.valueOf(m.invoke(obj))))
                        ret.add(String.valueOf(m.invoke(obj)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
    
    public static List<String> findNames(Object[] objs) {
        List<String> ret = new ArrayList<>();
        for(Object obj : objs) {
            Class<?> o = obj.getClass();
            Method m = null;
            Field f = null;
            try {
                m = o.getMethod("getRealName");
            } catch (NoSuchMethodException e) {
                try {
                    m = o.getMethod("getName");
                } catch (NoSuchMethodException e1) {
                    try {
                        m = o.getMethod("name");
                    } catch (NoSuchMethodException e2) {
                        try {
                            f = o.getField("name");
                        } catch (NoSuchFieldException e3) {
                        }
                    }
                }
            }
            if(m!=null) {
                try {
                    if(!ret.contains(String.valueOf(m.invoke(obj))))
                        ret.add(String.valueOf(m.invoke(obj)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(f!=null){
                try {
                    if(!ret.contains(String.valueOf(f.get(obj))))
                        ret.add(String.valueOf(f.get(obj)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
    
    public static List<String> findNames(Enum[] objs) {
        List<String> ret = new ArrayList<>();
        for(Enum obj : objs) {
            if(!ret.contains(obj.name()))
                ret.add(obj.name());
        }
        return ret;
    }
    
    public static CommandSource getConsole() {
        return core.getConsole();
    }
    
    public static User getUser(Player base) {
        return getUser(base.getUniqueId());
    }
    
    public static User getUser(PlayerEvent e) {
        return getUser(e.getPlayer());
    }
    
    public static User getUser(String name) {
        for(User u : getOnlineUsers()) {
            if(CoreAPI.isSet()) {
                if(ColorUtil.removeColor(u.getRealName()).equalsIgnoreCase(ColorUtil.removeColor(name)) ||
                   ColorUtil.removeColor(u.getProfile().getName()).equalsIgnoreCase(ColorUtil.removeColor(name)) ||
                   u.getBase().getName().equalsIgnoreCase(ColorUtil.removeColor(name))) return u;
            }else
            if(u.getRealName().equalsIgnoreCase(name) ||
               u.getProfile().getName().equalsIgnoreCase(name) ||
               u.getBase().getName().equalsIgnoreCase(name)) return u;
        }
        return null;
    }
    public static User getUserAll(String name) {
        for(User u : getOnlineUsers()) {
            if(CoreAPI.isSet()) {
                if(ColorUtil.removeColor(u.getRealName()).equalsIgnoreCase(ColorUtil.removeColor(name)) ||
                   ColorUtil.removeColor(u.getProfile().getName()).equalsIgnoreCase(ColorUtil.removeColor(name)) ||
                   u.getBase().getName().equalsIgnoreCase(ColorUtil.removeColor(name))) return u;
            }else
            if(u.getRealName().equalsIgnoreCase(name) ||
               u.getProfile().getName().equalsIgnoreCase(name) ||
               u.getBase().getName().equalsIgnoreCase(name)) return u;
        }
        if(name.equalsIgnoreCase(core.getAllUsers().getName()))
            return core.getAllUsers();
        return null;
    }
    
    public static User getUser(UUID name) {
        for(User u : getOnlineUsers()) {
            if(CoreAPI.isSet()) {
                if(u.getUniqueId().toString().equalsIgnoreCase(ColorUtil.removeColor(name.toString()))) return u;
            }else
            if(u.getUniqueId().toString().equalsIgnoreCase((name.toString()))) return u;
        }
        return null;
    }
    
    public static OfflineUser getOfflineUser(String name) {
        for(OfflineUser u : getOfflineUsers()) {
            if(CoreAPI.isSet()) {
                if(ColorUtil.removeColor(u.getRealName()).equalsIgnoreCase(ColorUtil.removeColor(name)) ||
                   u.getBase().getName().equalsIgnoreCase(ColorUtil.removeColor(name))) return u;
            }else
            if(u.getRealName().equalsIgnoreCase(name) ||
               u.getBase().getName().equalsIgnoreCase(name)) return u;
        }
        return null;
    }
    
    public static OfflineUser getAllUser(String name) {
        if(getUser(name)!=null) {
            return getUser(name);
        }
        return getOfflineUser(name);
    }
    
    public static OfflineUser getAllUser(UUID uuid) {
        if(getUser(uuid)!=null) {
            return getUser(uuid);
        }
        return getOfflineUser(uuid);
    }
    
    public static OfflineUser getOfflineUser(UUID name) {
        for(OfflineUser u : getOfflineUsers()) {
            if(CoreAPI.isSet()) {
                if(u.getUniqueId().toString().equalsIgnoreCase(ColorUtil.removeColor(name.toString()))) return u;
            }else
            if(u.getUniqueId().toString().equalsIgnoreCase((name.toString()))) return u;
        }
        return null;
    }
    
    public static Set<CommandSource> getAllCommandSources() {
        Set<CommandSource> sources = new HashSet<>();
        for(User u : getOnlineUsers()) {
            sources.add(u);
        }
        sources.add(getConsole());
        return sources;
    }
    
    public static Set<User> getOnlineUsers() {
        Set<User> u = core == null || core.getOnline() == null ? new HashSet<>() : core.getOnline();
        return u;
    }
    
    public static Set<OfflineUser> getOfflineUsers() {
        Set<OfflineUser> u = core == null || core.getOffline() == null ? new HashSet<>() : core.getOffline();
        return u;
    }
    
    public static Set<OfflineUser> getAllUsers() {
        Set<OfflineUser> u = core == null || core.getAll() == null ? new HashSet<>() : core.getAll();
        return u;
    }
    
    public static Set<OfflineUser> getBannedPlayers() {
        Set<OfflineUser> banned = new HashSet<>();
        for(OfflineUser u : getOfflineUsers()) {
            if(getBan(u.getUniqueId())!=null)banned.add(u);
        }
        return banned;
    }
    
    public static List<String> getUserNames() {
        List<String> l = Lists.newArrayList();
        for(User u : CoreAPI.getOnlineUsers()) {
            if(!l.contains(u.getRealName()))
                l.add(u.getRealName());
        }
        return l;
    }
    
    public static List<String> getOfflineUserNames() {
        List<String> l = Lists.newArrayList();
        for(OfflineUser u : CoreAPI.getOfflineUsers()) {
            l.add(u.getRealName());
        }
        return l;
    }
    
    public static Set<String> getAllUserNames() {
        Set<String> l = Sets.newConcurrentHashSet();
        for(OfflineUser u : CoreAPI.getAllUsers()) {
            l.add(u.getRealName());
        }
        return l;
    }
    
    public static String[] formatArray(String location, Object... objs) {
        String[] ret = new String[getMessages().getStringArray(location).length];
        for(int i = 0; i < getMessages().getStringArray(location).length; i++) {
            ret[i] = formatString(getMessages().getStringArray(location)[i], objs);
        }
        return ret;
    }

    public static String[] formatArray(String[] location, Object... objs) {
        String[] ret = location;
        for(int i = 0; i < location.length; i++) {
            ret[i] = formatString(location[i], objs);
        }
        return ret;
    }
    
    public static String format(String location, Object... objs) {
        return format(getMessages(), location, objs);
    }
    
    public static String format(Config cfg, String location, Object... objs) {
        if(cfg == null) {
            return StringUtils.translate("[pc]Error[c]: Config is null");
        }
        if(!cfg.isSet(location)) {
            return StringUtils.translate("[pc]"+cfg.getName()+"[c]: Invalid location [pc]" + location);
        }
        if(cfg.getString(location) == null) {
            return StringUtils.translate("[pc]"+cfg.getName()+"[c]: location [pc]" + location + "[c] is null >>" + cfg.getString(location));
        }
        return formatString(cfg.getString(location), objs);
    }
    
    public static String formatString(String toTranslate, Object... objs) {
        String ret = toTranslate;
        for(int i = 0; i < objs.length; i++) {
            ret = ret.replace("{"+i+"}", objs[i].toString());
        }
        return StringUtils.translate(ret);
    }
    public static BanData banPlayer(BanData.BanType type, String banner, UUID target, String reason, long expire) {
        if(getUser(target)!=null) {
            User u = getUser(target);
            String prefix = CoreAPI.format(CoreAPI.getSettings(),"settings.BanReasonPrefix", reason, banner, TimeUtil.formatDateDiff(expire));
            u.kick(prefix);
        }
        return new BanData(type, banner, target, reason, expire);
    }
    
    public static BanData banPlayer(BanData.BanType type, String banner, UUID target, String reason) {
        return new BanData(type, banner, target, reason);
    }
    
    public static BanData getBan(UUID user) {
        if ((getBanFile().getSection("Banned." + user.toString()) == null) || (getBanFile().getSection("Banned." + user.toString()).getKeys(false).size() < 1)) return null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        BanData ban = null;
        try {
            ban = new BanData(BanData.BanType.valueOf(getBanFile().getString("Banned." + user + ".BanType")), getBanFile().getString("Banned." + user + ".BannedBy"),
                    user, getBanFile().getString("Banned." + user + ".Reason"), format.parse(getBanFile().getString("Banned." + user + ".Expires")).getTime());
        }
        catch (ParseException e) {
            ban = new BanData(BanData.BanType.valueOf(getBanFile().getString("Banned." + user + ".BanType")),
                    getBanFile().getString("Banned." + user + ".BannedBy"), user, getBanFile().getString("Banned." + user + ".Reason"));
        }
        return ban;
    }
    
    public static void pardonPlayer(UUID uuid) {
        getBanFile().set("Banned." + uuid, null);
    }
    
    private static void log(LogType type, Object... messages) {
        for(Object obj : messages) {
            Bukkit.getConsoleSender().sendMessage(StringUtils.translate(type.getName() + "&7>>&6 " + obj.toString()));
        }
    }
    
    public static void info(Object... messages) {
        log(LogType.INFO, messages);
    }
    
    public static void error(Object... messages) {
        log(LogType.ERROR, messages);
    }
    
    public static void warning(Object... messages) {
        log(LogType.WARNING, messages);
    }
    
    public static void debug(Object... messages) {
        if(core.getSettings()!=null)
        if(core.getSettings().getBoolean("settings.isDebug"))
            for(Object obj : messages) {
                Bukkit.getConsoleSender().sendMessage(StringUtils.translate(LogType.DEBUG.getName() + "&7>>&6 " + obj.toString()));
            }
    }
    
    public static void broadcastPerm(String permission, Object... messages) {
        for(User u : getOnlineUsers()) {
            if(u.hasPermission(permission))u.sendMessage(messages);
        }
        getConsole().sendMessage(messages);
    }
    
    public static void broadcast(Object... messages) {
        broadcastPerm("", messages);
    }
}
