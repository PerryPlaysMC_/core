package me.perryplaysmc.core.logging;

public enum LogType {

    DEBUG("&5&lDebug"),
    WARNING("&c&lWarning"),
    ERROR("&4&lError"),
    INFO("&a&lInfo");

    private String name;

    private LogType(String name)
    {
        this.name = name;
    }

    public String getName() { return name; }
}
