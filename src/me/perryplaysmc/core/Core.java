package me.perryplaysmc.core;

import com.sun.istack.internal.NotNull;
import me.perryplaysmc.Main_Core;
import me.perryplaysmc.command.Command;
import me.perryplaysmc.command.CommandHandler;
import me.perryplaysmc.command.CommandUtil;
import me.perryplaysmc.core.abstraction.AbstractionHandler;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.permissions.EconomyImpl;
import me.perryplaysmc.permissions.PermissionsImpl;
import me.perryplaysmc.permissions.group.Group;
import me.perryplaysmc.permissions.group.GroupManager;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.handlers.AllUsers;
import me.perryplaysmc.user.handlers.BaseCommandSource;
import me.perryplaysmc.user.handlers.BaseOfflineUser;
import me.perryplaysmc.user.handlers.BaseUser;
import me.perryplaysmc.utils.helper.ClassUtils;
import me.perryplaysmc.utils.helper.PluginHelper;
import me.perryplaysmc.utils.helper.SimplePluginHelper;
import me.perryplaysmc.utils.helper.ban.BanData;
import me.perryplaysmc.utils.string.TimeUtil;
import net.milkbowl.vault.Vault;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@SuppressWarnings("all")
public abstract class Core extends JavaPlugin implements Listener {
    
    
    public List<Listener> listeners;
    private Config settings, chat, permissions, messages, worlds, banFile, locations, cells;
    private Object vault;
    private CommandSource console;
    private User alluser;
    private Set<User> online;
    private Set<OfflineUser> offline;
    private Set<OfflineUser> all;
    private static Core api;
    private PluginHelper pluginHelper;
    private SimpleCommandMap scm;
    private SimplePluginManager spm;
    protected Core core;
    protected List<Core> plugins;
    protected String name;
    private List<Config> cfgs;
    private long start = System.currentTimeMillis();
    
    private void setupSimpleCommandMap() {
        spm = (SimplePluginManager) this.getServer().getPluginManager();
        Field f = null;
        try {
            f = SimplePluginManager.class.getDeclaredField("commandMap");
        } catch (Exception e) {
            e.printStackTrace();
        }
        f.setAccessible(true);
        try {
            scm = (SimpleCommandMap) f.get(spm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Core getAPI() {
        return api;
    }
    
    public List<Config> getConfigs() {
        return cfgs;
    }
    
    private void load_1() {
        cfgs = new ArrayList<>();
        if(settings == null)
            settings = new Config("settings.yml");
        if(messages == null)
            messages = new Config("messages.yml");
        if(banFile==null)
            banFile = new Config("player_data", "bannedPlayers.yml");
        if(worlds==null)
            worlds = new Config("worlds.yml");
        if(permissions == null)
            permissions = new Config("permissions.yml");
        if(locations == null)
            locations = new Config("locations.yml");
        if(cells == null && !settings.getBoolean("Features.Cell.perGroupFile"))
            cells = new Config("cells.yml");
        if(chat == null)
            chat = new Config("chat.yml");
        
        cfgs.add(chat);
        cfgs.add(cells);
        cfgs.add(locations);
        cfgs.add(settings);
        cfgs.add(permissions);
        cfgs.add(worlds);
        cfgs.add(banFile);
        cfgs.add(messages);
        
        
        if(console == null)
            console = new BaseCommandSource(Bukkit.getConsoleSender());
        if(alluser == null)
            alluser = new AllUsers();
        if(scm == null)
            setupSimpleCommandMap();
        if(pluginHelper == null)
            pluginHelper = new SimplePluginHelper();
    }
    
    public void load() {
        online = new HashSet<>();
        offline = new HashSet<>();
        all = new HashSet<>();
        if(listeners == null) listeners = new ArrayList<>();
        try {
            for (OfflinePlayer p : Bukkit.getOfflinePlayers()) {
                if(p!=null) {
                    OfflineUser u = new BaseOfflineUser(p);
                    addOfflineUser(u);
                }
            }
        }catch(Exception e) {
            debug("Retrying later - offlinePlayer loading");
        }
        for(Player p : Bukkit.getOnlinePlayers()) {
            if(p!=null) {
                User u = new BaseUser(p);
                addUser(u);
            }
        }
        for(Core c : plugins) {
            c.alluser = getAllUsers();
            c.online = online;
            c.offline = offline;
            c.all = all;
        }
    }
    
    protected void loadData() {
        core = Main_Core.getPlugin(Main_Core.class);
//        this.online  = core.getOnline();
//        this.offline = core.getOffline();
//        this.alluser = core.getAllUsers();
        if(listeners == null) listeners = core.listeners;
        messages = core.getMessages();
        banFile = core.getBanFile();
        worlds = core.getWorlds();
        permissions = core.getPermissions();
        settings = core.getSettings();
        locations = core.getLocations();
        chat = core.getChat();
        console = core.getConsole();
        alluser = core.getAllUsers();
        cells = core.getCells();
        scm = core.getCommandMap();
        pluginHelper = core.getPluginHelper();
        core.plugins.add(this);
        CoreAPI.setCore(core);
        try {
            Field f = getDescription().getClass().getDeclaredField("name");
            f.setAccessible(true);
            f.set(getDescription(), name);
            f.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected void init() {
        start = System.currentTimeMillis();
        api = this;
        AbstractionHandler.load();
        load_1();
        plugins = new ArrayList<>();
        if(getPluginHelper().isEnabled("Vault") && getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class)==null)
                Bukkit.getServer().getServicesManager()
                        .register(net.milkbowl.vault.permission.Permission.class,
                                new PermissionsImpl(this), Vault.getPlugin(Vault.class),
                                ServicePriority.Normal);
            if(Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class)==null)
                Bukkit.getServer().getServicesManager()
                        .register(net.milkbowl.vault.economy.Economy.class,
                                new EconomyImpl(this), Vault.getPlugin(Vault.class),
                                ServicePriority.Normal);
        }
        this.name = getSettings().getString("settings.main-name")+"Core";
        setupPermissions();
        CoreAPI.setCore(this);
        load();
        CoreAPI.setCore(this);
    }
    
    
    public void initialize(String name) {
        if(!name.equalsIgnoreCase("Core")) {
            start = System.currentTimeMillis();
            load_1();
            this.name = getSettings().getString("settings.main-name")+name;
            loadData();
        }else {
            try {
                Field f = getDescription().getClass().getDeclaredField("name");
                f.setAccessible(true);
                f.set(getDescription(), getSettings().getString("settings.main-name") + "Core");
                f.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        GroupManager.reload();
        registerListeners(this);
        //registerListeners("net.miniverse");
        long end = System.currentTimeMillis();
        CoreAPI.info(" ");
        CoreAPI.info(" ");
        CoreAPI.info("Took " + (end-start) + "ms to initialize " + this.name);
        CoreAPI.info(" ");
        CoreAPI.info(" ");
    }
    
    
    
    private boolean setupPermissions() {
        if(!getPluginHelper().isEnabled("Vault")) {
            return false;
        }
        RegisteredServiceProvider<net.milkbowl.vault.permission.Permission> rsp =
                Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        vault = rsp.getProvider();
        return vault != null;
    }
    
    
    
    public InputStream getResource(String name) {
        return super.getResource(name);
    }
    
    public void debug(Object... messages) {
        CoreAPI.debug( messages);
    }
    
    public void registerListeners(Listener... listeners) {
        for(Listener l : listeners) {
            if(!this.listeners.contains(l)) {
                try {
                    pluginHelper.registerListener(listeners);
                }catch(Exception e) {
                    e.printStackTrace();
                }
                debug("Registered listener " + l.getClass().getSimpleName());
                this.listeners.add(l);
            }
        }
    }
    
    public void registerListeners(String packageName) {
        for(Class<Listener> l : ClassUtils.getAllClassesOf(getFile(), Listener.class, packageName)) {
            try {
                Listener i = l.newInstance();
                registerListeners(i);
            } catch (InstantiationException | IllegalAccessException e) {
            }
        }
    }
    
    public void registerCommands(String packageName) {
        for(Class<Command> l : ClassUtils.getAllClassesOf(getFile(), Command.class, packageName)) {
            try {
                Command i = l.newInstance();
                registerCommand(i);
                if(i instanceof Listener)
                    registerListeners((Listener) i);
            } catch (InstantiationException | IllegalAccessException e) {
            }
        }
    }
    
    public void registerAllListeners() {
        registerListeners("net.miniverse");
    }
    
    public Player loadPlayer(@NotNull final OfflinePlayer offline) {
        final String key = offline.getUniqueId().toString();
        if (offline.isOnline()) {
            final Player loaded = offline.getPlayer();
            return loaded;
        }
        if (Bukkit.isPrimaryThread()) {
            return AbstractionHandler.getPlayerDataLoader().loadPlayer(offline);
        }
        final Future<Player> future = Bukkit.getScheduler().callSyncMethod(this, () ->
                AbstractionHandler.getPlayerDataLoader().loadPlayer(offline));
        int ticks = 0;
        while (!future.isDone() && !future.isCancelled() && ticks < 10) {
            ++ticks;
            try {
                Thread.sleep(50L);
                continue;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        if (!future.isDone() || future.isCancelled()) {
            return null;
        }
        Player loaded;
        try {
            loaded = future.get();
        }
        catch (InterruptedException | ExecutionException e2) {
            e2.printStackTrace();
            return null;
        }
        return loaded;
    }
    
    public void registerCommand(Command cmd) {
        String name = getSettings().getString("settings.main-name");
        boolean isDoubleSlash = CoreAPI.getSettings().getBoolean("settings.isCommandDoubleSlash");
        String x = isDoubleSlash ? "/"+cmd.getName():cmd.getName();
        boolean ret = false;
        if(getSettings().isSet("Command-Toggle."+cmd.getName()) && !getSettings().getBoolean("Command-Toggle."+cmd.getName()))
            ret = true;
        if(!ret&&cmd.getAliases().length > 0)
            for(String s : cmd.getAliases())
                if(getSettings().isSet("Command-Toggle."+s) && !getSettings().getBoolean("Command-Toggle."+s))
                    ret = true;
        if(ret) {
            CoreAPI.warning("[c]Command '[pc]" + x + "[c]' Can't be registered because it has been disabled in the config(settings.yml)");
            return;
        }
        CommandHandler command = new CommandHandler(isDoubleSlash ? "/" + cmd.getName():cmd.getName());
        command.setDescription(cmd.getDescription());
        command.setPermission(cmd.getPermission());
        Permission perm = Bukkit.getPluginManager().getPermission(cmd.getPermission().toLowerCase());
        if(perm == null) {
            perm = new Permission(cmd.getPermission().toLowerCase());
            Bukkit.getPluginManager().addPermission(perm);
        }
        for(String s : cmd.getDefaultPermissions()) {
            String pre = CoreAPI.getSettings().getString("settings.permissions-prefix.command")+".";
            String perms = s.toLowerCase().startsWith(pre.toLowerCase()) ? s.toLowerCase() : pre.toLowerCase()+s.toLowerCase();
            perm = Bukkit.getPluginManager().getPermission(perms);
            if(perm == null) {
                perm = new Permission(perms);
                Bukkit.getPluginManager().addPermission(perm);
            }
        }
        for(String a : cmd.getAliases()) {
            CommandHandler command2 = new CommandHandler(isDoubleSlash?"/"+a:a);
            command2.setDescription(cmd.getDescription());
            command2.setPermission(cmd.getPermission());
            scm.register(command2.getName(), name.toLowerCase(), command2);
        }
        debug("Registered command '" + command.getName() + "'");
        CommandHandler.addCommand(cmd);
        scm.register(command.getName(), name.toLowerCase(), command);
        getSettings().setIfNotSet("Command-Toggle."+cmd.getName(), true);
        CommandUtil.reloadTopics();
    }
    
    public void removeUser(User u) {
        if(!all.contains(u)) {
            all.add(u);
        }
        online.remove(u);
        for(Core c : plugins) {
            c.alluser = getAllUsers();
            c.online = online;
            c.offline = offline;
            c.all = all;
        }
        CoreAPI.setCore(this);
    }
    
    public void removeOfflineUser(OfflineUser u) {
        if(!all.contains(u)) {
            all.add(u);
        }
        offline.remove(u);
        CoreAPI.setCore(this);
    }
    
    public void addUser(User u) {
        if(CoreAPI.getOfflineUser(u.getBase().getName())!=null)
            removeOfflineUser(CoreAPI.getOfflineUser(u.getBase().getName()));
        if(!all.contains(u)) {
            all.add(u);
        }
        if(!online.contains(u)) {
            online.add(u);
        }
        for(Group g : u.getPermissionData().getGroups()) {
            if(g!=null)
                for(String perm : g.getPermissions())
                    u.getPermissionData().addPermission(perm);
        }
        CoreAPI.setCore(this);
    }
    
    public void addOfflineUser(OfflineUser u) {
        try {
            if(CoreAPI.getUser(u.getBase().getName()) != null)
                removeUser(CoreAPI.getUser(u.getBase().getName()));
            if(!all.contains(u)) {
                all.add(u);
            }
            if(!offline.contains(u)) {
                offline.add(u);
            }
        }catch(Exception e) {
            debug(u==null ? "oof O.o" : u.getBase().getName());
        }
        CoreAPI.setCore(this);
    }
    
    public File getFile() {
        return super.getFile();
    }
   
    public PluginHelper getPluginHelper() {
        return pluginHelper;
    }
  
    public CommandSource getConsole() {
        return console;
    }
    
    public User getAllUsers() {
        return alluser;
    }
    
    public SimpleCommandMap getCommandMap() {
        return scm;
    }
    
    public Config getSettings() {
        return settings == null ? settings = new Config("settings.yml") : settings;
    }
    
    public Config getCells() {
        if(cells == null && !settings.getBoolean("Features.Cell.perGroupFile"))
            cells = new Config("cells.yml");
        return cells;
    }
    
    public Config getChat() {
        return chat == null ? chat = new Config("chat.yml") : chat;
    }
    
    public Config getBanFile() {
        return banFile == null ? banFile = new Config("player_data", "bannedPlayers.yml") : banFile;
    }
    
    public Config getPermissions() {
        return permissions == null ? permissions = new Config("permissions.yml") : permissions;
    }
    
    public Config getLocations() {
        return locations == null ? locations = new Config("locations.yml") : locations;
    }
    
    public Config getWorlds() {
        return worlds == null ? worlds = new Config("worlds.yml") : worlds;
    }
    
    public Config getMessages() {
        return messages == null ? messages = new Config("messages.yml") : messages;
    }
    
    protected Set<User> getOnline() {
        return online;
    }
    
    protected Set<OfflineUser> getAll() {
        return all;
    }
    
    protected Set<OfflineUser> getOffline() {
        return offline;
    }
    
    public Object getVaultPermissions() {
        return (net.milkbowl.vault.permission.Permission)vault;
    }
    
    public void registerUser(User user) {
        Scoreboard sb = Bukkit.getScoreboardManager().getMainScoreboard();
        Team t = sb.getTeam("c") != null?sb.getTeam("c"):sb.registerNewTeam("c");
        t.setNameTagVisibility(NameTagVisibility.NEVER);
        t.addPlayer(user.getBase());
    }
    
    
    
    List<String> s = new ArrayList<>();
    
    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        User u = CoreAPI.getUser(p.getUniqueId());
        if(u==null) {
            u = new BaseUser(p);
            addUser(u);
        }
        BanData b = CoreAPI.getBan(u.getUniqueId());
        if(b != null) {
            if(b.getExpire() == -1) {
                
                String reason =
                        CoreAPI.format(CoreAPI.getSettings(),"settings.BanReasonPrefix", b.getReason(), b.getBy(), "Never");
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, reason);
                e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                return;
            }
            if(TimeUtil.formatDateDiff((b.getExpire())).equalsIgnoreCase("Now")) {
                CoreAPI.pardonPlayer(u.getUniqueId());
                return;
            }
            String reason = TimeUtil.removeTimePattern(
                    CoreAPI.format(CoreAPI.getSettings(),"settings.BanReasonPrefix", b.getReason(), b.getBy(), TimeUtil.formatDateDiff(b.getExpire())));
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, reason);
            e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            return;
        }
    }
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        User u = CoreAPI.getUser(p.getUniqueId());
        if(s.contains(e.getPlayer().getName()))return;
        else s.add(e.getPlayer().getName());
        (new BukkitRunnable() {
            public void run() {
                s.remove(e.getPlayer().getName());
            }
        }).runTaskLater(this, 5);
        if(u==null) {
            u = new BaseUser(p);
            addUser(u);
        }
        for(Group g : GroupManager.getGroups()) {
            if(g.isDefault()) {
                if(!u.getPermissionData().getGroups().contains(g))
                    g.addUser(u);
            }
        }
    }
    
    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        OfflineUser u = new BaseOfflineUser(e.getPlayer());
        u.getConfig().set("settings.LastKnownLocation", e.getPlayer().getLocation());
        User u1 = CoreAPI.getUser(e.getPlayer());
        if(u1!=null&&u1.getReplier()!=null&&u1.getName().equalsIgnoreCase(u1.getReplier().getRealName()))
            u1.getReplier().setReplier(null);
        if(u1!=null) {
            u1.setReplier(null);
        }
        u.getPermissionData().removeAttachment();
        if(CoreAPI.getOfflineUser(e.getPlayer().getUniqueId())==null)
            (new BukkitRunnable() {
                @Override
                public void run() {
                    addOfflineUser(u);
                }
            }).runTaskLater(this, 2);
    }
    
    
}
