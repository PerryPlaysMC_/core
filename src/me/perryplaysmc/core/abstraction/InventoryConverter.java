package me.perryplaysmc.core.abstraction;

import me.perryplaysmc.utils.inventory.CustomInventory;
import org.bukkit.inventory.Inventory;

public interface InventoryConverter {
    
    
    
    
    CustomInventory convertInventory(Inventory inv);
    
    
    
    
    
}
