package me.perryplaysmc.core.abstraction;

import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.entityHider.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.itemUtil.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_14.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_8.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_9.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_10.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_11.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_12.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_13.*;
import me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v1_14.*;
import org.bukkit.Bukkit;

public class AbstractionHandler {
    
    private static PacketUtil packetUtil;
    private static JsonMessager messager;
    private static EntityHider entityHider;
    private static InventoryCreate inventoryCreate;
    private static PlayerDataLoader playerDataLoader;
    private static InventoryConverter inventoryConverter;
    private static ItemDataUtilHelper itemDataUtilHelper;
    private static ItemStackJSONConverter itemStackJSONConverter;
    
    public static void load() {
        String pack = Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.') + 1).replaceFirst("v", "");
        boolean isSupported = true;
        long start = System.currentTimeMillis();
        System.out.println("Loading PacketData for version " + version);
        if(true) {
            try {
                Class handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.jsonMessager.v" +
                                              version.substring(0, version.lastIndexOf('_'))
                                              +".JsonMessager_v"+version);
                if(handler!=null)
                    messager = (JsonMessager) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".PacketUtil_v"+version);
                if(handler!=null)
                    packetUtil = (PacketUtil) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.entityHider.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".EntityHider_v"+version);
                if(handler!=null)
                    entityHider = (EntityHider) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".Inventory_v"+version);
                if(handler!=null)
                    inventoryCreate = (InventoryCreate) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.playerDataLoader.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".PlayerData_v"+version);
                if(handler!=null)
                    playerDataLoader = (PlayerDataLoader) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.itemUtil.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".ItemUtil_v"+version);
                if(handler!=null)
                    itemDataUtilHelper = (ItemDataUtilHelper) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".InventoryConverter_v"+version);
                if(handler!=null)
                    inventoryConverter = (InventoryConverter) handler.newInstance();
                handler = Class.forName("me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v" +
                                        version.substring(0, version.lastIndexOf('_'))
                                        +".ItemStackConverter_v"+version);
                if(handler!=null)
                    itemStackJSONConverter = (ItemStackJSONConverter) handler.newInstance();
            } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
                e.printStackTrace();
            }
            return;
        }
        switch (version) {
            case "1_8_R1": {
                messager = new JsonMessager_v1_8_R1();
                packetUtil = new PacketUtil_v1_8_R1();
                entityHider = new EntityHider_v1_8_R1();
                inventoryCreate = new Inventory_v1_8_R1();
                playerDataLoader = new PlayerData_v1_8_R1();
                itemDataUtilHelper = new ItemUtil_v1_8_R1();
                inventoryConverter = new InventoryConverter_v1_8_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_8_R1();
                break;
            }
            case "1_8_R2": {
                messager = new JsonMessager_v1_8_R2();
                packetUtil = new PacketUtil_v1_8_R2();
                entityHider = new EntityHider_v1_8_R2();
                inventoryCreate = new Inventory_v1_8_R2();
                playerDataLoader = new PlayerData_v1_8_R2();
                itemDataUtilHelper = new ItemUtil_v1_8_R2();
                inventoryConverter = new InventoryConverter_v1_8_R2();
                itemStackJSONConverter = new ItemStackConverter_v1_8_R2();
                break;
            }
            case "1_8_R3": {
                messager = new JsonMessager_v1_8_R3();
                packetUtil = new PacketUtil_v1_8_R3();
                entityHider = new EntityHider_v1_8_R3();
                inventoryCreate = new Inventory_v1_8_R3();
                playerDataLoader = new PlayerData_v1_8_R3();
                itemDataUtilHelper = new ItemUtil_v1_8_R3();
                inventoryConverter = new InventoryConverter_v1_8_R3();
                itemStackJSONConverter = new ItemStackConverter_v1_8_R3();
                break;
            }
            case "1_9_R1": {
                messager = new JsonMessager_v1_9_R1();
                packetUtil = new PacketUtil_v1_9_R1();
                entityHider = new EntityHider_v1_9_R1();
                inventoryCreate = new Inventory_v1_9_R1();
                playerDataLoader = new PlayerData_v1_9_R1();
                itemDataUtilHelper = new ItemUtil_v1_9_R1();
                inventoryConverter = new InventoryConverter_v1_9_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_9_R1();
                break;
            }
            case "1_9_R2": {
                messager = new JsonMessager_v1_9_R2();
                packetUtil = new PacketUtil_v1_9_R2();
                entityHider = new EntityHider_v1_9_R2();
                inventoryCreate = new Inventory_v1_9_R2();
                playerDataLoader = new PlayerData_v1_9_R2();
                itemDataUtilHelper = new ItemUtil_v1_9_R2();
                inventoryConverter = new InventoryConverter_v1_9_R2();
                itemStackJSONConverter = new ItemStackConverter_v1_9_R2();
                break;
            }
            case "1_10_R1": {
                messager = new JsonMessager_v1_10_R1();
                packetUtil = new PacketUtil_v1_10_R1();
                entityHider = new EntityHider_v1_10_R1();
                inventoryCreate = new Inventory_v1_10_R1();
                playerDataLoader = new PlayerData_v1_10_R1();
                itemDataUtilHelper = new ItemUtil_v1_10_R1();
                inventoryConverter = new InventoryConverter_v1_10_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_10_R1();
                break;
            }
            case "1_11_R1": {
                messager = new JsonMessager_v1_11_R1();
                packetUtil = new PacketUtil_v1_11_R1();
                entityHider = new EntityHider_v1_11_R1();
                inventoryCreate = new Inventory_v1_11_R1();
                playerDataLoader = new PlayerData_v1_11_R1();
                itemDataUtilHelper = new ItemUtil_v1_11_R1();
                inventoryConverter = new InventoryConverter_v1_11_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_11_R1();
                break;
            }
            case "1_12_R1": {
                messager = new JsonMessager_v1_12_R1();
                packetUtil = new PacketUtil_v1_12_R1();
                entityHider = new EntityHider_v1_12_R1();
                inventoryCreate = new Inventory_v1_12_R1();
                playerDataLoader = new PlayerData_v1_12_R1();
                itemDataUtilHelper = new ItemUtil_v1_12_R1();
                inventoryConverter = new InventoryConverter_v1_12_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_12_R1();
                break;
            }
            case "1_13_R1": {
                messager = new JsonMessager_v1_13_R1();
                packetUtil = new PacketUtil_v1_13_R1();
                entityHider = new EntityHider_v1_13_R1();
                inventoryCreate = new Inventory_v1_13_R1();
                playerDataLoader = new PlayerData_v1_13_R1();
                itemDataUtilHelper = new ItemUtil_v1_13_R1();
                inventoryConverter = new InventoryConverter_v1_13_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_13_R1();
                break;
            }
            case "1_13_R2": {
                messager = new JsonMessager_v1_13_R2();
                packetUtil = new PacketUtil_v1_13_R2();
                entityHider = new EntityHider_v1_13_R2();
                inventoryCreate = new Inventory_v1_13_R2();
                playerDataLoader = new PlayerData_v1_13_R2();
                itemDataUtilHelper = new ItemUtil_v1_13_R2();
                inventoryConverter = new InventoryConverter_v1_13_R2();
                itemStackJSONConverter = new ItemStackConverter_v1_13_R2();
                break;
            }
            case "1_14_R1": {
                messager = new JsonMessager_v1_14_R1();
                packetUtil = new PacketUtil_v1_14_R1();
                entityHider = new EntityHider_v1_14_R1();
                inventoryCreate = new Inventory_v1_14_R1();
                playerDataLoader = new PlayerData_v1_14_R1();
                itemDataUtilHelper = new ItemUtil_v1_14_R1();
                inventoryConverter = new InventoryConverter_v1_14_R1();
                itemStackJSONConverter = new ItemStackConverter_v1_14_R1();
                break;
            }
            default: {
                isSupported = false;
                System.out.println("Unsupported version '" + version + "'");
                break;
            }
        }
        long end = System.currentTimeMillis() - start;
        if(isSupported) {
            System.out.println("Loaded PacketData for version " + version + " Took " + end + "ms");
        }
    }
    
    public static ItemDataUtilHelper getItemDataUtilHelper() {
        return itemDataUtilHelper;
    }
    
    public static JsonMessager getMessager() {
        return messager;
    }
    
    public static ItemStackJSONConverter getItemStackJSONConverter() {
        return itemStackJSONConverter;
    }
    
    public static InventoryCreate getInventoryCreate() {
        return inventoryCreate;
    }
    
    public static PlayerDataLoader getPlayerDataLoader() {
        return playerDataLoader;
    }
    
    public static InventoryConverter getInventoryConverter() {
        return inventoryConverter;
    }
    
    public static PacketUtil getPacketUtil() {
        return packetUtil;
    }
    
    public static EntityHider getEntityHider() {
        return entityHider;
    }
}
