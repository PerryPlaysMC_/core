package me.perryplaysmc.topics;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.help.HelpTopic;

public class Topic extends HelpTopic {

    private String description, name;

    public Topic(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getFullText(CommandSender forWho) {
        return ChatColor.translateAlternateColorCodes('&', description);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getShortText() {
        return ChatColor.translateAlternateColorCodes('&', description);
    }

    public void setName(String newName) {
        this.name = newName;
    }



    @Override
    public boolean canSee(CommandSender sender) {
        return true;
    }
}
