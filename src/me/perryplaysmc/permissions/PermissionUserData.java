package me.perryplaysmc.permissions;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.permissions.group.Group;
import me.perryplaysmc.permissions.group.GroupManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.permissions.*;

import java.lang.reflect.Field;
import java.util.*;

@SuppressWarnings("all")
public class PermissionUserData {

    private Player base;
    private OfflineUser u;
    private Config perm = CoreAPI.getPermissions();
    private HashMap<UUID, PermissionAttachment> attachments;

    public PermissionUserData(OfflineUser u) {
        this.u = u;
        base = (u.getAsPlayer());
        attachments = new HashMap<>();
        loadAttachment();
        updatePermissions();
    }

    public void removeAttachment() {
        /*if(attachments.containsKey(u.getUniqueId())) {
            PermissionAttachment attachment = attachments.get(u.getUniqueId());
            for(String s : attachment.getPermissions().keySet()) {
                if(!attachment.getPermissions().get(s)) {
                    attachment.unsetPermission(s);
                }
            }
            attachment.getPermissible().recalculatePermissions();
            attachment.remove();
        }*/
    }
    public void loadAttachment() {
        if(!attachments.containsKey(u.getUniqueId())) {
            PermissionAttachment attachment = base.addAttachment(Core.getAPI());
            Set<PermissionAttachmentInfo> inf = attachment.getPermissible().getEffectivePermissions();
            List<String >f = new ArrayList<>();
            for(PermissionAttachmentInfo i : inf) {
                if(f.contains(i.getPermission())) continue;
                else f.add(i.getPermission());
                //System.out.println(i.getPermission() + "/" + i.getValue());
                attachment.setPermission(i.getPermission(), i.getValue());
            }
            attachments.put(u.getUniqueId(), attachment);
        }
    }

    public Set<PermissionAttachmentInfo> getEffectivePermissions() {
        return base.getEffectivePermissions();
    }

    public List<String> getPermissions() {
        List<String> x = new ArrayList<>();
        for(PermissionAttachmentInfo a : getEffectivePermissions()) {
            if(!x.contains(a.getPermission()) && a.getValue())
                x.add(a.getPermission());
        }
        return x;
    }

    public String getPrefix() {
        String prefix;
        if(getPrimaryGroup() != null && getPrimaryGroup().getPrefix() != "" && getPrimaryGroup().getPrefix() != null) {
            prefix = getPrimaryGroup().getPrefix();
        }else if(perm.getString("Permissions.Users." + u.getUniqueId().toString() + ".prefix") != ""
                && perm.getString("Permissions.Users." + u.getUniqueId().toString() + ".prefix") != null) {
            prefix = perm.getString("Permissions.Users." + u.getUniqueId().toString() + ".prefix");
        }else {
            prefix = "";
        }
        return ColorUtil.translateColors('&', prefix);
    }

    public void setPrefix(String newPrefix) {
        if(newPrefix.equalsIgnoreCase(u.getUniqueId().toString())) {
            perm.set("Permissions.Users."+u.getUniqueId()+".prefix", null);
            return;
        }
        perm.set("Permissions.Users."+u.getUniqueId()+".prefix", ColorUtil.translateColors('&', newPrefix));
    }

    public String getSuffix() {
        String prefix;
        if(getPrimaryGroup() != null && !StringUtils.isEmpty(getPrimaryGroup().getSuffix())) {
            prefix = getPrimaryGroup().getSuffix();
        }else if(!StringUtils.isEmpty(perm.getString("Permissions.Users." + u.getUniqueId().toString() + ".suffix"))) {
            prefix = perm.getString("Permissions.Users." + u.getUniqueId().toString() + ".suffix");
        }else {
            prefix = "";
        }
        return ColorUtil.translateColors('&', prefix);
    }

    public void setSuffix(String newSuffix) {
        if(newSuffix.equalsIgnoreCase(u.getUniqueId().toString())) {
            perm.set("Permissions.Users."+u.getUniqueId()+".prefix", null);
            return;
        }
        perm.set("Permissions.Users."+u.getUniqueId()+".suffix", ColorUtil.translateColors('&', newSuffix));
    }

    public List<Group> getGroups() {
        List<Group> groups = new ArrayList<>();
        for(String group : perm.getStringList("Permissions.Users." + u.getUniqueId().toString() + ".groups")) {
            if(GroupManager.getGroup(group)!=null)
                groups.add(GroupManager.getGroup(group));
        }
        return groups;
    }

    public Group getPrimaryGroup() {
        List<Group> groups = getGroups();
        if(getGroups().size() == 0) {
            return null;
        }
        int highest = 0;
        for(int i = 1; i < getGroups().size(); i++) {
            if(groups.get(i).getRank() < groups.get(highest).getRank()) highest = i;
        }
        return groups.get(highest);
    }



    public boolean isPermissionSet(String... permissions) {
        String cmdPrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.command");
        String featurePrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.feature");
        String pre = CoreAPI.getSettings().getString("settings.main-name").toLowerCase();
        for(String perm : permissions) {
            if ((base.isPermissionSet("*")) ||
                    (base.isPermissionSet(perm + ".*")) ||
                    (base.isPermissionSet(cmdPrefix+"." + perm + ".*")) ||
                    (base.isPermissionSet(cmdPrefix+"." + perm + ".*")) ||
                    (base.isPermissionSet(pre+".*")) ||
                    (base.isPermissionSet(perm)) ||
                    (base.isPermissionSet(pre+"." + perm)) ||
                    (base.isPermissionSet(cmdPrefix + "." + perm)) ||
                    (base.isPermissionSet(featurePrefix + "." + perm)) ||
                    (base.isOp())) {
                return true;
            }
        }
        return false;
    }

    public boolean hasSinglePermission(String... permissions) {
        for(String permission : permissions)
            for(PermissionAttachmentInfo info : getEffectivePermissions())
                if(info.getValue() && info.getPermission().equalsIgnoreCase(permission))return true;
        return false;
    }

    public boolean isSinglePermissionSet(String... permissions) {
        for(String permission : permissions)
            if(base.isPermissionSet(permission))return true;
        return false;
    }

    boolean getValue(String permission) {
        PermissionAttachment attachment = attachments.get(u.getUniqueId());
        return attachment.getPermissions().containsKey(permission) && attachment.getPermissions().get(permission);
    }

    public boolean hasPermission(String... permissions) {
        String cmdPrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.command");
        String featurePrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.feature");
        String pre = CoreAPI.getSettings().getString("settings.main-name").toLowerCase();
        for(String perm : permissions) {
            if ((base.hasPermission("*") && getValue("*")) ||
                    (base.hasPermission(perm + ".*") && getValue(perm + ".*")) ||
                    (base.hasPermission(cmdPrefix+"." + perm + ".*") && getValue(cmdPrefix+"." + perm + ".*")) ||
                    (base.hasPermission(pre+".*") && getValue(pre+".*")) ||
                    (base.hasPermission(perm) && getValue(perm)) ||
                    (base.hasPermission(pre+"." + perm) && getValue(pre+"." + perm)) ||
                    (base.hasPermission(featurePrefix+"." + perm) && getValue(featurePrefix+"." + perm)) ||
                    (base.hasPermission(cmdPrefix+"." + perm) && getValue(cmdPrefix+"." + perm))||
                    base.isOp()) {
                return true;
            }
        }
        return false;
    }



    public void disablePermission(String permission) {
        List<String> perms = perm.getStringList("Permissions.Users."+u.getUniqueId().toString()+".disabledPermissions");
        if(!perms.contains(permission.toLowerCase()))
            perms.add(permission.toLowerCase());
        perm.set("Permissions.Users."+u.getUniqueId().toString()+".disabledPermissions", perms);
    }

    public void enablePermission(String permission) {
        List<String> perms = perm.getStringList("Permissions.Users."+u.getUniqueId().toString()+".disabledPermissions");
        if(perms.contains(permission.toLowerCase()))
            perms.remove(permission.toLowerCase());
        perm.set("Permissions.Users."+u.getUniqueId().toString()+".disabledPermissions", perms);
    }


    public void addPermission(String permission) {
        PermissionAttachment attachment = attachments.get(u.getUniqueId());
        if(!base.hasPermission(permission)) {
            attachment.setPermission(permission, true);
            attachments.put(u.getUniqueId(), attachment);
        }
        List<String> perms = perm.getStringList("Permissions.Users."+u.getUniqueId().toString()+".permissions");
        if(!perms.contains(permission.toLowerCase()))
            perms.add(permission.toLowerCase());
        perm.set("Permissions.Users."+u.getUniqueId().toString()+".permissions", perms);
        updatePermissions();

    }
    public void removePermission(String permission) {
        PermissionAttachment attachment = attachments.get(u.getUniqueId());
        if(base.hasPermission(permission)) {
            attachment.setPermission(permission.toLowerCase(), false);
            Bukkit.getPluginManager().unsubscribeFromPermission(permission, base);
            base.recalculatePermissions();
            attachments.put(u.getUniqueId(), attachment);
        }
        List<String> perms = perm.getStringList("Permissions.Users."+u.getUniqueId().toString()+".permissions");
        if(perms.contains(permission.toLowerCase()))
            perms.remove(permission.toLowerCase());
        perm.set("Permissions.Users."+u.getUniqueId().toString()+".permissions", perms);
        updatePermissions();
    }


    public void updatePermissions() {
        PermissionAttachment attachment = attachments.get(u.getUniqueId());
        List<String> dPerms = perm.getStringList("Permissions.Users."+u.getUniqueId().toString()+".disabledPermissions");
        List<String> perms = perm.getStringList("Permissions.Users."+u.getUniqueId().toString()+".permissions");
        List<String> perms2 = new ArrayList<>();
        for(Group g : getGroups())
            if(g!=null)
                for(String s : g.getPermissions())
                    if(!perms2.contains(s.toLowerCase()))
                        perms2.add(s.toLowerCase());
        perms.addAll(perms2);
        for(String permission : perms) {
            if(StringUtils.isEmpty(permission))continue;
            boolean isDisabled = dPerms.contains(permission.toLowerCase());
            if(isDisabled){
                attachment.setPermission(permission.toLowerCase(), false);
            }else {
                attachment.setPermission(permission.toLowerCase(), true);
            }
        }
        try {
            if(Version.isVersionHigherThan(true, Versions.v1_13))
                if(u.isOnline()){
                    if(Core.getAPI().isEnabled()&&Core.getAPI()!=null)
                        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Core.getAPI(), () -> {
                            if (u != null && u.getAsPlayer() != null)
                                u.getAsPlayer().updateCommands();
                        }, 1);
                    else if (u != null && u.getAsPlayer() != null)
                        u.getAsPlayer().updateCommands();
                }
            attachment.getPermissible().recalculatePermissions();
        }catch (Exception e) {

        }
    }


    public void clearPermissions() {
        for(PermissionAttachmentInfo inf : getEffectivePermissions())
            removePermission(inf.getPermission());
        base.getEffectivePermissions().clear();
        getEffectivePermissions().clear();
        perm.set("Permissions.Users."+u.getUniqueId().toString()+".permissions", "");
        perm.set("Permissions.Users."+u.getUniqueId().toString()+".disabledPermissions", "");
    }
}
