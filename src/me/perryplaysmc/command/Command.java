package me.perryplaysmc.command;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.core.configuration.ConfigManager;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.data.PlayerData;
import me.perryplaysmc.user.handlers.Home;
import me.perryplaysmc.utils.inventory.ingorish.EnchantUtil;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.permissions.group.Group;
import me.perryplaysmc.permissions.group.GroupManager;
import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;

import java.util.*;

@SuppressWarnings("all")
public abstract class Command  {

    private String name, description, permission, usage;
    private String[] aliases;
    protected CommandSource s;
    protected boolean allowOfflinePlayerTab = false, useMaxTab=false;
    protected int maxTabArgs = 1;
    protected CommandHandler handler;
    protected List<String> defaultPermissions = new ArrayList<>();
    protected Argument[] args, tabArgs;


    public Command(String name, String... aliases)  {
        this.name = name = name.toLowerCase().replace(" ", "-");
        this.aliases = new String[aliases.length];
        for(int i = 0; i < aliases.length; i ++) {
            this.aliases[i] = aliases[i].replace(" ", "");
        }
        this.permission = CoreAPI.getSettings().getString("settings.permissions-prefix.command")+"."+name;
        this.description = "Undefined";
        this.usage = "/" + getName();
    }
    public Command(List<String> aliases)  {
        name = aliases.get(0);
        this.name = name.toLowerCase().replace(" ", "-");
        this.aliases = new String[aliases.size()+-1];
        if(aliases.size()>1)
            for(int i = 0; i < (aliases.size()+-1); i ++) {
                this.aliases[i] = aliases.get(i+1).replace(" ", "");
            }
        this.permission = CoreAPI.getSettings().getString("settings.permissions-prefix.command")+"."+name;
        this.description = "Undefined";
        this.usage = "/" + getName();
    }

    protected void addDefaultPermission(String... permissions) {
        for(String s : permissions) {
            if(!defaultPermissions.contains(s.toLowerCase()))
                defaultPermissions.add(s.toLowerCase());
        }
    }

    public String[] getDefaultPermissions() {
        return defaultPermissions.toArray(new String[defaultPermissions.size()]);
    }

    public String getUsage() {
        return usage;
    }


    public void setUsage(String usage) {
        this.usage = usage;
    }


    public CommandHandler getHandler() {
        return handler;
    }

    public abstract void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException;

    public List<String> tabComplete(CommandSource s, CommandLabel label, Argument[] args) throws CommandException, IllegalArgumentException {
        List<String> f = Lists.newArrayList();
        List<String> l = Lists.newArrayList();
        this.tabArgs = args;
        if(args.length == 0 || (args.length>=(maxTabArgs+1)&&useMaxTab)) {
            return ImmutableList.of();
        }
        String name = args[args.length+-1].getContents();
        for(User u : CoreAPI.getOnlineUsers()) {
            l.add(u.getRealName());
        }
        if(allowOfflinePlayerTab)
            for(OfflineUser u : CoreAPI.getOfflineUsers()) {
                l.add(u.getRealName());
            }
        for(String u : l) {
            if(u.toLowerCase().startsWith(name.toLowerCase())) f.add(u);
        }
        return f;
    }

    protected List<String> loadTab(int length, Iterable<String> arguments) {
        return loadTab(length, true, arguments);
    }

    protected List<String> loadTab(int length, String prevArg, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        try {
            if(tabArgs.length == length && tabArgs[tabArgs.length+-2].equalsIgnoreCase(prevArg))
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getContents()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        try {
            if(tabArgs.length == length && isTrue)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getContents()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, String[] args, Iterable<String> arguments) {
        return loadTab(length, args, true, arguments);
    }


    protected List<String> loadTab(int length, Iterable<String> arguments, String replace) {
        List<String> returnList = new ArrayList<>();
        try {
            if(tabArgs.length == length)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getContents().substring(replace.length())))returnList.add(replace+s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, String[] args, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        try {
            boolean isTrue1 = false;
            for(int i = args.length; i > 0; i--) {
                if(tabArgs[tabArgs.length+-i].equalsIgnoreCase(args[args.length+-i]))
                    isTrue1=true;
                else
                    isTrue1=false;
            }
            if(tabArgs.length == length && isTrue && isTrue1)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getContents()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, ArgumentData data, Iterable<String> arguments) {
        return loadTab(length, data, true, arguments);
    }

    protected List<String> loadTab(int length, ArgumentData[] argsData, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        boolean check = false;
        try {
            for(ArgumentData data : argsData)
                if(data.index != -1) {
                    if(tabArgs[data.index].equalsIgnoreCase(data.name))
                        check = true;
                    //System.out.println(tabArgs[data.index].equalsIgnoreCase(data.name));
                }else
                    for(int c = 0; c < data.indexes.length; c++) {
                        int i = data.indexes[c];
                        if(tabArgs[i].equalsIgnoreCase(data.name[c]))
                            check = true;
                        else {
                            check = false;
                            break;
                        }
                    }
            if(tabArgs.length==length&&check&&isTrue)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getContents()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, ArgumentData data, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        boolean check = false;
        try {
            if(data.index != -1) {
                if(tabArgs[data.index].equalsIgnoreCase(data.name))
                    check = true;
                //System.out.println(tabArgs[data.index].equalsIgnoreCase(data.name));
            }else
                for(int c = 0; c < data.indexes.length; c++) {
                    int i = data.indexes[c];
                    if(tabArgs[i].equalsIgnoreCase(data.name[c]))
                        check = true;
                    else {
                        check = false;
                        break;
                    }
                }
            if(tabArgs.length==length&&check&&isTrue)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getContents()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected class ArgumentData {
        int index = -1;
        int[] indexes;
        String[] name;
        public ArgumentData(int index, String... name) {
            this.index = index;
            this.name = name;
        }
        public ArgumentData(int[] indexes, String... name) {
            this.indexes = indexes;
            this.name = name;
        }
    }

    protected List<String> forInt(int min, int max) {
        List<String> str = new ArrayList<>();
        int rmin, rmax;
        if(min < max) {
            rmin=min;
            rmax=max;
        }else {
            rmin=max;
            rmax=min;
        }
        for(int i = rmin; i < rmax; i++) {
            if(!str.contains(""+i)) str.add(""+i);
        }
        return str;
    }

    private boolean checkTab(String s1, String arg) {
        return s1.toLowerCase().startsWith(arg.toLowerCase());
    }

    protected void argsLengthError(boolean tooMany) throws CommandException {
        if(tooMany)
            throw new CommandException(CoreAPI.format("Commands.Error.too-Many-Arguments"));
        throw new CommandException(CoreAPI.format("Commands.Error.not-Enough-Arguments"));
    }

    protected GameMode getGamemode(Argument arg) throws CommandException {
        GameMode gm = null;
        if(!gamemodeExists(arg.getContents())) {
            if(arg.equalsIgnoreCase("s", "0"))
                gm = GameMode.SURVIVAL;
            if(arg.equalsIgnoreCase("c", "1"))
                gm = GameMode.CREATIVE;
            if(arg.equalsIgnoreCase("a", "2"))
                gm = GameMode.ADVENTURE;
            if(arg.equalsIgnoreCase("sp", "3"))
                gm = GameMode.SPECTATOR;
        }else
            gm = GameMode.valueOf(arg.getContents().toUpperCase());
        if(gm == null)
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-GameMode", arg.getContents()));
        return gm;
    }

    private boolean gamemodeExists(String text) {
        for(GameMode m : GameMode.values())
            if(text.equalsIgnoreCase(m.name()))
                return true;
        return false;
    }

    protected void sendMessageRaw(Object... messages) {
        s.sendMessage(messages);
    }

    protected void sendMessage(String location, Object... objects) {
        s.sendMessageFormat(location.startsWith("Commands.") ? location : "Commands."+location, objects);
    }
    protected void sendMessageEnd(String location, Object... objects) throws CommandException{
        s.sendMessageFormat(location.startsWith("Commands.") ? location : "Commands."+location, objects);
        throw new CommandException("");
    }

    protected void sendMessage(User t, String location, Object... objects) {
        t.sendMessageFormat(location.startsWith("Commands.") ? location : "Commands."+location, objects);
    }

    protected void hasPermission(String... permissions) throws CommandException {
        String pre = CoreAPI.getSettings().getString("settings.permissions-prefix.command").toLowerCase();
        String f = (permissions.length>0?permissions[0]:"");
        String x = (!f.startsWith(pre+".")?pre+"."+f:f);
        /*System.out.println();*/
        if(!s.hasPermission(getDefaultPermissions()) && !s.hasPermission(permissions))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Permission", x));
    }
    protected void isPermissionSet(OfflineUser u, String... permissions) throws CommandException {
        String pre = CoreAPI.getSettings().getString("settings.permissions-prefix.command").toLowerCase();
        if(!u.getPermissionData().isPermissionSet(getDefaultPermissions()) && !u.getPermissionData().isPermissionSet(permissions))
            throw new CommandException(CoreAPI.format("Commands.Error.permission-Is-Not-Set", permissions[0]));
    }

    protected void hasPermission(boolean invert, Group g, String... permissions) throws CommandException {
        String f = (permissions.length>0?permissions[0]:"Null");
        String x = (f);
        if(!invert && !g.hasPermission(permissions))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Permission-Group", x));
        if(invert && (g.hasPermission(permissions)))
            throw new CommandException(CoreAPI.format("Commands.Error.has-Permission-Group", x));
    }

    protected void hasPermission(boolean invert, OfflineUser g, String... permissions) throws CommandException {
        String f = (permissions.length>0?permissions[0]:"Null");
        String x = (f);
        if(!invert && !g.getPermissionData().hasSinglePermission(permissions))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Permission-User", x));
        if(invert && (g.getPermissionData().hasSinglePermission(permissions)))
            throw new CommandException(CoreAPI.format("Commands.Error.has-Permission-User", x));
    }


    protected boolean getBoolean(Argument arg) throws CommandException {
        if(arg.equalsIgnoreCase("true"))return true;
        if(arg.equalsIgnoreCase("false"))return false;
        throw new CommandException(CoreAPI.format("Commands.Error.invalid-Boolean", arg.getContents()));
    }

    protected boolean getBoolean(String path) throws CommandException {
        return CoreAPI.getMessages().getBoolean(path);
    }
    protected Config getConfig(Argument arg) throws CommandException {
        if(ConfigManager.getConfig(arg.getContents()) == null)
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Config", arg.getContents()));
        return ConfigManager.getConfig(arg.getContents());
    }

    protected Config getConfigMain(Argument arg) throws CommandException {
        if(ConfigManager.getConfigMain(arg.getContents()) == null)
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Config", arg.getContents()));
        return ConfigManager.getConfigMain(arg.getContents());
    }

    protected void sendUnknown() {
        new Message("[c]Unknown argument").send(s);
    }

    protected int integer(Argument arg) throws CommandException {
        if(!NumberUtil.isInt(arg.getContents()))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Number", arg.getContents()));
        return NumberUtil.getInt(arg.getContents());
    }

    protected double money(Argument arg) throws CommandException {
        if(!NumberUtil.isDouble(arg.getContents()))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Number", arg.getContents()));
        return NumberUtil.getMoney(arg.getContents());
    }

    protected double getDouble(Argument arg) throws CommandException {
        if(!NumberUtil.isDouble(arg.getContents()))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Number", arg.getContents()));
        return NumberUtil.getDouble(arg.getContents());
    }

    protected double getDouble(String arg) throws CommandException {
        if(!NumberUtil.isDouble(arg))
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Number", arg));
        return NumberUtil.getDouble(arg);
    }

    protected Group getGroup(Argument arg) throws CommandException {
        if(GroupManager.getGroup(arg.getContents()) == null)
            throw new CommandException(CoreAPI.format("Commands.Error.invalid-Group", arg.getContents()));
        return GroupManager.getGroup(arg.getContents());
    }

    protected EntityType getEntityType(Argument arg) throws CommandException {
        for(EntityType type : EntityType.values()) {
            if(type.name().equalsIgnoreCase(arg.getContents())) {
                return type;
            }
        }
        throw new CommandException(CoreAPI.format("Commands.Error.invalid-Entity", arg.getContents()));
    }

    protected void checkGroup(boolean exists, Argument arg) throws CommandException {
        if(exists) {
            if(GroupManager.getGroup(arg.getContents()) != null)
                throw new CommandException(CoreAPI.format("Commands.Error.group-Exists", arg.getContents()));
        }else {
            if(GroupManager.getGroup(arg.getContents()) == null)
                throw new CommandException(CoreAPI.format("Commands.Error.invalid-Group", arg.getContents()));
        }
    }

    protected boolean hasGroup(boolean invert, OfflineUser u, Group g) throws CommandException {
        if(invert) {
            if(!u.getPermissionData().getGroups().contains(g)) {
                throw new CommandException(CoreAPI.format("Commands.Error.User-Doesnt-Have.Group", u.getName()));
            }
        }
        else {
            if(u.getPermissionData().getGroups().contains(g)) {
                throw new CommandException(CoreAPI.format("Commands.Error.User-Has.Group", u.getName()));
            }
        }
        return true;
    }

    protected boolean hasGroup(boolean invert, Group u, Group g) throws CommandException {
        if(invert) {
            if(!u.getInherits().contains(g)) {
                throw new CommandException(CoreAPI.format("Commands.Error.Group-Doesnt-Inherit", u.getName()));
            }
        }
        else {
            if(u.getInherits().contains(g)) {
                throw new CommandException(CoreAPI.format("Commands.Error.Group-Inherits", u.getName()));
            }
        }
        return true;
    }

    protected Enchantment getEnchantment(Argument arg) throws CommandException {
        String name = arg.getContents();
        if(EnchantHandler.getEnchantment(name) != null) return EnchantHandler.getEnchantment(name);
        for(Enchantment x : Enchantment.values()) {
            if(ColorUtil.removeColor(x.getName()).equalsIgnoreCase(ColorUtil.removeColor(name))) return x;
            if(ColorUtil.removeColor(EnchantUtil.fixName(x)).equalsIgnoreCase(ColorUtil.removeColor(name))) return x;
            if(ColorUtil.removeColor(EnchantUtil.fixName(x).replace(" ", "")).equalsIgnoreCase(ColorUtil.removeColor(name))) return x;
            if(ColorUtil.removeColor(EnchantUtil.fixName(x).replace(" ", "_")).equalsIgnoreCase(ColorUtil.removeColor(name))) return x;
        }
        if(Enchantment.getByName(name) != null) return Enchantment.getByName(name);
        throw new CommandException(CoreAPI.format("Commands.Error.invalid-Enchantment", name));
    }

    protected OfflinePlayer getPlayer(boolean useCatch, Argument arg) throws CommandException {
        String name = arg.getContents();
        if(Bukkit.getPlayer(name)!=null)
            return Bukkit.getPlayer(name);
        if(Bukkit.getOfflinePlayer(name)!=null)
            return Bukkit.getOfflinePlayer(name);
        try {
            UUID uuid = PlayerData.getUUID(name);
            return Bukkit.getOfflinePlayer(uuid);
        } catch (Exception e) {
            if(useCatch)
                throw new CommandException(CoreAPI.format("Commands.Error.invalid-Player", name));
            else
                return null;
        }
    }

    protected String createMessage(int argsStart) {
        StringBuilder sb = new StringBuilder();
        for(int i = argsStart; i < args.length; i++) {
            sb.append(args[i].getContents()).append(" ");
        }
        return sb.toString().substring(0, sb.toString().length()+-1);
    }
    protected String createMessage(int argsStart, int argStop) {
        StringBuilder sb = new StringBuilder();
        if(argStop>args.length)argStop=args.length;
        if(argsStart>args.length)argsStart=args.length;
        for(int i = argsStart; i < argStop; i++) {
            sb.append(args[i].getContents()).append(" ");
        }
        return sb.toString().substring(0, sb.toString().length()+-1);
    }

    protected void mustBe(boolean player) throws CommandException {
        if(player && !s.isPlayer()) {
            throw new CommandException(CoreAPI.format("Commands.Error.SenderType.Player"));
        }else {
            if(!player && s.isPlayer()) {
                throw new CommandException(CoreAPI.format("Commands.Error.SenderType.Console"));
            }
        }
    }

    protected void specifyUser() throws CommandException {
        if(!s.isPlayer())
            throw new CommandException(CoreAPI.format("Commands.Error.specifyUser"));

    }

    protected Home getHome(OfflineUser user, Argument arg) throws CommandException {
        if(user.getHomeData().getHome(arg.getContents()) != null)return user.getHomeData().getHome(arg.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.invalid-Home", arg.getContents()));
    }

    protected OfflineUser getAnyUser(Argument name) throws CommandException{
        if(CoreAPI.getAllUser(name.getContents()) != null) return CoreAPI.getAllUser(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.InvalidUser.AnyUser", name.getContents()));
    }


    protected User getUser(Argument name) throws CommandException{
        if(CoreAPI.getUser(name.getContents()) != null) return CoreAPI.getUser(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.InvalidUser.Online", name.getContents()));
    }

    protected User getAllUser(Argument name) throws CommandException{
        if(CoreAPI.getUserAll(name.getContents()) != null) return CoreAPI.getUserAll(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.InvalidUser.Online", name.getContents()));
    }

    protected CommandSource getSource(Argument name) throws CommandException{
        if(CoreAPI.getCommandSource(name.getContents()) != null) return CoreAPI.getCommandSource(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.InvalidUser.Online", name.getContents()));
    }

    protected void checkUser(CommandSource u) throws CommandException{
        if(u == null)
            throw new CommandException(CoreAPI.format("Commands.Error.InvalidUser.Online", "not found"));
    }

    protected OfflineUser getOfflineUser(Argument name) throws CommandException{
        if(CoreAPI.getOfflineUser(name.getContents()) != null) return CoreAPI.getOfflineUser(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.InvalidUser.Offline", name.getContents()));
    }

    protected World getWorld(Argument name) throws CommandException{
        if(Bukkit.getWorld(name.getContents()) != null) return Bukkit.getWorld(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.invalid-World", name.getContents()));
    }

    protected void worldExists(Argument name) throws CommandException{
        if(Bukkit.getWorld(name.getContents()) == null) return;
        throw new CommandException(CoreAPI.format("Commands.Error.world-Exists", name.getContents()));
    }

    protected GameRule getGameRule(Argument name) throws CommandException{
        if(GameRule.getByName(name.getContents()) != null) return GameRule.getByName(name.getContents());
        throw new CommandException(CoreAPI.format("Commands.Error.invalid-GameRule", name.getContents()));
    }

    public String getPermission() {
        return permission;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getAliases() {
        return aliases;
    }

    public void setAliases(String[] aliases) {
        this.aliases = aliases;
    }

    public List<String> getAliasesAsList() {
        return Arrays.asList(aliases);
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }


    public HashMap<String, Object> parseOptions(HashMap<String, Class<?>> opts, Argument[] args) {
        return parseOptions(opts, 0, args);
    }
    public HashMap<String, Object> parseOptions(HashMap<String, Class<?>> opts, int start, Argument[] args) {
        HashMap<String, Class<?>> options = new HashMap<>();
        for(String s : opts.keySet()) {
            options.put(s.toLowerCase(), opts.get(s));
        }
        HashMap<String, Object> option = new HashMap<>();
        for(int i = start; i < args.length; i+=2) {
            try {
                String arg0 = args[i +- 1].getContents().toLowerCase();
                Object arg1 = args[i].getContents();
                if(options.containsKey(arg0) && (arg1.getClass().isInstance(options.get(arg0)) || options.get(arg0).isInstance(arg1)))
                    option.put(arg0, arg1);
            }catch(ArrayIndexOutOfBoundsException e) {
                break;
            }

        }
        return option;
    }

    private enum HelpType {

    }

}
