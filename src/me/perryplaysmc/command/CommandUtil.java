package me.perryplaysmc.command;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.topics.Index;
import me.perryplaysmc.topics.Topic;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.help.HelpTopic;

import java.util.*;

public class CommandUtil {
    
    
    private static List<HelpTopic> topics, topics2, aliases;
    private static Index cc;
    private static String name = CoreAPI.getSettings().getString("settings.main-name");
    public static void reloadTopics() {
        if(topics == null)topics = new ArrayList<>();
        if(topics2 == null)topics2 = new ArrayList<>();
        if(aliases == null)aliases = new ArrayList<>();
        HashMap<String, String> commands = new HashMap<>();
        for(me.perryplaysmc.command.Command cmd : CommandHandler.getCommands()) {
            commands.put(cmd.getName(), cmd.getDescription());
        }
        List<String> cmds = new ArrayList<>();
        for(String c : commands.keySet()) {
            if(!cmds.contains(c)) {
                cmds.add(c);
            }
        }
        Collections.sort(cmds);
        for(String c : cmds) {
            Topic e = new Topic(c, commands.get(c));
            if (!topics2.contains(e)) {
                topics2.add(e);
            }
        }
        if(cc == null && Bukkit.getHelpMap().getHelpTopic(name) == null) {
            cc = new Index(name, "Help for " + name, name.toLowerCase()+".help", topics2);
        }else if(cc == null && Bukkit.getHelpMap().getHelpTopic(name) != null) {
            cc = (Index) Bukkit.getHelpMap().getHelpTopic(name);
            
        }
        if(cc!=null){
            List<HelpTopic> top = new ArrayList<>();
            for (HelpTopic t : topics2) {
                if(!cc.getTopics().contains(t)&&!top.contains(t)) {
                    top.add(t);
                }
            }
            top.addAll(cc.getTopics());
            HashMap<String, String> t = new HashMap<>();
            List<HelpTopic> ts = new ArrayList<>();
            for (me.perryplaysmc.command.Command cmd : CommandHandler.getCommands()) {
                t.put(cmd.getName(), cmd.getDescription());
            }
            List<String> keys = new ArrayList<>(t.keySet());
            Collections.sort(keys);
            for (String a : keys) {
                ts.add(new Topic("/" + a, t.get(a)));
            }
            cc.setTopics(ts);
        }
        
        if(!Bukkit.getHelpMap().getHelpTopics().contains(cc)) {
            Bukkit.getHelpMap().addTopic(cc);
        }
        HashMap<String, List<String>> tz = new HashMap<>();
        SimpleCommandMap cMap = Core.getAPI().getCommandMap();
        for(Command cmd : cMap.getCommands()) {
            List<String> al = cmd.getAliases();
            Collections.sort(al);
            tz.put(cmd.getName(), al);
        }
        List<String> terpix = new ArrayList<>();
        for(Map.Entry<String, List<String>> entry : tz.entrySet()) {
            for(String a : entry.getValue()) {
                String e = "/" + a + "::§eAlias for §6/" + entry.getKey();
                if(!terpix.contains(e)) {
                    terpix.add(e);
                }
            }
        }
        Collections.sort(terpix);
        List<HelpTopic> topix = new ArrayList<>();
        for(String t : terpix) {
            Topic e = new Topic(t.split("::")[0], t.split("::")[1]);
            if(!topix.contains(e))
                topix.add(e);
        }
        Index ca = null;
        if(ca == null && Bukkit.getHelpMap().getHelpTopic("Aliases") != null) {
            ca = (Index) Bukkit.getHelpMap().getHelpTopic("Aliases");
        }else if(ca == null && Bukkit.getHelpMap().getHelpTopic("Aliases") == null) {
            ca = new Index("Aliases", "", "", topix);
        }
        List<HelpTopic> top = new ArrayList<>();
        for(HelpTopic t : topics2) {
            if(!cc.getTopics().contains(t)) {
                top.add(t);
            }
        }
        top.addAll(cc.getTopics());
        for(HelpTopic tr : getCoreCommands()) {
            if(topix.contains(tr)) continue;
            topix.add(tr);
        }
        
        
        ca.setTopics(topix);
        
        
        Bukkit.getHelpMap().getHelpTopics().remove(ca);
        Bukkit.getHelpMap().addTopic(ca);
    }
    
    
    
    private static List<HelpTopic> getCoreCommands() {
        HashMap<String, List<String>> t = new HashMap<>();
        List<HelpTopic> ts = new ArrayList<>();
        for(me.perryplaysmc.command.Command cmd : CommandHandler.getCommands()) {
            List<String> al = cmd.getAliasesAsList();
            Collections.sort(al);
            t.put(cmd.getName(), al);
        }
        List<String> keys = new ArrayList<>();
        for(String a : t.keySet()) {
            keys.add(a);
        }
        Collections.sort(keys);
        for(String a : keys) {
            for(String e : t.get(a)) {
                ts.add(new Topic("/" + e, "§eAliase for §6/"+a));
            }
        }
        return ts;
    }
    
    
}
