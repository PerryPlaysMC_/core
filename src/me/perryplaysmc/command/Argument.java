package me.perryplaysmc.command;

import java.util.ArrayList;
import java.util.List;

public class Argument {

    private String name;
    private Object value;
    private String contains = "";

    public Argument(String name) {
        this.name = (name);
        this.value=name;
    }

    public boolean equalsIgnoreCase(String... aliases) {
        for(String a : aliases)
            if(getContents().equalsIgnoreCase(a.replaceAll(" ", "_")))return true;
        return false;
    }

    public Argument[] split(String split) {
        List<Argument> args = new ArrayList<>();
        if(getContents().contains(split))
            for(String s : name.split(split))
                args.add(new Argument(s));
        else
            args.add(this);
        return args.toArray(new Argument[args.size()]);
    }

    public Object getValue(){
        return value;
    }

    public Argument toLowerCase() {
        return new Argument(name.toLowerCase());
    }

    public Argument subStringEnd(int end) {
        return new Argument(name.substring(0, end));
    }

    public boolean startsWith(Iterable<String> possibilities) {
        List<String> str = new ArrayList<>();
        for(String s : possibilities) {
            if(!str.contains(s))
                str.add(s);
        }
        return startsWith(str.toArray(new String[str.size()]));
    }

    public boolean startsWith(String... possibilities) {
        for(String a : possibilities)
            if(getContents().toLowerCase().startsWith(a.toLowerCase()))return true;
        return false;

    }

    public boolean contains(String... contains) {
        for(String s : contains) {
            if(getContents().toLowerCase().contains(s.toLowerCase())){
                this.contains = s.toLowerCase();
                return true;
            }
        }
        return false;
    }


    public String getContents() {
        return (name);
    }

    public int length() {
        return getContents().length();
    }

    public char charAt(int index) {
        return getContents().charAt(index);
    }

    public CharSequence subSequence(int start, int end) {
        return getContents().subSequence(start, end);
    }


    public String subString(int i) {
        return getContents().substring(i);
    }

    public String subString(int i, int j) {
        return getContents().substring(i, j);
    }

    public String getContains() {
        return contains;
    }
}
