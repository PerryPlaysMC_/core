package me.perryplaysmc.utils.helper;

import com.sun.istack.internal.NotNull;
import me.perryplaysmc.core.Core;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.IOUtils;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.plugin.*;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;

public class SimplePluginHelper implements PluginHelper {

    PluginManager pm = Bukkit.getPluginManager();

    @Override
    public Set<Plugin> getPlugins() {
        return new HashSet<>(Arrays.asList(pm.getPlugins()));
    }

    @Override
    public Plugin getPlugin(String name) {
        for(Plugin pl : getPlugins()) {
            if(pl.getName().equalsIgnoreCase(name)) {
                return pl;
            }
        }
        return null;
    }

    @Override
    public PluginManager getPluginManager() {
        return pm;
    }

    @Override
    public PluginHelper registerListener(Listener... listeners) {
        for(Listener l : listeners)
            pm.registerEvents(l, Core.getPlugin(Core.class));
        return this;
    }

    @Override
    public boolean isEnabled(Plugin pl) {
        return getPlugin(pl.getName())!=null&&getPlugin(pl.getName()).isEnabled();
    }

    @Override
    public boolean isDisabled(Plugin pl) {
        return getPlugin(pl.getName())!=null&&!getPlugin(pl.getName()).isEnabled();
    }
    
    @Override
    public boolean isEnabled(String pl) {
        return getPlugin(pl)!=null&&getPlugin(pl).isEnabled();
    }
    
    @Override
    public boolean isDisabled(String pl) {
        return getPlugin(pl)!=null&&!getPlugin(pl).isEnabled();
    }
    
    @Override
    public String loadPlugin(String plugin) {
        return loadPlugin("plugins", plugin);
    }

    @Override
    public String loadPlugin(String folder, String plugin) {

        Plugin target = null;

        File pluginDir = new File(folder);
        PluginManager pm =  Bukkit.getPluginManager();
        if (!pluginDir.isDirectory()) {
            return "[p] Plugins folder is invalid";
        }
        File pluginFile = new File(pluginDir, plugin.endsWith(".jar") ? plugin : plugin + ".jar");
        PluginDescriptionFile pdf = null;
        if (!pluginFile.isFile()) {
            for (File f : pluginDir.listFiles()) {
                if (f.getName().endsWith(".jar")) {
                    PluginDescriptionFile desc = getPluginDescriptionBukkit(f);
                    if(desc.getName().isEmpty()) continue;
                    if (desc.getName().equalsIgnoreCase(plugin)) {
                        if(isEnabled(getPlugin(desc.getName()))) {
                            return "[pc]MPL[c]: That plugin is already enabled.";
                        }
                        pluginFile = f;
                        break;
                    }
                }
            }
        }
        if(!pluginFile.exists() || pluginFile == null) {
            return "[p] Invalid plugin name [pc]" + plugin;
        }

        try {
            //, PluginDescriptionFile description, File dataFolder, File file
            target = pm.loadPlugin(pluginFile);
        } catch (Exception e) {
            e.printStackTrace();
            return "[p] Invalid plugin.yml [pc]" + plugin + "";
        }

        if(target != null) {
            target.onLoad();
            pm.enablePlugin(target);
        }
        return "[p] Successfully loaded [pc]" + plugin + "";
    }

    @Override
    public String disablePlugin(String plugin) {
        return getPlugin(plugin) != null ? disablePlugin(getPlugin(plugin)) : "[p]Invalid plugin[c]&l»&r [pc]"+plugin+"[c]";
    }

    @Override
    public String disablePlugin(Plugin plugin) {
        String name = plugin.getName();

        PluginManager pluginManager = Bukkit.getPluginManager();

        SimpleCommandMap commandMap = null;

        List<Plugin> plugins = null;

        Map<String, Plugin> names = null;
        Map<String, Command> commands = null;
        Map<Event, SortedSet<RegisteredListener>> listeners = null;

        boolean reloadlisteners = true;

        if (pluginManager != null) {

            pluginManager.disablePlugin(plugin);

            try {

                Field pluginsField = Bukkit.getPluginManager().getClass().getDeclaredField("plugins");
                pluginsField.setAccessible(true);
                plugins = (List<Plugin>) pluginsField.get(pluginManager);

                Field lookupNamesField = Bukkit.getPluginManager().getClass().getDeclaredField("lookupNames");
                lookupNamesField.setAccessible(true);
                names = (Map<String, Plugin>) lookupNamesField.get(pluginManager);

                try {
                    Field listenersField = Bukkit.getPluginManager().getClass().getDeclaredField("listeners");
                    listenersField.setAccessible(true);
                    listeners = (Map<Event, SortedSet<RegisteredListener>>) listenersField.get(pluginManager);
                } catch (Exception e) {
                    reloadlisteners = false;
                }

                Field commandMapField = Bukkit.getPluginManager().getClass().getDeclaredField("commandMap");
                commandMapField.setAccessible(true);
                commandMap = (SimpleCommandMap) commandMapField.get(pluginManager);

                Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
                knownCommandsField.setAccessible(true);
                commands = (Map<String, Command>) knownCommandsField.get(commandMap);

            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
                Logger.getLogger("Minecraft").log(Level.SEVERE, null, e);
            }

        }

        pluginManager.disablePlugin(plugin);

        if (plugins != null && plugins.contains(plugin))
            plugins.remove(plugin);

        if (names != null && names.containsKey(name))
            names.remove(name);

        if (listeners != null && reloadlisteners) {
            for (SortedSet<RegisteredListener> set : listeners.values()) {
                set.removeIf(value -> value.getPlugin() == plugin);
            }
        }

        if (commandMap != null) {
            for (Iterator<Map.Entry<String, Command>> it = commands.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, Command> entry = it.next();
                if (entry.getValue() instanceof PluginCommand) {
                    PluginCommand c = (PluginCommand) entry.getValue();
                    if (c.getPlugin() == plugin) {
                        c.unregister(commandMap);
                        it.remove();
                    }
                }
            }
        }

        // Attempt to close the classloader to unlock any handles on the plugin's jar file.
        ClassLoader cl = plugin.getClass().getClassLoader();

        if (cl instanceof URLClassLoader) {

            try {

                Field pluginField = cl.getClass().getDeclaredField("plugin");
                pluginField.setAccessible(true);
                pluginField.set(cl, null);

                Field pluginInitField = cl.getClass().getDeclaredField("pluginInit");
                pluginInitField.setAccessible(true);
                pluginInitField.set(cl, null);

            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger("Minecraft").log(Level.SEVERE, null, ex);
            }

            try {
                ((URLClassLoader) cl).close();
            } catch (IOException ex) {
                Logger.getLogger("Minecraft").log(Level.SEVERE, null, ex);
                return "[p]An internal error occurred, please check console for more info, or latest.log";                }

        }

        // Will not work on processes started with the -XX:+DisableExplicitGC flag, but lets try it anyway.
        // This tries to get around the issue where Windows refuses to unlock jar files that were previously loaded into the JVM.
        System.gc();

        return "[p]Successfully unloaded [pc]" + name + "";

    }


    private static void writeJarEntry(JarOutputStream jos, String path, File f) throws IOException, FileNotFoundException {
        JarEntry je = new JarEntry(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = new FileInputStream(f);
        try {
            IOUtils.copy(is, baos);
        } finally {
            is.close();
        }
        byte[] data = baos.toByteArray();
        je.setSize(data.length);
        CRC32 crc = new CRC32();
        crc.update(data);
        je.setCrc(crc.getValue());
        jos.putNextEntry(je);
        jos.write(data);
    }

    @NotNull
    public PluginDescriptionFile getPluginDescriptionBukkit(@NotNull File file) {
        Validate.notNull(file, "File cannot be null");
        JarFile jar = null;
        InputStream stream = null;

        PluginDescriptionFile var6;
        try {
            try {
                jar = new JarFile(file);
            } catch (IOException e) {
                return null;
            }
            JarEntry entry = jar.getJarEntry("plugin.yml");
            if (entry == null) {
                return null;
            }

            try {
                stream = jar.getInputStream(entry);
            } catch (IOException e) {
                return null;
            }
            try {
                var6 = new PluginDescriptionFile(stream);
            } catch (InvalidDescriptionException e) {
                return null;
            }
        }  finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException var16) {
                }
            }

            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException var15) {
                }
            }

        }

        return var6;
    }

    public PluginDescriptionFile getPluginDescription(File file) throws InvalidDescriptionException {
        Validate.notNull(file, "File cannot be null");

        JarFile jar = null;
        InputStream stream = null;

        try {
            jar = new JarFile(file);
            JarEntry entry = jar.getJarEntry("miniverse.yml");
            JarEntry entry2 = jar.getJarEntry("mini.yml");
            boolean check = false;
            if(entry == null) {
                check = true;
                if(entry2 == null) {
                    if(check) {
                        throw new InvalidDescriptionException(new FileNotFoundException("Jar does not contain mini.yml"));
                    }
                } else {
                    check = false;
                }
                if(check) {
                    throw new InvalidDescriptionException(new FileNotFoundException("Jar does not contain miniverse.yml"));
                }
            }
            JarEntry en = entry == null ? entry2 : entry;
            writeJarEntry(new JarOutputStream(new FileOutputStream(file),new Manifest()), "miniverse", new File("plugin.yml"));
            FileUtils.copyFile(new File(en.getName()), new File("plugin.yml"));
            en = jar.getJarEntry("plugin.yml");
            stream = jar.getInputStream(en);


            return new PluginDescriptionFile(stream);

        } catch (IOException ex) {
            throw new InvalidDescriptionException(ex);
        } catch (YAMLException ex) {
            throw new InvalidDescriptionException(ex);
        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException e) {
                }
            }
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                }
            }
        }
    }
}


