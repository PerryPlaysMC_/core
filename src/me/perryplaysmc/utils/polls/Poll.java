package me.perryplaysmc.utils.polls;

import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.NumberUtil;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

public class Poll {
    
    private String name;
    private List<String> topics;
    private long time;
    private HashMap<String, Integer> voted;
    private HashMap<User, String> votedUsers;
    
    public Poll(String name, List<String> topics) {
        this(name, 120, topics);
    }
    
    public Poll(String name, long time, List<String> topics) {
        this.name = name;
        this.topics = topics;
        this.time = time;
        this.voted = new HashMap<>();
        this.votedUsers = new HashMap<>();
        for(String a : topics) {
            voted.put(a, 0);
        }
        PollManager.addPoll(this);
    }
    
    public String getName() {
        return name;
    }
    
    public List<String> getTopics() {
        return topics;
    }
    
    public boolean hasTopic(String topic) {
        for(String a : topics) {
            if(a.equalsIgnoreCase(topic.toLowerCase())) return true;
        }
        return false;
    }
    
    public void vote(User u, String topic) {
        if(hasTopic(topic)) {
            int amount = voted.containsKey(topic) ? voted.get(topic)+1 : 1;
            voted.put(topic, amount);
            if(votedUsers.containsKey(u)) {
                voted.put(votedUsers.get(u), voted.get(votedUsers.get(u))+-1);
            }
            votedUsers.put(u, topic);
        }
    }
    
    public String getResults() {
        StringBuilder sb = new StringBuilder("[c]Poll Results:");
        DecimalFormat fm = new DecimalFormat("##0.##");
        for(String topic : topics) {
            sb.append("\n &b-&3 ").append(topic).append("&b-&3").append(" ")
                    .append(fm.format(NumberUtil.getPercent(voted.get(topic).doubleValue(), (double) votedUsers.keySet().size()))).append("%");
        }
        return sb.toString();
    }
    
    public long getTime() {
        return time;
    }
    
    public void setTime(long time) {
        this.time = time;
    }
}

