package me.perryplaysmc.utils.inventory.ingorish;

import me.perryplaysmc.core.abstraction.AbstractionHandler;
import me.perryplaysmc.core.abstraction.ItemDataUtilHelper;
import me.perryplaysmc.utils.inventory.NBTTag;
import me.perryplaysmc.utils.inventory.TagType;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/6/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CustomItem {
    
    HashMap<String, NBTTag> data = new HashMap<>();
    ItemStack item;
    ItemMeta im;
    private ItemDataUtilHelper helper;
    
    public CustomItem(ItemStack item) {
        this.item = item;
        im = item.getItemMeta();
        helper = AbstractionHandler.getItemDataUtilHelper();
        data = helper.getData(item);
    }
    
    public static CustomItem setItem(ItemStack item) {
        return new CustomItem(item);
    }
    
    public static CustomItem getData(ItemStack item) {
        return CustomItem.setItem(item);
    }
    
    public NBTTag get(String key) {
        if(data.containsKey(key)) return data.get(key);
        return null;
    }
    public String getString(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.STRING) return (String)data.get(key).getValue();
        return "";
    }
    public double getDouble(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.DOUBLE)return (double) data.get(key).getValue();
        return 0.0;
    }
    public int getInt(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.INT) return (int)data.get(key).getValue();
        return 0;
    }
    public boolean getBoolean(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (boolean)data.get(key).getValue();
        return false;
    }
    public long getLong(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (long)data.get(key).getValue();
        return 0L;
    }
    public short getShort(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (short) data.get(key).getValue();
        return 0;
    }
    public byte getByte(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (byte)data.get(key).getValue();
        return 0;
    }
    public float getFloat(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (float) data.get(key).getValue();
        return 0;
    }
    public byte[] getByteArray(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (byte[])data.get(key).getValue();
        return new byte[] {0};
    }
    public int[] getIntArray(String key) {
        if(data.containsKey(key) && data.get(key).getType() == TagType.BOOLEAN) return (int[])data.get(key).getValue();
        return new int[]{0};
    }
    
    public boolean hasKey(String key) {
        return data.containsKey(key);
    }
    
    public CustomItem setString(String key, String value) {
        data.put(key, new NBTTag(TagType.STRING, value));
        return this;
    }
    public CustomItem setDouble(String key, double value) {
        data.put(key, new NBTTag(TagType.DOUBLE, value));
        return this;
    }
    public CustomItem setInt(String key, int value) {
        data.put(key, new NBTTag(TagType.INT, value));
        return this;
    }
    public CustomItem setBoolean(String key, boolean value) {
        data.put(key, new NBTTag(TagType.BOOLEAN, value));
        return this;
    }
    public CustomItem setLong(String key, long value) {
        data.put(key, new NBTTag(TagType.LONG, value));
        return this;
    }
    public CustomItem setShort(String key, short value) {
        data.put(key, new NBTTag(TagType.SHORT, value));
        return this;
    }
    public CustomItem setFloat(String key, float value) {
        data.put(key, new NBTTag(TagType.FLOAT, value));
        return this;
    }
    public CustomItem setByteArray(String key, Byte[] value) {
        data.put(key, new NBTTag(TagType.BYTE_ARRAY, value));
        return this;
    }
    public CustomItem setIntArray(String key, Integer[] value) {
        data.put(key, new NBTTag(TagType.INT_ARRAY, value));
        return this;
    }
    
    public CustomItem setDisplayName(String name) {
        return setDisplayName(false, name);
    }
    public CustomItem setDisplayName(boolean translateColor, String name) {
        if(translateColor) {
            im.setDisplayName(StringUtils.translate(name));
            return this;
        }
        im.setDisplayName(ColorUtil.translateColors('&', name));
        return this;
    }
    public CustomItem setLore(String... lore) {
        return setLore(false, lore);
    }
    public CustomItem setLore(boolean miniColor, String... lore) {
        List<String> a = new ArrayList<>();
        for(String i : lore) {
            if(miniColor) {
                a.add(StringUtils.translate(i));
                continue;
            }
            a.add(ColorUtil.translateColors('&',i));
        }
        im.setLore(a);
        return this;
    }
    
    public ItemStack finish() {
        item.setItemMeta(im);
        if(data.size()>0)
            item = helper.finish(item);
        return item;
    }
    
}
