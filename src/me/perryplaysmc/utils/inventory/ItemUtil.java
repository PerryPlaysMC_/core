package me.perryplaysmc.utils.inventory;

import me.perryplaysmc.core.abstraction.AbstractionHandler;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemUtil {
    
    
    public static Config cfg;
    private static Core core;
    public static double getVersion() {
        String vString = getVersionStr().replace("v", "");
        double v = 0;
        if (!vString.isEmpty()){
            String[] array = vString.split("_");
            v = Double.parseDouble(array[0] + "." + array[1]);
        }
        return v;
    }
    
    public static boolean isAir(ItemStack stack) {
        if(Version.isVersionHigherThan(true, Versions.v1_13))
            return stack == null || stack.getType()==Material.AIR || stack.getType()==Material.CAVE_AIR || stack.getType()==Material.VOID_AIR;
        return stack == null || stack.getType()==Material.AIR;
    }
    
    private static String getVersionStr(){
        String[] array = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",");
        if (array.length == 4)
            return array[3] + ".";
        return "";
    }
    
    public static void loadData() {
        core = Core.getAPI();
        cfg = new Config("items.yml");
        for(Material m : Material.values()) {
            if(cfg.get("Materials." + m.name().toLowerCase()) == null) {
                String na = m.name().toLowerCase();
//				if(cfg.get("Materials." + na + ".id") == null) {
//					cfg.set("Materials." + na + ".id", m.getId());
//				}
                
                if(Version.isVersionHigherThan(false, Versions.v1_13)){
                    if(cfg.get("Materials." + na + ".isLegacy") == null) {
                        cfg.set("Materials." + na + ".isLegacy", m.isLegacy());
                    }
                }
                if(cfg.get("Materials." + na + ".returnType") == null) {
                    cfg.set("Materials." + na + ".returnType", m.name());
                }
            }
            //NO UNDERSCORE
            if(cfg.get("Materials." + m.name().toLowerCase().replace("_", "")) == null) {
                String na = m.name().toLowerCase().replace("_", "");
//				if(cfg.get("Materials." + na + ".id") == null) {
//					cfg.set("Materials." + na + ".id", m.getId());
//				}
                
                if(getVersion() == 1.13){
                    if(cfg.get("Materials." + na + ".isLegacy") == null) {
                        cfg.set("Materials." + na + ".isLegacy", m.isLegacy());
                    }
                }
                if(cfg.get("Materials." + na + ".returnType") == null) {
                    cfg.set("Materials." + na + ".returnType", m.name());
                }
            }
        }
    }
    
    public static ItemStack getItem(String name) {
        Material m = null;
        name = ColorUtil.removeColor(name);
        if(name == null)
            return null;
        if(name.isEmpty())
            return null;
        
        if(cfg.getSection("Materials").contains(name.toLowerCase())) {
            m = Material.getMaterial(cfg.getString("Materials." + name.toLowerCase() + ".returnType"),
                    cfg.getBoolean("Materials." + name.toLowerCase() + ".isLegacy"));
            if(m == null) m = Material.matchMaterial(cfg.getString("Materials." + name.toLowerCase()));
            return new ItemStack(m, 1);
        }
        
        if(Material.getMaterial(name.toUpperCase()) != null) {
            return new ItemStack(Material.getMaterial(name.toUpperCase()), 1);
        }
        if(Material.matchMaterial(name.toUpperCase()) != null) {
            return new ItemStack(Material.matchMaterial(name.toUpperCase()), 1);
        }
        return null;
    }
    
    public static ItemStack getItem(String name, int quantity) {
        ItemStack i = getItem(name);
        i.setAmount(quantity);
        return i;
    }
    
    public static ItemStack getItem(String name, int quantity, int data) {
        ItemStack r = getItem(name, quantity);
        return new ItemStack(r.getType(), quantity, (short)0, (byte)data);
    }
    
    public static String convertItemStackToJson(ItemStack itemStack) {
        return AbstractionHandler.getItemStackJSONConverter().convertItemStack(itemStack);
    }
    
    
}
