package me.perryplaysmc.utils.inventory.shops.events;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.shops.GUIShop;
import me.perryplaysmc.utils.inventory.shops.GUIShopHandler;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShopInteract implements org.bukkit.event.Listener {
    public static Inventory inv;
    private static Config cfg = CoreAPI.getSettings();
    String invName =  StringUtils.translate(cfg.getString("DisplayName"));
    public static void load() {
        List<ItemStack> items = new ArrayList<>();
        List<String> shops = new ArrayList<>();
        for (GUIShop gui : GUIShopHandler.getShops()) {
                shops.add(gui.getName());
        }
        Collections.sort(shops);

        for (String guiz : shops) {
            GUIShop gui = GUIShopHandler.getShop(guiz);
            try {
                if (!gui.isNext()) {
                    //inv.addItem(gui.getDisplay());
                    items.add(gui.getDisplay());
                    CoreAPI.debug("Adding GUI to inventory: " + gui.getName() + "/" + gui.getDisplayName());
                }
                else {
                    CoreAPI.debug("Adding GUI: " + gui.getName() + "/" + gui.getDisplayName() + " is Next, skipping...");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(items.size() < 54) {
            inv = Bukkit.createInventory(null, 54, StringUtils.translate(cfg.getString("DisplayName")));
        }
        if(items.size() < 45) {
            inv = Bukkit.createInventory(null, 45, StringUtils.translate(cfg.getString("DisplayName")));
        }
        if(items.size() < 36) {
            inv = Bukkit.createInventory(null, 36, StringUtils.translate(cfg.getString("DisplayName")));
        }
        if(items.size() < 27) {
            inv = Bukkit.createInventory(null, 27, StringUtils.translate(cfg.getString("DisplayName")));
        }
        if(items.size() < 18) {
            inv = Bukkit.createInventory(null, 18, StringUtils.translate(cfg.getString("DisplayName")));
        }
        if(items.size() < 9) {
            inv = Bukkit.createInventory(null, 9, StringUtils.translate(cfg.getString("DisplayName")));
        }
        inv.addItem(items.toArray(new ItemStack[items.size()]));
        System.out.println("Loaded: " + inv.getContents().length);
    }

    public ShopInteract() {
        load();
    }

    public static void showInventory(User u) {
        u.openInventory(inv);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        ItemStack current;
        if (!e.isCancelled()) {
            if ((e.getSlot() <= -1) || (e.getClickedInventory() == null) || (!e.getView().getTitle().equalsIgnoreCase(invName))) return;
            e.setCancelled(true);
            if (e.getCurrentItem() == null) return;
            e.setCancelled(true);
            if (!e.getCurrentItem().hasItemMeta()) return;
            e.setCancelled(true);
            current = e.getCurrentItem();
            GUIShop s = GUIShopHandler.getShop(current.getItemMeta().getDisplayName());
            if (s != null) {
                e.getWhoClicked().openInventory(s.getInventory());
                return;
            }
            for (GUIShop shop : GUIShopHandler.getShops()) {
                if (current.isSimilar(shop.getDisplay())) {
                    e.getWhoClicked().openInventory(shop.getInventory());
                    break;
                }
            }
        }
    }
}
