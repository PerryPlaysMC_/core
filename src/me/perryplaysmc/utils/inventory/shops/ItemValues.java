package me.perryplaysmc.utils.inventory.shops;

import org.bukkit.inventory.ItemStack;

public class ItemValues {
  private ItemStack stack;
  private double buy;
  private double sell;
  
  public ItemValues(ItemStack stack, double buy, double sell) {
    this.stack = stack;
    this.buy = buy;
    this.sell = sell;
  }
  
  public ItemStack getStack()
  {
    return stack;
  }
  
  public double getSell() {
    return sell;
  }
  
  public double getBuy() {
    return buy;
  }
}
