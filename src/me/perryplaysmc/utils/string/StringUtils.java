package me.perryplaysmc.utils.string;

import me.perryplaysmc.command.Argument;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.util.ChatPaginator;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("all")
public class StringUtils {
    
    private static String[] fancyAlphabetLowerCase = new String[] {
            "Ⓐ", "ⓐ",
            "Ⓑ", "ⓑ",
            "Ⓒ", "ⓒ",
            "Ⓓ", "ⓓ",
            "Ⓔ", "ⓔ",
            "Ⓕ", "ⓕ",
            "Ⓖ", "ⓖ",
            "Ⓗ", "ⓗ",
            "Ⓘ", "ⓘ",
            "Ⓙ", "ⓙ",
            "Ⓚ", "ⓚ",
            "Ⓛ", "ⓛ",
            "Ⓜ", "ⓜ",
            "Ⓝ", "ⓝ",
            "Ⓞ", "ⓞ",
            "Ⓟ", "ⓟ",
            "Ⓠ", "ⓠ",
            "Ⓡ", "ⓡ",
            "Ⓢ", "ⓢ",
            "Ⓣ", "ⓣ",
            "Ⓤ", "ⓤ",
            "Ⓥ", "ⓥ",
            "Ⓦ", "ⓦ",
            "Ⓧ", "ⓧ",
            "Ⓨ", "ⓨ",
            "Ⓩ", "ⓩ"
        
    };
    
    static String[] newFancyText = new String[]{
            "⓪",
            "①",
            "②",
            "③",
            "④",
            "⑤",
            "⑥",
            "⑦",
            "⑧",
            "⑨"
    };
    
    public static String toFancy(String text) {
        String ret = "";
        for(char c : text.toCharArray()) {
            A:switch (c) {
                case 'A':
                    ret += fancyAlphabetLowerCase[0];
                    break A;
                case 'a':
                    ret += fancyAlphabetLowerCase[1];
                    break A;
                case 'B':
                    ret += fancyAlphabetLowerCase[2];
                    break A;
                case 'b':
                    ret += fancyAlphabetLowerCase[3];
                    break A;
                case 'C':
                    ret += fancyAlphabetLowerCase[4];
                    break A;
                case 'c':
                    ret += fancyAlphabetLowerCase[5];
                    break A;
                case 'D':
                    ret += fancyAlphabetLowerCase[6];
                    break A;
                case 'd':
                    ret += fancyAlphabetLowerCase[7];
                    break A;
                case 'E':
                    ret += fancyAlphabetLowerCase[8];
                    break A;
                case 'e':
                    ret += fancyAlphabetLowerCase[9];
                    break A;
                case 'F':
                    ret += fancyAlphabetLowerCase[10];
                    break A;
                case 'f':
                    ret += fancyAlphabetLowerCase[11];
                    break A;
                case 'G':
                    ret += fancyAlphabetLowerCase[12];
                    break A;
                case 'g':
                    ret += fancyAlphabetLowerCase[13];
                    break A;
                case 'H':
                    ret += fancyAlphabetLowerCase[14];
                    break A;
                case 'h':
                    ret += fancyAlphabetLowerCase[15];
                    break A;
                case 'I':
                    ret += fancyAlphabetLowerCase[16];
                    break A;
                case 'i':
                    ret += fancyAlphabetLowerCase[17];
                    break A;
                case 'J':
                    ret += fancyAlphabetLowerCase[18];
                    break A;
                case 'j':
                    ret += fancyAlphabetLowerCase[19];
                    break A;
                case 'K':
                    ret += fancyAlphabetLowerCase[20];
                    break A;
                case 'k':
                    ret += fancyAlphabetLowerCase[21];
                    break A;
                case 'L':
                    ret += fancyAlphabetLowerCase[22];
                    break A;
                case 'l':
                    ret += fancyAlphabetLowerCase[23];
                    break A;
                case 'M':
                    ret += fancyAlphabetLowerCase[24];
                    break A;
                case 'm':
                    ret += fancyAlphabetLowerCase[25];
                    break A;
                case 'N':
                    ret += fancyAlphabetLowerCase[26];
                    break A;
                case 'n':
                    ret += fancyAlphabetLowerCase[27];
                    break A;
                case 'O':
                    ret += fancyAlphabetLowerCase[28];
                    break A;
                case 'o':
                    ret += fancyAlphabetLowerCase[29];
                    break A;
                case 'P':
                    ret += fancyAlphabetLowerCase[30];
                    break A;
                case 'p':
                    ret += fancyAlphabetLowerCase[31];
                    break A;
                case 'Q':
                    ret += fancyAlphabetLowerCase[32];
                    break A;
                case 'q':
                    ret += fancyAlphabetLowerCase[33];
                    break A;
                case 'R':
                    ret += fancyAlphabetLowerCase[34];
                    break A;
                case 'r':
                    ret += fancyAlphabetLowerCase[35];
                    break A;
                case 'S':
                    ret += fancyAlphabetLowerCase[36];
                    break A;
                case 's':
                    ret += fancyAlphabetLowerCase[37];
                    break A;
                case 'T':
                    ret += fancyAlphabetLowerCase[38];
                    break A;
                case 't':
                    ret += fancyAlphabetLowerCase[39];
                    break A;
                case 'U':
                    ret += fancyAlphabetLowerCase[40];
                    break A;
                case 'u':
                    ret += fancyAlphabetLowerCase[41];
                    break A;
                case 'V':
                    ret += fancyAlphabetLowerCase[42];
                    break A;
                case 'v':
                    ret += fancyAlphabetLowerCase[43];
                    break A;
                case 'W':
                    ret += fancyAlphabetLowerCase[44];
                    break A;
                case 'w':
                    ret += fancyAlphabetLowerCase[45];
                    break A;
                case 'X':
                    ret += fancyAlphabetLowerCase[46];
                    break A;
                case 'x':
                    ret += fancyAlphabetLowerCase[47];
                    break A;
                case 'Y':
                    ret += fancyAlphabetLowerCase[48];
                    break A;
                case 'y':
                    ret += fancyAlphabetLowerCase[49];
                    break A;
                case 'Z':
                    ret += fancyAlphabetLowerCase[50];
                    break A;
                case 'z':
                    ret += fancyAlphabetLowerCase[51];
                    break A;
                case '0':
                    ret += newFancyText[0];
                    break A;
                case '1':
                    ret += newFancyText[1];
                    break A;
                case '2':
                    ret += newFancyText[2];
                    break A;
                case '3':
                    ret += newFancyText[3];
                    break A;
                case '4':
                    ret += newFancyText[4];
                    break A;
                case '5':
                    ret += newFancyText[5];
                    break A;
                case '6':
                    ret += newFancyText[6];
                    break A;
                case '7':
                    ret += newFancyText[7];
                    break A;
                case '8':
                    ret += newFancyText[8];
                    break A;
                case '9':
                    ret += newFancyText[9];
                    break A;
                case '+':
                    ret += '⊕';
                    break A;
                case '-':
                    ret += '⊖';
                    break A;
                case '*':
                    ret += '⊗';
                    break A;
                case '/':
                    ret += '⊘';
                    break A;
                case '<':
                    ret += '⧀';
                    break A;
                case '>':
                    ret += '⧁';
                    break A;
                case '=':
                    ret += '⊜';
                    break A;
                    
                default:
                    ret+=c;
                    break A;
            }
        }
        return ret;
    }
    
    
    
    private static Config getSettings() {
        return CoreAPI.getSettings();
    }
    
    public static String translate(String toTrans) {
        String message = toTrans;
        String msg = message;
        String prefix = "", errorPrefix = "", anticheatPrefix = "",
                errorPrefixFile = getSettings().getString("ColorSettings.Error-Prefix"),
                prefixFile = getSettings().getString("ColorSettings.Prefix"),
                anticheatPrefixFile = getSettings().getString("ColorSettings.AntiCheat-Prefix");
        if(toTrans != null && getSettings() != null) {
            if(getSettings().getString("ColorSettings.Prefix") != ""
               && getSettings().getString("ColorSettings.Prefix") != null)
                prefix = (!toTrans.contains("[p] ") ?
                        (prefixFile.endsWith(" ") ? prefixFile.substring(0, prefixFile.length()+-1) : prefixFile) + " " :
                        prefixFile);
            
            if(getSettings().getString("ColorSettings.Error-Prefix") != ""
               && getSettings().getString("ColorSettings.Error-Prefix") != null)
                errorPrefix = (!toTrans.contains("[e] ") ?
                        (errorPrefixFile.endsWith(" ") ? errorPrefixFile.substring(0, errorPrefixFile.length()+-1) : errorPrefixFile) + " " :
                        errorPrefixFile);
            if(getSettings().getString("ColorSettings.AntiCheat-Prefix") != ""
               && getSettings().getString("ColorSettings.AntiCheat-Prefix") != null)
                anticheatPrefix = (!toTrans.contains("[ac] ") ?
                        (anticheatPrefixFile.endsWith(" ") ? anticheatPrefixFile.substring(0, anticheatPrefixFile.length()+-1) : anticheatPrefixFile) + " " :
                        anticheatPrefixFile);
            msg = message
                    .replace("[p]".toLowerCase(), prefix)
                    .replace("[e]".toLowerCase(), errorPrefix)
                    .replace("[ac]".toLowerCase(), anticheatPrefix)
                    .replace("[pc]".toLowerCase(), getSettings().getString("ColorSettings.Important"))
                    .replace("[c]".toLowerCase(), getSettings().getString("ColorSettings.Default"))
                    .replace("[a]".toLowerCase(), getSettings().getString("ColorSettings.Argument"))
                    .replace("[r]".toLowerCase(), getSettings().getString("ColorSettings.Required-Argument"))
                    .replace("[o]".toLowerCase(), getSettings().getString("ColorSettings.Optional-Argument"))
                    .replace("[n]", "\n")
                    .replace("\\n", "\n");
        }
        
        return ColorUtil.translateColors('&', msg);
    }
    
    
    public static String addChar(int index, String toAdd, String s) {
        String text = "";
        char[] chars = s.toCharArray();
        for(int i = 0; i<chars.length;i++) {
            char c = chars[i];
            if(index==i)text+=toAdd;
            text+=c;
        }
        return text;
    }
    
    public static String join(String[] args, String joiner, int start) {
        String ret = "";
        for(int i = start; i < args.length; i++) {
            ret+=args[i]+(i < (args.length+-1) ? joiner : "");
        }
        return ret;
    }
    
    public static String join(Object[] args, String joiner, int start) {
        String ret = "";
        for(int i = start; i < args.length; i++) {
            ret+=CoreAPI.findName(args[i])+(i < (args.length+-1) ? joiner : "");
        }
        return ret;
    }
    
    
    public static String repeat(String toRepeat, int amount) {
        String ret = "";
        for(int i = 0; i < amount; i++) {
            ret+=toRepeat;
        }
        return ret;
    }
    
    
    public static String centerText(int maxWidth, String text) {
        int spaces = (int) Math.round((maxWidth - 1.4 * ColorUtil.removeColor(text).length()) / 2);
        
        return repeat(" ", spaces) + text;
    }
    
    
    public static String centerText(int maxWidth, String text, String endWith) {
        int spaces = (int) Math.round((maxWidth - 1.4 * ColorUtil.removeColor(text).length()) / 2);
        String repeat = repeat(" ", spaces);
        int x = repeat.length()+-text.length();
        int end = x < 0 ? 0 : x;
        return repeat + text + repeat(" ", maxWidth-spaces-text.toCharArray().length-1) + endWith;
    }
    
    public static String centerDefaultText(String text) {
        return centerText(80, text);
    }
    
    
    public static int getPages(String str) {
        return ChatPaginator.paginate(str, 1).getTotalPages();
    }
    
    
    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty() || str.length() == 0 || !hasChars(str);
    }
    public static boolean isEmptyArray(String[] str) {
        boolean x = true;
        for(String f : str) {
            if(!isEmpty(f)) {
                x = false;
            }
        }
        return str == null || x || str.length == 0;
    }
    public static boolean hasChars(String str) {
        for(char c : str.toCharArray()) {
            if(c!=' ' && c!='\n' && !Character.getName(c).equalsIgnoreCase("LINE FEED (LF)"))
                return true;
        }
        return str == null || str.isEmpty() || str.length() == 0;
    }
    
    
    public static String getNameFromEnum(Enum e) {
        String f = e.name();
        String a = f.charAt(0) + f.substring(1).toLowerCase().split("_")[0];
        if(f.contains("_")) {
            for(int z = 1; z < f.split("_").length; z++) {
                String p = f.split("_")[z];
                a+=" " + (p.charAt(0)+"").toUpperCase() + p.substring(1).toLowerCase();
            }
        }
        return a;
    }
    
    public static String getNameFromCaps(String e) {
        String f = e.replace(" ", "_");
        String a = f.charAt(0) + f.substring(1).toLowerCase().split("_")[0];
        if(f.contains("_")) {
            for(int z = 1; z < f.split("_").length; z++) {
                String p = f.split("_")[z];
                a+=" " + (p.charAt(0)+"").toUpperCase() + p.substring(1).toLowerCase();
            }
        }
        return a;
    }
    
    public static String translateForUser(User u, String original, String ms) {
        if(u == null) {
            return ColorUtil.translateChatColor(ms);
        }
        String og = !original.startsWith("&") & !original.startsWith("§") ? "§"+original : original;
        
        return ColorUtil.replace(u, og, ms);
    }
    
    public static String formatList(List<String> str) {
        return formatList(false, false, str);
    }
    
    public static String formatList(boolean comma, List<String> str) {
        return formatList(false, comma, str);
    }
    
    public static String formatList(boolean color, boolean comma, List<String> str) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < str.size(); i++) {
            String x = str.get(i);
            if(color) {
                sb.append("[pc]").append(x);
                if(i < (str.size()+-1))
                    if(comma)
                        sb.append("[c], ");
                    else
                        sb.append("[c] ");
            }else {
                sb.append(x);
                if(i < (str.size()+-1))
                    if(comma)
                        sb.append(", ");
                    else
                        sb.append(" ");
            }
        }
        return sb.toString().trim();
    }
    
    public static String getBalanceTops(int pageNumber) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb.append(CoreAPI.getMessages().getString("Commands.Balance.Top.header") + "\n");
        int index = 1;
        HashMap<String, Double> names = new HashMap<>();
        for(UUID uuid : CoreAPI.getBalanceTops()) {
            OfflineUser u = CoreAPI.getAllUser(uuid);
            names.put(u.getRealName(), u.getAccount().getBalanceRaw());
        }
        for(Map.Entry<String, Double> e : MapSorting.reversedValues(names)) {
            String format = CoreAPI.format("Commands.Balance.Top.format", index, e.getKey(), NumberUtil.format(e.getValue()));
            sb2.append(format).append("\n");
            index++;
        }
        if(pageNumber<1)pageNumber=1;
        if(pageNumber>getPages(sb.toString().trim()))pageNumber=getPages(sb2.toString().trim());
        ChatPaginator.ChatPage p = ChatPaginator.paginate(sb2.toString().trim(), pageNumber, Integer.MAX_VALUE, 10);
        for(String s : p.getLines())
            sb.append(s+"\n");
        sb.append(CoreAPI.getMessages().getString("Commands.Balance.Top.footer"));
        return sb.toString();
    }
    
    public static int getBalanceTopsPages() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        int index = 1;
        HashMap<String, Double> names = new HashMap<>();
        for(UUID uuid : CoreAPI.getBalanceTops()) {
            OfflineUser u = CoreAPI.getAllUser(uuid);
            names.put(u.getRealName(), u.getAccount().getBalanceRaw());
        }
        for(Map.Entry<String, Double> e : MapSorting.reversedValues(names)) {
            String format = CoreAPI.format("Commands.Balance.Top.format", index, e.getKey(), NumberUtil.format(e.getValue()));
            sb2.append(format).append("\n");
            index++;
        }
        return getPages(sb2.toString().trim());
    }
    
    public static boolean checkForDomain(String str) {
        String domainPattern = "[a-zA-Z0-9](([a-zA-Z0-9\\-]{0,61}[A-Za-z0-9])?\\.)+(com|net|org|co|gg|io|pro)";
        Pattern r = Pattern.compile(domainPattern);
        Matcher m = r.matcher(str);
        Pattern invPat = Pattern.compile("(?:https?://)?discord(?:app\\.com/invite|\\.gg)/([a-z0-9-]+)", 2);
        Matcher m2 = invPat.matcher(str);
        return m.find() || m2.find();
    }
    
    public static String[] toStringArray(Argument[] args) {
        String[] ret = new String[args.length];
        for(int x = 0; x < args.length; x++) {
            ret[x]=args[x].getContents();
        }
        return ret;
    }
    
    public boolean linkIsValid(String targetUrl) {
        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
                    .openConnection();
            httpUrlConn.setRequestMethod("HEAD");
            httpUrlConn.setConnectTimeout(10000);
            httpUrlConn.setReadTimeout(10000);
            
            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            return false;
        }
    }
}
