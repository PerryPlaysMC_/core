package me.perryplaysmc.utils.string;

import me.perryplaysmc.user.User;
import org.bukkit.ChatColor;

public class ColorUtil {



    public static String translateColors(char altColor, String text) {
        if(text == null) return "Error";
        for(ChatColor c : ChatColor.values()) {
            if(text.contains((altColor+"")+c.getChar()))
            text = text.replace((altColor+"") + c.getChar(), "§" + c.getChar());
        }
        return text;
    }



    public static String replace(User u, String original, String ms) {
        for (ChatColor c : ChatColor.values()) {
            if ((u.hasPermission("allcodes", "chatcolor." + c.getChar())) && (ms.contains("&" + c.getChar()))) {
                if(c.getChar() == 'r') {
                    ms = ms.replace("&r", original.replace("&", "§"));
                    continue;
                }
                ms = ms.replace("&" + c.getChar(), "§" + c.getChar());
            }
        }
        return ms;
    }

    public static String translateChatColor(String text) {
        return translateColors('&', text);
    }

    public static String removeColor(String str) {
        String r = StringUtils.translate(str);
        for(ChatColor c : ChatColor.values()) {
            if(r.contains("§"+c.getChar()))
                r = r.replace("§" + c.getChar(), "");
            else if(r.contains("&"+c.getChar()))
                r = r.replace("&" + c.getChar(), "");
        }
        return r;
    }
    public static ChatColor locateChatColor(String l) {
        int id = l.length()+-1;
        ChatColor c = null;
        try {
            while(c == null) {
                if(id < 0) {
                    c = null;
                    break;
                }
                c = ChatColor.getByChar(l.charAt(id));
                id -= 1;
                String x = c.name();
            }
        }catch (Exception t){
        }
        return c;
    }
    
    public static String removeColorExact(String str) {
        String r = StringUtils.translate(str);
        for(ChatColor c : ChatColor.values()) {
            if(r.contains("§"+c.getChar()))
                r = r.replace("§" + c.getChar(), "");
        }
        return r;
    }

    public static String changeChatColorTo(char newChar, String str) {
        String r = str;
        if(str.contains("§") || str.contains("&"))
            for(ChatColor c : ChatColor.values()) {
                if(str.contains("§"+c.getChar()))
                    r = r.replace("§" + c.getChar(), ""+newChar + c.getChar());
            }
        return r;
    }

    public static ChatColor getLastUsedColorsAsChatColor(String message, ChatColor def) {
        String a = removeColor(message);
        if(a.toCharArray().length < 1) return def;
        int l = a.toCharArray().length;
        ChatColor c = ChatColor.getByChar(a.charAt(l+-1));
        int i = l+-1;
        while(c == null || !c.isColor()) {
            i = i+-2;
            if(l >= l+- i) {
                c = ChatColor.getByChar(a.charAt(l +- i));
            }else {
                break;
            }
        }
        if(c == null || !c.isColor()) return def;
        return c;
    }

    public static String getLastUsedColors(String msg) {
        String last = "", l2 = "";
        if(msg.contains("&") || msg.contains("§")) {
            msg = msg.replace("&", "§");
            for (int c = 1; c < msg.split("§").length; c++) {
                String[] d = msg.split("§");
                if(d.length == 0 || d[c] == null || d[c].length() == 0) continue;
                String ch = d[c].charAt(0) + "";
                if(ChatColor.getByChar(d[c].charAt(0)) != null) {
                    if(isColor(last)) {
                        if(last.length()+-2>0)
                            last = last.substring(0, last.length()+-2);
                    }
                    if(!last.contains("§"+ch))
                        if(isColor("§"+ch))
                            last = "§" + ch;
                        else
                            last += "§" + ch;
                }
            }
        }
        return last.replace("§§","§");
    }


    public static boolean isColor(String last) {
        return !isBold(last) && !isItalic(last) && !isStrikThrough(last) && !isUnderline(last);
    }

    public static boolean hasColor(String last) {
        for(String x : changeChatColorTo('&', last).split("&")) {
            if(isColor(x)) {
                return true;
            }
        }
        return false;
    }
    public static String replaceFirstColor(String last) {
        String f = changeChatColorTo('&',last);
        if(f.length()>0) {
            f.split("&")[0] = "";
            return f.substring(2);
        }
        return f;
    }

    private static boolean isBold(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        return x.charAt(x.length() + -1) == 'l';
    }
    private static boolean isItalic(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        return x.charAt(x.length() + -1) == 'o';
    }
    private static boolean isUnderline(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        return x.charAt(x.length() + -1) == 'n';
    }
    private static boolean isStrikThrough(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        return x.charAt(x.length() + -1) == 'm';
    }
    
    
}
