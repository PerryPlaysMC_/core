package me.perryplaysmc.utils.string;

import java.util.*;
import java.util.function.Consumer;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 7/31/19-2023
 **/
public class ListsHelper<T> implements Iterable<T>{
    
    private ListsHelper<T> inst;
    private List<T> list;
    private ListsHelper(List<T> list) {
        inst = this;
        this.list = list;
    }
    
    public static <T> ListsHelper<T> createList(Set<T> objs) {
        return new ListsHelper<>(new ArrayList<T>(objs));
    }
    
    public static <T> ListsHelper<T> createList(T... objs) {
        return new ListsHelper<>(new ArrayList<T>(Arrays.asList(objs)));
    }
    
    public static <T> ListsHelper<T> createList(List<T> objs) {
        return new ListsHelper<>(new ArrayList<T>(objs));
    }
    
    public String toString() {
        return "["+toString(", ")+"]";
    }
    public String toString(String joiner) {
        String ret = "";
        for(Object t : list) {
            ret+=t.toString()+joiner;
        }
        return ret.substring(0, ret.length()+-joiner.length());
    }
    
    public ListsHelper<T> add(T t) {
        list.add(t);
        return inst;
    }
    
    public ListsHelper<T> addAll(List<T> t) {
        list.addAll(t);
        return inst;
    }
    
    public ListsHelper<T> removeAll(List<T> t) {
        list.removeAll(t);
        return inst;
    }
    
    public ListsHelper<T> addAll(T... t) {
        list.addAll(Arrays.asList(t));
        return inst;
    }
    
    public ListsHelper<T> removeAll(T... t) {
        list.removeAll(Arrays.asList(t));
        return inst;
    }
    
    public ListsHelper<T> remove(T t) {
        list.remove(t);
        return inst;
    }
    
    public boolean contains(T t) {
        return list.contains(t);
    }
    
    public List<T> getList() {
        return list;
    }
    
    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }
    
    @Override
    public void forEach(Consumer<? super T> action) {
        list.forEach(action);
    }
    
    @Override
    public Spliterator<T> spliterator() {
        return list.spliterator();
    }
}
