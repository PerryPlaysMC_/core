package me.perryplaysmc.utils.string;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;

@SuppressWarnings("rawtypes")
public class MapSorting {
	// Building block - extract key from entry
	private static final Function EXTRACT_KEY =
			(Function<Entry<Object, Object>, Object>) input -> Objects.requireNonNull(input).getKey();
	// Same as above, only we extract the value
	private static final Function EXTRACT_VALUE =
			(Function<Entry<Object, Object>, Object>) objectObjectEntry -> objectObjectEntry != null ? objectObjectEntry.getValue() : null;
	
	/**
	 * Sort the given map by the value in each entry.
	 * @param map - map of comparable values.
	 * @return A new list with the sort result.
	 */
	public static <T, V extends Comparable<V>> List<Entry<T, V>> sortedValues(Map<T, V> map) {
		return sortedValues(map, Ordering.<V>natural());
	}
	
	public static <T, V extends Comparable<V>>  List<Entry<T, V>> reversedValues(Map<T, V> map) {
		List<Entry<T, V>> sort = sortedValues(map);
		List<Entry<T, V>> ret = new ArrayList<>();
		for(int i = sort.size()+-1; i > -1; i--) {
			ret.add(sort.get(i));
		}
		return ret;
	}
	
	/**
	 * Sort the given map by the value in each entry.
	 * @param map - map of comparable values.
	 * @param valueComparator - object for comparing each values.
	 * @return A new list with the sort result.
	 */
	public static <T, V> List<Entry<T, V>> sortedValues(Map<T, V> map, Comparator<V> valueComparator) {
		return Ordering.from(valueComparator).onResultOf(MapSorting.<T, V>extractValue()).sortedCopy(map.entrySet());
	}
	
	/**
	 * Retrieve every key in the entry list in order.
	 * @param entryList - the entry list.
	 * @return Every key in order.
	 */
	public static <T, V> Iterable<T> keys(List<Entry<T, V>> entryList) {
		return entryList.stream().map(MapSorting.<T, V>extractKey()).collect(Collectors.toList());
	}
	
	/**
	 * Retrieve every value in the entry list in order.
	 * @param entryList - the entry list.
	 * @return Every value in order.
	 */
	public static <T, V> Iterable<V> values(List<Entry<T, V>> entryList) {
		return entryList.stream().map(MapSorting.<T, V>extractValue()).collect(Collectors.toList());
	}
	
	@SuppressWarnings("unchecked")
	private static <T, V> Function<Entry<T, V>, T> extractKey() {
		return EXTRACT_KEY;
	}
	
	@SuppressWarnings("unchecked")
	private static <T, V> Function<Entry<T, V>, V> extractValue() {
		return EXTRACT_VALUE;
	}
}