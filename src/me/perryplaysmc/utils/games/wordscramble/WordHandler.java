package me.perryplaysmc.utils.games.wordscramble;

import java.util.ArrayList;
import java.util.List;

public class WordHandler {
    
    private static List<WordScramble> words = new ArrayList<>();
    
    public static List<WordScramble> getWords() {
        if (words == null) words = new ArrayList<>();
        return words;
    }
    
    public static void addWord(WordScramble wordScramble) {
        if (!getWords().contains(wordScramble)) {
            getWords().add(wordScramble);
        }
    }
    
    public static WordScramble getWord(int i) {
        return getWords().get(i);
    }
}
