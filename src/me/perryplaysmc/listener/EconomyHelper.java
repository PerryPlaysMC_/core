package me.perryplaysmc.listener;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("all")
public class EconomyHelper {


	public static ItemStack back, GreenGlass1, RedGlass1, GreenGlass32, RedGlass32, GreenGlass64, RedGlass64, YG, OG, Center;

	public static void sellItem(User user, int amount, double worth, Material m) {
		sellItem(user, true, amount, worth, m);
	}
	public static void sellItem(User user, int amount, double worth, ItemStack m) {
		sellItem(user, true, amount, worth, m);
	}
	public static void sellItem(User user, boolean sendMessage, int amount, double worth, Material m) {
		String format = StringUtils.getNameFromEnum(m);
		if(getItems(user, m) >= amount && amount >0) {
			worth = worth*amount +- 5%5;
			if(user.getAccount().exceedsMax(false, worth)) {
				String f = NumberUtil.format(worth);

				if(sendMessage)
					user.sendMessageFormat("Shop.Sell.exceeds-Balance", amount, format, f, worth);
				return;
			}else {
				removeItem(user, amount, m);
				user.getAccount().addBalance(worth);
				String f = NumberUtil.format(worth);
				if(sendMessage)
					user.sendMessageFormat("Shop.Sell.success", amount, format, f, worth);
			}
		}else {
			String f = NumberUtil.format(worth);
			if(sendMessage)
				user.sendMessageFormat("Shop.Sell.fail", amount, format, f, worth);
		}
	}
	public static void sellItem(User user, boolean sendMessage, int amount, double worth, ItemStack m) {
		String format = StringUtils.getNameFromEnum(m.getType());
		if(getItems(user, m.getType()) >= amount && amount >0) {
			worth = worth*amount +- 5%5;
			if(user.getAccount().exceedsMax(false, worth)) {
				String f = NumberUtil.format(worth);

				if(sendMessage)
					user.sendMessageFormat("Shop.Sell.exceeds-Balance", amount, format, f, worth);
				return;
			}else {
				if(!m.hasItemMeta()) {
					removeItem(user, amount, m.getType());
					user.getAccount().addBalance(worth);
					String f = NumberUtil.format(worth);
					if(sendMessage)
						user.sendMessageFormat("Shop.Sell.success", amount, format, f, worth);
				}
			}
		}else {
			String f = NumberUtil.format(worth);
			if(sendMessage)
				user.sendMessageFormat("Shop.Sell.fail", amount, format, f, worth);
		}
	}

	public static void update(Inventory inv, User u, boolean isSellable, boolean isBuyable, Material mat, double sellcost, double cost) {

		int amt = getFreeItems(u, mat);
		while(!u.getAccount().hasEnough(cost*amt)) {
			amt--;
		}
		String format = StringUtils.getNameFromEnum(mat);
		if(amt < 0) amt = 0;
		ItemStack Center = ItemBuilder.createItem(mat).setName("[c]Sell/Buy all [pc]" + format).setAmount(64)
				.setLore(
				(isSellable ? "[c]Sell x[pc]" + getItems(u, mat) + "[c],  [pc]" + format + " [c]for [c]$[pc]" + NumberUtil.format(sellcost*getItems(u, mat)) : "&cNot Sellable"),
				(isBuyable ? "[c]Buy x[pc]" + amt +"[c], [pc]" + format + " [c]for [c]$[pc]" + NumberUtil.format(cost*amt) : "&cNot Buyable")).buildItem();
		inv.setItem(13, Center);
		new BukkitRunnable() {
			@Override
			public void run() {
				u.updateInventory();
			}

		}.runTaskLater(Core.getAPI(), 20);
	}

	public static int getFreeItems(User user, Material mat) {
		int amount = 64*36 +- getItems(user, mat);
		return amount;
	}
	public static int getFreeItems(Inventory user) {
		int amount = 64*36 +- getItems(user);
		return amount;
	}

	public static void buyItem(User user, int amount, double worth, Material mat) {
		String format = StringUtils.getNameFromEnum(mat);
		if(!user.getAccount().hasEnough(amount*worth)) {
			user.sendMessageFormat("Shop.Buy.not-Enough-Money", amount, format, worth);
			return;
		}
		addItem(user, amount, worth, mat);
	}

	public static boolean removeItem(User user, int count, Material mat) {
		Map<Integer, ? extends ItemStack> ammo = user.getInventory().all(mat);

		int found = 0;
		for (ItemStack stack : ammo.values())
			found += stack.getAmount();
		if (count > found)
			return false;

		for (Integer index : ammo.keySet()) {
			ItemStack stack = ammo.get(index);

			int removed = Math.min(count, stack.getAmount());
			count -= removed;

			if (stack.getAmount() == removed)
				user.getInventory().setItem(index, null);
			else
				stack.setAmount(stack.getAmount() - removed);

			if (count <= 0)
				break;
		}
		user.updateInventory();
		return true;
	}

	public static void addItem(User user, OfflineUser o, Chest c, int amount, double worth, Material mat) {
		HashMap<Integer, ItemStack> excess = user.getInventory().addItem(new ItemStack(mat, amount));
		String format = StringUtils.getNameFromEnum(mat);
		double money = amount*worth;
		user.getAccount().removeBalance(worth*amount);
		String f = NumberUtil.format(money);
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			money = worth;
			user.getAccount().addBalance(money);
			f = NumberUtil.format(money);
			c.getInventory().addItem(me.getValue());
			o.getAccount().removeBalance(money);
			user.sendMessageFormat("Shop.Buy.not-enough-space", me.getValue().getAmount(), format, f, money);
		}
		user.sendMessageFormat("Shop.Buy.success", amount, format, f, user.getAccount().getBalance(), amount*worth);
	}

	public static void addItem(User user, int amount, double worth, Material mat) {
		HashMap<Integer, ItemStack> excess = user.getInventory().addItem(new ItemStack(mat, amount));
		String format = StringUtils.getNameFromEnum(mat);
		double money = amount*worth;
		user.getAccount().removeBalance(worth*amount);
		String f = NumberUtil.format(money);
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			money = me.getValue().getAmount()*worth;
			user.getAccount().addBalance(money);
			f = NumberUtil.format(money);
			amount-= me.getValue().getAmount();
			user.sendMessageFormat("Shop.Buy.not-enough-space", me.getValue().getAmount(), format, f, money);
		}
		user.sendMessageFormat("Shop.Buy.success", amount, format, f, user.getAccount().getBalance(), amount*worth);
	}

	public static void addItem(Inventory user, int amount, double worth, Material mat) {
		HashMap<Integer, ItemStack> excess = user.addItem(new ItemStack(mat, amount));
		String format = StringUtils.getNameFromEnum(mat);
		double money = amount*worth;
		String f = NumberUtil.format(money);
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			money = me.getValue().getAmount()*worth;
			f = NumberUtil.format(money);
			amount-= me.getValue().getAmount();
		}
	}

	public static double getMoney(User user, int amount, double worth, Material mat) {
		HashMap<Integer, ItemStack> excess = user.getInventory().addItem(new ItemStack(mat, amount));
		String format = StringUtils.getNameFromEnum(mat);
		double money = 0;
		user.getAccount().removeBalance(amount*worth);
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			money = me.getValue().getAmount()*worth;
			return money;
		}
		return (amount*worth);
	}

	public static int getItemsAmount(User user, Material mat) {
		Inventory i = Bukkit.createInventory(null, 64*36);
		HashMap<Integer, ItemStack> excess = i.addItem(new ItemStack(mat, 64));
		String format = StringUtils.getNameFromEnum(mat);
		double money = 0;
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			return me.getValue().getAmount();
		}
		return (64*36);
	}
	public static int getItems(User user, Material mat) {
		ItemStack[] g = user.getInventory().getContents();

		int cuantity= 0;
		for(int i = 0; i < g.length; i++) {
			if(g[i] != null){
				if( g[i].getType().equals(mat)){
					int cant = g[i].getAmount();
					cuantity= cuantity + cant;
				}
			}
		}
		return cuantity;
	}
	public static int getItems(Inventory user) {
		ItemStack[] g = user.getContents();

		int cuantity= 0;
		for(int i = 0; i < g.length; i++) {
			if(g[i] != null){
				int cant = g[i].getAmount();
				cuantity= cuantity + cant;
			}
		}
		return cuantity;
	}

	public static void sellAll(User u, boolean sendMessage, double worth, ItemStack mat) {
		sellItem(u, sendMessage, getItems(u, mat.getType()), worth, mat);
	}
	public static void sellAll(User u, double worth, ItemStack mat) {
		sellItem(u, true, getItems(u, mat.getType()), worth, mat);
	}
	public static void sellAll(User u, double worth, Material mat) {
		sellItem(u, getItems(u, mat), worth, mat);
	}

	public static void buyAll(User u, double worth, Material mat) {
		String format = StringUtils.getNameFromEnum(mat);
		int amount = 64*36;
		while(!u.getAccount().hasEnough(amount*worth)) {
			amount = amount +-1;
		}
		double money = amount*worth;
		String f = NumberUtil.format(money);
		if(!u.getAccount().hasEnough(money)) {
			u.sendMessageFormat("Shop.Buy.not-Enough-Money", amount, format, worth);
			return;
		}
		HashMap<Integer, ItemStack> excess = u.getInventory().addItem(new ItemStack(mat, amount));
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			money = me.getValue().getAmount()*worth;
			f = NumberUtil.format(money);
			amount = amount +- me.getValue().getAmount();
			u.sendMessageFormat("Shop.Buy.not-enough-space", me.getValue().getAmount(), format, f, money);
		}
		u.sendMessageFormat("Shop.Buy.success", amount, format, f, u.getAccount().getBalance(), amount*worth);
		u.getAccount().removeBalance(worth*amount);
	}

	public static Inventory createGUI(User u, boolean isBuyable, boolean isSellable, Material m, double sellcost, double cost) {
		Inventory a = Bukkit.createInventory(null, 9*3, "§a§lSell/Buy.");
		String format = StringUtils.getNameFromEnum(m);
		YG = ItemBuilder.createGlass(ItemBuilder.ColorType.YELLOW, true).setName("§6").setAmount(1).buildItem();
		OG = ItemBuilder.createGlass(ItemBuilder.ColorType.ORANGE, true).setName("§6").setAmount(1).buildItem();
		back = ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("&a&lBack").setLore("&e&lGo back to the main shop menu.").buildItem();
		GreenGlass1 =  ItemBuilder.createGlass(ItemBuilder.ColorType.LIME, true).setName(
                "[c]Buy 1 [pc]" + format).setLore("[c]Buy 1 [pc]" + format + " [c]for $[pc]" + NumberUtil.format(cost) + "&5&l").buildItem();

		RedGlass1 =  ItemBuilder.createGlass(ItemBuilder.ColorType.RED, true).setName(
                "[c]Sell 1 [pc]" + format).setLore("[c]Sell 1 [pc]" + format + " [c]for $[pc]" + NumberUtil.format(sellcost)).buildItem();

		GreenGlass32 = ItemBuilder.createGlass(ItemBuilder.ColorType.LIME, true).setName(
				"[c]Buy 32 [pc]" + format).setAmount(32)
				.setLore("[c]Buy 32 [pc]" + format + " [c]for $[pc]" + NumberUtil.format(cost*32) + "&5&l").buildItem();

		RedGlass32 = ItemBuilder.createGlass(ItemBuilder.ColorType.RED, true).setName(
				"[c]Sell 32 [pc]" + format).setAmount(32)
				.setLore("[c]Sell 32 [pc]" + format + " [c]for $[pc]" + NumberUtil.format(sellcost*32)).buildItem();

		GreenGlass64 = ItemBuilder.createGlass(ItemBuilder.ColorType.LIME, true).setName(
				"[c]Buy 64 [pc]" + format).setAmount(64)
				.setLore("[c]Buy 64 [pc]" + format + " [c]for $[pc]" + NumberUtil.format(cost*64) + "&5&l").buildItem();

		RedGlass64 = ItemBuilder.createGlass(ItemBuilder.ColorType.RED, true).setName(
				"[c]Sell 64 [pc]" + format).setAmount(64)
				.setLore("[c]Sell 64 [pc]" + format + " [c]for $[pc]" + NumberUtil.format(sellcost*64)).buildItem();


		Inventory nul = Bukkit.createInventory(u.getBase(), 4*9);
		HashMap<Integer, ItemStack> excess = nul.addItem(new ItemStack(m, 64*9*4));
		int amount = 64*36;
		for (Map.Entry<Integer, ItemStack> me : excess.entrySet()) {
			amount-=me.getValue().getAmount();
			break;
		}
		int amt = 64*36;
		while(!u.getAccount().hasEnough(cost*amt)) {
			amt--;
		}
		Center = ItemBuilder.createItem(m).setName("[c]Sell/Buy all [pc]" + format).setAmount(64).setLore(
				(isSellable ? "[c]Sell x[pc]" + getItems(u, m) + "[c],  [pc]" + format + " [c]for [c]$[pc]" + NumberUtil.format(sellcost*getItems(u, m)) : "&cNot Sellable"),
				(isBuyable ? "[c]Buy x[pc]" + amt +"[c], [pc]" + format + " [c]for [c]$[pc]" + NumberUtil.format(cost*amt) : "&cNot Buyable")).buildItem();
		a.setItem(1, YG);
		a.setItem(2, YG);
		a.setItem(3, OG);
		a.setItem(4, YG);
		a.setItem(5, OG);
		a.setItem(6, YG);
		a.setItem(7, YG);
		a.setItem(8, OG);
		a.setItem(9, YG);
		if(isSellable) {
			a.setItem(10, RedGlass1);
			a.setItem(11, RedGlass32);
			a.setItem(12, RedGlass64);
		}else if(isBuyable){
			a.setItem(10, GreenGlass1);
			a.setItem(11, GreenGlass32);
			a.setItem(12, GreenGlass64);
		}
		a.setItem(13, Center);
		if(isBuyable) {
			a.setItem(14, GreenGlass64);
			a.setItem(15, GreenGlass32);
			a.setItem(16, GreenGlass1);
		}else if(isSellable) {
			a.setItem(14, RedGlass64);
			a.setItem(15, RedGlass32);
			a.setItem(16, RedGlass1);
		}
		a.setItem(17, OG);
		a.setItem(18, YG);
		a.setItem(19, OG);
		a.setItem(20, YG);
		a.setItem(21, YG);
		a.setItem(22, YG);
		a.setItem(23, OG);
		a.setItem(24, OG);
		a.setItem(25, YG);
		a.setItem(26, OG);
		a.setItem(0, back);
		return a;
	}
}
