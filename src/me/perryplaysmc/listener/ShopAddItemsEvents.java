package me.perryplaysmc.listener;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.event.user.UserChatEvent;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.inventory.shops.GUIShop;
import me.perryplaysmc.utils.inventory.shops.GUIShopHandler;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@SuppressWarnings("all")
public class ShopAddItemsEvents implements Listener {

    public static Inventory inv;
    private List<User> shopName = new ArrayList<>();
    private HashMap<User, GUIShop> addItem = new HashMap<>();
    private HashMap<User, List<Inventory>> prev = new HashMap<>();
    private HashMap<User, Inven> ph = new HashMap<>();
    private HashMap<User, InvenHolder> inventory = new HashMap<>();
    private HashMap<User, InvenHolder> rearrange = new HashMap<>();
    private HashMap<User, ItemStack> cursor = new HashMap<>();
    String invName = "Select a Shop to Edit";

    public static void load() {
        inv = Bukkit.createInventory(null, 9*6, "Select a Shop to Edit");
        int last = 0;
        for (GUIShop gui : GUIShopHandler.getShops()) {
            if(gui.isNext()) continue;
            if(gui.getDisplay() == null) {
                ItemStack f = ItemBuilder.createItem(new ItemStack(Material.BARRIER))
                        .setName(gui.getDisplayName()).setLore("[c]Most likely a Gui with NextPage").buildItem();
                if(!hasItem(inv, f))
                    inv.setItem(last+9, f);
                continue;
            }
            inv.addItem(gui.getDisplay());
            last = getItemSlot(inv, gui.getDisplay());
            while(gui.hasNext()) {
                if(last == -1) {
                    last = inv.firstEmpty();
                }
                ItemStack f = ItemBuilder.createItem(new ItemStack(Material.BARRIER)).setName(
                        gui.getNext().getDisplayName()).setLore("[c]Most likely a Gui with NextPage").buildItem();
                if(!hasItem(inv, f))
                    inv.setItem(last+9, f);
                last = getItemSlot(inv, f);
                gui = gui.getNext();
            }
        }
        inv.setItem(inv.getSize()+-1, ItemBuilder.createItem(new ItemStack(Material.GREEN_WOOL)).setName("§aAdd a new GUI").buildItem());
        for(int i = 0; i < inv.getSize(); i ++) {
            if(inv.getItem(i) != null && inv.getItem(i).getType() != Material.AIR) {
                inv.getItem(i).setAmount(1);
                inv.setItem(i, inv.getItem(i));
            }
        }
    }

    public static void Show(User u) {
        u.openInventory(inv);
    }

    public ShopAddItemsEvents() {
        load();
    }

    @EventHandler
    void onChat(UserChatEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(shopName.contains(u)) {
            e.setCancelled(true);
            if(e.getMessage().equalsIgnoreCase("cancel")) {
                shopName.remove(u);
                u.sendMessage("[c]Cancelling");
                return;
            }
            String[] args = e.getMessage().split(" ");
            if(args.length < 4) {
                e.setCancelled(true);
                u.sendMessage("[c]Please add a FileName, DisplayName, InventorySize, and Item Type");
                return;
            }
            if(!args[3].toUpperCase().endsWith("R") && !args[3].toUpperCase().endsWith("S")) {
                e.setCancelled(true);
                u.sendMessage("[c]Invalid number");
                return;
            }
            String Int = args[3].split(args[3].endsWith("R") ? "R" : args[3].endsWith("S") ? "S" : "")[0];
            if(!NumberUtil.isInt(Int)) {
                u.sendMessage("[c]Invalid number");
                e.setCancelled(true);
                return;
            }
            if(ItemBuilder.convertMaterial(args[2]) == null) {
                u.sendMessage("[c]Invalid Item");
                e.setCancelled(true);
                return;
            }
            int size = args[3].endsWith("R") ? NumberUtil.getInt(Int)*9 : NumberUtil.getInt(Int);
            if(!args[3].endsWith("R") && size > 54) size = 54;
            if(args[3].endsWith("R") && size > 6) size = 54;


            for(User u2 : CoreAPI.getOnlineUsers()) {
                if(u2.isCommandSpyEnabled() && !u.getName().equalsIgnoreCase(u2.getName())) {
                    u2.sendMessage("[pc]"+u.getName()+" [c]GUI: " + args[1] + " Created Size: " + size + " Item: " + args[2]);
                }
            }
            Config cfg2 = new Config(new File(Config.defaultDirectory, "shops"),args[0]+".yml");
            if(!cfg2.isSet("Settings.Display"))cfg2.set("Settings.Display", args[1].replace("_", " "));
            if(!cfg2.isSet("Settings.Item"))cfg2.set("Settings.Item", args[2]);
            if(!cfg2.isSet("Settings.Size"))cfg2.set("Settings.Size", size);
            new GUIShop(args[0]);
            load();
            Show(u);
            e.setCancelled(true);
        }
    }

    List<User> clickedClose = new ArrayList<>();

    @EventHandler
    void onClose(InventoryCloseEvent e) {
        User u = CoreAPI.getUser(e.getPlayer().getName());
        if(!clickedClose.contains(u) && rearrange.containsKey(u)) {
            GUIShop gui = rearrange.get(u).getGui();
            prev.remove(u);
            cursor.remove(u);
            rearrange.remove(u);
            for(int ie = 0; ie < e.getInventory().getContents().length + -9; ie++) {
                ItemStack z = e.getInventory().getItem(ie);
                if(z != null && !z.getType().name().contains("AIR")) {
                    ItemBuilder i = ItemBuilder.createItem(z);
                    if(i.hasKey("path"))
                        gui.getConfig().set(i.getString("path") + ".slot", ie);
                }
            }
            GUIShopHandler.reload();
            load();
        }
    }

    @EventHandler
    void click(InventoryClickEvent e) {
        if(e.getSlot() <=-1
                || e.getClickedInventory()==null
                || e.getClickedInventory().getType() == InventoryType.CREATIVE
                || e.getClickedInventory().getType() == InventoryType.PLAYER) return;
        User u = CoreAPI.getUser(e.getWhoClicked().getName());
        if(rearrange.containsKey(u) && rearrange.get(u).getSecond() != null) {
            Inventory x = rearrange.get(u).getSecond();
            GUIShop gui = rearrange.get(u).getGui();
            if(e.getSlot() >= x.getSize()+-9 && gui.isNext()) {
                if(e.getSlot() == x.getSize()+-9) {
                    e.setCancelled(true);
                    if(gui.isNext()) {
                        if(e.getCursor()!=null) {
                            cursor.put(u, e.getCursor());
                        }
                        u.openInventory(prev.get(u).get(prev.get(u).size()+-1));
                        List<Inventory> i = prev.get(u);
                        i.remove(i.size()+-1);
                        prev.put(u, i);
                        e.setCursor(cursor.get(u));
                    }
                    return;
                }
                if(e.getSlot() == x.getSize()+-5) {
                    prev.remove(u);
                    cursor.remove(u);
                    rearrange.remove(u);
                    clickedClose.add(u);
                    for(int ie = 0; ie < e.getClickedInventory().getContents().length+-9;ie++) {
                        ItemStack z = e.getClickedInventory().getItem(ie);
                        if(z != null && !z.getType().name().contains("AIR")) {
                            ItemBuilder i = ItemBuilder.createItem(z);
                            if(i.hasKey("path"))
                                gui.getConfig().set(i.getString("path")+".slot", ie);
                        }
                    }
                    GUIShopHandler.reload();
                    load();
                    e.setCancelled(true);
                    u.openInventory(inv);
                    Bukkit.getScheduler().runTaskLater(Core.getAPI(), ()->{
                        clickedClose.remove(u);
                    },10);
                    return;
                }
                if(e.getSlot() == x.getSize()+-1 && gui.hasNext()) {
                    e.setCancelled(true);
                    if(gui.hasNext()) {
                        if(e.getCursor() != null) {
                            cursor.put(u, e.getCursor());
                        }

                        ItemBuilder i = ItemBuilder.createItem(e.getCursor());
                        if(i.hasKey("path")) {
                            String path = i.getString("path") + ".";
                            Config cfg = gui.getConfig();
                            gui.getNext().getConfig().set(path + "isSellable", cfg.getBoolean(path + "isSellable"))
                                    .set(path + "itemColor", cfg.getBoolean(path + "itemColor"))
                                    .set(path + "isBuyable", cfg.getBoolean(path + "isBuyable"))
                                    .set(path + "lore", cfg.getStringList(path + "lore"))
                                    .set(path + "buyCost", cfg.getDouble(path + "buyCost"))
                                    .set(path + "sellCost", cfg.getDouble(path + "sellCost"));
                            gui.getConfig().set(path.substring(0, path.length() + -1), null);
                        }
                        List<Inventory> pre = prev.get(u);
                        pre.add(e.getClickedInventory());
                        prev.put(u, pre);
                        u.openInventory(gui.getNext().getInventory());
                        (new BukkitRunnable() {
                            @Override
                            public void run() {
                                e.setCursor(cursor.get(u));

                            }
                        }).runTaskLater(Core.getAPI(), 5L);
                    }
                    return;
                }
                e.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler
    void onClick(InventoryClickEvent e) {
        if(e.getSlot() <=-1
                || e.getClickedInventory()==null
                || e.getClickedInventory().getType() == InventoryType.CREATIVE
                || e.getClickedInventory().getType() == InventoryType.PLAYER) return;
        if(e.getView().getTitle().equalsIgnoreCase(invName)) {
            if(e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta()) return;
            User u = CoreAPI.getUser(e.getWhoClicked().getName());
            ItemStack i = e.getCurrentItem();
            ItemMeta im = i.getItemMeta();
            String name = i.hasItemMeta() & im.hasDisplayName() ? ColorUtil.removeColor(im.getDisplayName()) : StringUtils.getNameFromEnum(i.getType());
            e.setCancelled(true);
            if(name.equalsIgnoreCase("Add a new GUI")) {
                u.sendMessage("[c]Type a name to use for a GUI, use UnderScores for spaces",
                        "[c]must contain 2 names and a number",
                        "[c]Name 1 is the file name",
                        "[c]Name 2 is the DisplayName",
                        "[c]Item Type is the Display Item",
                        "[c]And the final object should be a number (e.g 6R or 54S)",
                        "[pc]EXAMPLE: [c]blocks &[c]5Blocks Bricks 6R",
                        "[c]Type 'cancel' to cancel.");
                shopName.add(u);
                u.getBase().closeInventory();
                return;
            }
            GUIShop shop = GUIShopHandler.
                    getShop(name);
            if(shop != null) {
                ItemStack s = ItemBuilder.createWool(ItemBuilder.ColorType.RED).setName("[c]&lBack").setLore("[c]Return to Main page").buildItem();
                ItemStack a = ItemBuilder.createWool(ItemBuilder.ColorType.RED).setName("[c]&lBack").setLore("[c]Return to previous page").buildItem();
                Inventory ex = Bukkit.createInventory(null, GUIShopHandler.getShop(name).getInventory().getSize(), "§cEdit shop " + name);
                GUIShopHandler.reload();
                for(int j = 0; j < ex.getSize(); j++) {
                    ItemStack x = shop.getItem(j);
                    if(x != null)
                        ex.setItem(j, x);
                }
                for(int f = 1; f < 9; f++) {
                    ex.setItem(ex.getSize() +-f, null);
                }
                ex.setItem(ex.getSize()+-9, s);
                ItemStack f = ItemBuilder.createWool(ItemBuilder.ColorType.GREEN).setName("[c]&lAdd Item").setLore("[c]Adds items to the gui").buildItem();
                ex.setItem(ex.getSize()+-1, f);
                ex.setItem(ex.getSize()+-6, ItemBuilder.
                        createGlass(ItemBuilder.ColorType.GREEN, true).setName("§aRe-Arrange").buildItem());
                ex.setItem(ex.getSize()+-4, ItemBuilder.createGlass(ItemBuilder.ColorType.GREEN, true).setName("§aRe-Name").buildItem());
                u.openInventory(ex);
                return;
            }
            return;
        }
        if(e.getView().getTitle().startsWith("§cEdit shop ")) {
            if(e.getCurrentItem() == null) return;
            if(!e.getCurrentItem().hasItemMeta()) return;
            e.setCancelled(true);
            User u = CoreAPI.getUser(e.getWhoClicked().getName());
            ItemStack i = e.getCurrentItem();
            ItemMeta im = i.getItemMeta();
            String name = i.hasItemMeta() & im.hasDisplayName() ?
                    ColorUtil.removeColor(im.getDisplayName()) : StringUtils.getNameFromEnum(i.getType());
            GUIShop gui = GUIShopHandler.getShop(e.getView().getTitle().split("§cEdit shop ")[1]);
            if(name.equalsIgnoreCase("Add Item")) {
                addItem.put(u, gui);
                u.sendMessage("[c]Type 'cancel' to cancel");
                u.sendMessage("[c]Enter a ItemName Either Material name(From Bukkit/F3+H) or from Items.yml");
                u.getBase().closeInventory();
                return;
            }
            if(name.equalsIgnoreCase("Re-Name")) {
                inventory.put(u, new InvenHolder(gui, e.getClickedInventory()));
                u.sendMessage("[c]Type 'cancel' to cancel");
                u.sendMessage("[c]Enter a new name!");
                u.getBase().closeInventory();
                return;
            }
            if(name.equalsIgnoreCase("Re-Arrange")) {
                Inventory x = Bukkit.createInventory(null, gui.getInventory().getSize(), "§cRe-Arranging " + gui.getDisplayName());
                for(int j = 0; j < x.getSize() +-9; j++) {
                    ItemStack a = gui.getInventory().getItem(j);
                    if(a != null)
                        x.setItem(j, ItemBuilder.createItem(a).setString("path", gui.getPath(a)).buildItem());
                }
                if(gui.hasNext())
                    x.setItem(x.getSize()+-1, ItemBuilder.createWool(ItemBuilder.ColorType.GREEN)
                            .setName("[c]Next page").buildItem());

                x.setItem(x.getSize()+-5, ItemBuilder.createWool(ItemBuilder.ColorType.BLUE)
                        .setName("[c]Next Finish").buildItem());
                if(gui.isNext())
                    x.setItem(x.getSize()+-9, ItemBuilder.createWool(ItemBuilder.ColorType.GREEN)
                            .setName("[c]Previous page").buildItem());
                u.openInventory(x);
                Bukkit.getScheduler().runTaskLater(Core.getAPI(), ()-> {
                    rearrange.put(u, new InvenHolder(gui, e.getClickedInventory(), x));
                },10L);
                return;
            }
            if(name.equalsIgnoreCase("Back")) {
                u.getBase().openInventory(inv);
                return;
            }
            if(gui.getValue(e.getClickedInventory().getItem(e.getSlot())) == null) {
                return;
            }
            if(e.getClick() == ClickType.RIGHT) {
                gui.remove(e.getCurrentItem());
                ItemStack s = ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("[c]&lBack").setLore("[c]Return to Main page").buildItem();
                ItemStack a = ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("[c]&lBack").setLore("[c]Return to previous page").buildItem();
                String b = e.getView().getTitle().split("§cEdit shop ")[1];
                Inventory ex = Bukkit.createInventory(null, GUIShopHandler.getShop(b).getInventory().getSize(), "§cEdit shop " + b);
                GUIShopHandler.reload();
                ex.setContents(GUIShopHandler.getShop(b).getItems());
                for(int f = 1; f < 9; f++) {
                    ex.setItem(ex.getSize() +-f, null);
                }
                for(int f = 0; f < ex.getSize(); f++) {
                    ItemStack x = ex.getItem(f);
                    if(x != null && x.getType() != Material.AIR && !x.isSimilar(s) && !x.isSimilar(a)) {
                        ex.setItem(f, null);
                        ex.addItem(x);
                    }
                }
                ex.setItem(ex.getSize()+-9, s);
                ItemStack f = ItemBuilder.createWool(ItemBuilder.ColorType.GREEN).setName("[c]&lAdd Item").setLore("[c]Adds items to the gui").buildItem();
                ex.setItem(ex.getSize()+-1, f);
                u.getBase().openInventory(ex);
                u.sendMessage("[c]Removed item");
                return;
            }
            if(e.getClick() == ClickType.LEFT) {
                Inventory it = Bukkit.createInventory(null, InventoryType.HOPPER, "§cSelect an option");
                it.setItem(1, ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("&cChange BuyCost").buildItem());
                it.setItem(2, ItemBuilder.createWool(ItemBuilder.ColorType.BLUE).setName("&aCancel").buildItem());
                it.setItem(3, ItemBuilder.createWool(ItemBuilder.ColorType.RED).setName("&cChange SellCost").buildItem());
                ph.put(u, new Inven(e.getView().getTitle(), gui, e.getClickedInventory(), gui.getPath(e.getCurrentItem())));
                u.getBase().openInventory(it);
            }
            return;
        }
        if(e.getView().getTitle().equalsIgnoreCase("§cSelect an option")) {
            User u = CoreAPI.getUser(e.getWhoClicked().getName());
            ItemStack i = e.getCurrentItem();
            ItemMeta im = i.getItemMeta();
            String name = i.hasItemMeta() & im.hasDisplayName() ? ColorUtil.removeColor(im.getDisplayName()) : StringUtils.getNameFromEnum(i.getType());
            e.setCancelled(true);
            if(name.equalsIgnoreCase("Change SellCost")) {
                ph.put(u, new Inven(ph.get(u).getName(), ph.get(u).getGui(), ph.get(u).current, ph.get(u).getPath(), false, true));
                u.getBase().closeInventory();
                u.sendMessage("[c]Type 'cancel' to cancel");
                u.sendMessage("[c]*Changing Sell Price*");
                u.sendMessage("[c]Enter a price");
                return;
            }
            if(name.equalsIgnoreCase("Change BuyCost")) {
                ph.put(u, new Inven(ph.get(u).getName(), ph.get(u).getGui(), ph.get(u).current, ph.get(u).getPath(), true, false));
                u.getBase().closeInventory();
                u.sendMessage("[c]Type 'cancel' to cancel");
                u.sendMessage("[c]*Changing Buy Price*");
                u.sendMessage("[c]Enter a price");
                return;
            }
            if(name.equalsIgnoreCase("Cancel")) {
                u.getBase().openInventory(ph.get(u).current);
                ph.remove(u);
                return;
            }
        }
    }

    @EventHandler
    void onChat2(UserChatEvent e) {
        if(inventory.containsKey(e.getUser())) {
            e.setCancelled(true);
            if(e.getMessage().equalsIgnoreCase("cancel")) {
                inventory.remove(e.getUser());
                e.getUser().sendMessage("[c]Cancelling");
                return;
            }
            String prevName = inventory.get(e.getUser()).getGui().getDisplayName();
            inventory.get(e.getUser()).getGui().getConfig().set("Settings.Display", StringUtils.translate(e.getMessage()));
            e.getUser().sendMessage("[c]GUI: " + prevName + " is now '&r" + StringUtils.translate(e.getMessage()) + "&r[c]'");
            GUIShopHandler.reload();
            load();
            e.getUser().openInventory(inv);
            inventory.remove(e.getUser());
            for(User u : CoreAPI.getOnlineUsers()) {
                if(u.isCommandSpyEnabled() && !e.getUser().getName().equalsIgnoreCase(u.getName())) {
                    u.sendMessage("[pc]" + e.getUser().getName() + " [c]GUI: " + prevName + " is now '&r" + StringUtils.translate(e.getMessage()) + "&r[c]'");
                }
            }


            return;
        }
        if(addItem.containsKey(e.getUser())) {
            e.setCancelled(true);
            if(e.getMessage().equalsIgnoreCase("cancel")) {
                addItem.remove(e.getUser());
                e.getUser().sendMessage("[c]Cancelling");
                return;
            }
            String[] args = e.getMessage().split(" ");
            if(args.length != 1) {
                e.getUser().sendMessage("[c]Error, try again");
                return;
            }
            String af = e.getMessage().replace(" ", "_").toUpperCase();
            ItemStack a = ItemBuilder.toStack(af);
            if(a==null) {
                e.getUser().sendMessage("[c]Invalid item '[pc]"+ af+"[c]'.");
                return;
            }
            GUIShop s = addItem.get(e.getUser());
            for(User u : CoreAPI.getOnlineUsers()) {
                if(u.isCommandSpyEnabled() && !e.getUser().getName().equalsIgnoreCase(u.getName())) {
                    u.sendMessage("[pc]" + e.getUser().getName() + " [c]Adding [c]" + af + " to " + s.getDisplayName() );
                }
            }
            s.getConfig()
                    .set("Items."+af+".itemColor", "[c]")
                    .set("Items."+af+".buyCost", 1D)
                    .set("Items."+af+".sellCost", 1D)
                    .set("Items."+af+".lore", "'Click to buy a "+StringUtils.getNameFromEnum(a.getType())+"'");
            GUIShopHandler.reload();
            GUIShop r = GUIShopHandler.getShop(s.getName());
            ItemStack ss = ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("[c]&lBack").setLore("[c]Return to Main page").buildItem();
            ItemStack da = ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("[c]&lBack").setLore("[c]Return to previous page").buildItem();
            Inventory ex = Bukkit.createInventory(null, r.getInventory().getSize(), "§cEdit shop " + r.getDisplayName());
            GUIShopHandler.reload();
            for(int j = 0; j < ex.getSize(); j++) {
                ItemStack x = GUIShopHandler.getShop(s.getName()).getItem(j);
                if(x != null)
                    ex.setItem(j, x);
            }
            for(int f = 1; f < 9; f++) {
                ex.setItem(ex.getSize() +-f, null);
            }
//            for(int f = 0; f < ex.getSize(); f++) {
//                ItemStack x = ex.getItem(f);
//                if(x != null && x.getType() != Material.AIR && !x.isSimilar(ss) && !x.isSimilar(da)) {
//                    ex.setItem(f, null);
//                    ex.addItem(x);
//                }
//            }
            ex.setItem(ex.getSize()+-9, ss);
            ItemStack f = ItemBuilder.createWool(ItemBuilder.ColorType.GREEN).setName("[c]&lAdd Item").setLore("[c]Adds items to the gui").buildItem();
            ex.setItem(ex.getSize()+-1, f);
            e.getUser().sendMessage("[c]Added item '" + args[0] + "' to [c]'" + s.getDisplayName()+"[c]'");
            e.getUser().openInventory(ex);
            addItem.remove(e.getUser());
            return;
        }
        if(ph.get(e.getUser()) !=null) {
            e.setCancelled(true);
            if(e.getMessage().equalsIgnoreCase("cancel")) {
                ph.remove(e.getUser());
                e.getUser().sendMessage("[c]Cancelling");
                return;
            }
            String[] args = e.getMessage().split(" ");
            if(args.length != 1) {
                e.getUser().sendMessage("[c]Error, try again");
                return;
            }
            if(!NumberUtil.isDouble(args[0])) {
                e.getUser().sendMessage("[c]Invalid price");
                return;
            }
            for(User u : CoreAPI.getOnlineUsers()) {
                if(u.isCommandSpyEnabled() && !e.getUser().getName().equalsIgnoreCase(u.getName())) {
                    if(ph.get(e.getUser()).isSell)
                        u.sendMessage("[pc]" + e.getUser().getName() + " [c]Sell Price changed for [c]" +
                                ph.get(e.getUser()).getPath().replace("Items.","") + " to " + args[0]);
                    if(ph.get(e.getUser()).isBuy)
                        u.sendMessage("[pc]" + e.getUser().getName() + " [c]Buy Price changed for [c]" +
                                ph.get(e.getUser()).getPath().replace("Items.","") + " to " + args[0]);
                }
            }
            if(ph.get(e.getUser()).isSell)
                if(NumberUtil.getDouble(args[0]) < 0)
                    ph.get(e.getUser()).getGui().getConfig().set(ph.get(e.getUser()).path + ".isSellable", false);
                else
                    ph.get(e.getUser()).getGui().getConfig().set(ph.get(e.getUser()).path + ".sellCost", NumberUtil.getDouble(args[0]));
            if(ph.get(e.getUser()).isBuy) {
                if(NumberUtil.getDouble(args[0]) < 0)
                    ph.get(e.getUser()).getGui().getConfig().set(ph.get(e.getUser()).path + ".isBuyable", false);
                else
                    ph.get(e.getUser()).getGui().getConfig().set(ph.get(e.getUser()).path + ".buyCost", NumberUtil.getDouble(args[0]));
            }
            GUIShopHandler.reload();
            ItemStack s = ItemBuilder.createWool(ItemBuilder.ColorType.LIME).setName("[c]&lBack").setLore("[c]Return to Main page").buildItem();
            Inventory ex =
                    Bukkit.createInventory(null, GUIShopHandler.getShop(ph.get(e.getUser()).getName()
                            .split("§cEdit shop ")[1]).getInventory().getSize(), ph.get(e.getUser()).getName());
            GUIShopHandler.reload();
            for(int j = 0; j < ex.getSize(); j++) {
                ItemStack x = GUIShopHandler.getShop(ph.get(e.getUser()).getName()
                        .split("§cEdit shop ")[1]).getItem(j);
                if(x != null)
                    ex.setItem(j, x);
            }
            for(int f = 1; f < 9; f++) {
                ex.setItem(ex.getSize() +-f, null);
            }
            e.getUser().openInventory(ex);
            ph.remove(e.getUser());
        }
    }

    private static boolean hasItem(Inventory i, ItemStack item) {
        for(ItemStack a : i.getContents()) {
            if(item!=null&&a!=null&&a.isSimilar(item)) return true;
        }
        return false;
    }
    private static int getItemSlot(Inventory i, ItemStack item) {
        for(int x = 0; x < i.getContents().length; x++) {
            ItemStack a = i.getItem(x);
            if(item!=null&&a!=null&&a.isSimilar(item)) return x;
        }
        return -1;
    }

    protected class InvenHolder {
        private Inventory current, secondInv;
        private GUIShop gui;


        InvenHolder(GUIShop gui, Inventory current) {
            this.gui = gui;
            this.current = current;

        }InvenHolder(GUIShop gui, Inventory current, Inventory secondInv) {
            this.gui = gui;
            this.current = current;
            this.secondInv = secondInv;

        }
        public GUIShop getGui() {
            return gui;
        }
        public Inventory getCurrent() {
            return current;
        }
        public Inventory getSecond() {
            return secondInv;
        }

    }

    protected class Inven {
        private GUIShop gui;
        private Inventory prev;
        private Inventory current;
        private String path;
        private boolean isBuy;
        private boolean isSell;
        private String name;

        Inven(String name, GUIShop gui, Inventory current, String path) {
            this.gui = gui;
            this.current = current;
            this.path = path;
            this.name = name;
        }
        Inven(String name, GUIShop gui, Inventory prev, String path, boolean isBuy, boolean isSell) {
            this.gui = gui;
            this.prev = prev;
            this.path = path;
            this.isBuy = isBuy;
            this.isSell = isSell;
            this.name = name;
        }

        public GUIShop getGui() {
            return gui;
        }

        public String getPath() {
            return path;
        }

        public boolean isSell() {
            return isSell;
        }

        public boolean isBuy() {
            return isBuy;
        }

        public Inventory getPrev() {
            return prev;
        }

        public Inventory getCurrent() {
            return current;
        }

        public String getName() {
            return name;
        }
    }


}
