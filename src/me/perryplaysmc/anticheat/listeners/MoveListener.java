package me.perryplaysmc.anticheat.listeners;

import me.perryplaysmc.anticheat.checks.CheckResult;
import me.perryplaysmc.anticheat.checks.Distance;
import me.perryplaysmc.anticheat.checks.Settings;
import me.perryplaysmc.anticheat.checks.movement.NoSlowDown;
import me.perryplaysmc.anticheat.checks.movement.SpeedCheck;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.listeners
 * Path: me.perryplaysmc.anticheat.listeners.MoveListener
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class MoveListener implements Listener {
    
    public MoveListener(){
        if(Bukkit.getPluginManager().getPermission("anticheat.bypass.*")==null)
            Bukkit.getPluginManager().addPermission(new Permission("anticheat.bypass.*".toLowerCase()));
    }
    
    public static double speed(User u) {
        Player player = u.getBase();
        if(player.isFlying() && !player.isOnGround()) {
            //Integer.parseInt(("" + player.getFlySpeed()).split("\\.")[1])
            double speed = Double.parseDouble((player.getFlySpeed()+"").substring(0, 3));
            if(player.getFlySpeed() < 1 && player.getFlySpeed() > 0) {
                return speed;
            }
            if(player.getFlySpeed() > -1 && player.getFlySpeed() < 0) {
                return speed;
            }
            return speed;
        }
        //Integer.parseInt(("" + player.getWalkSpeed()).split("\\.")[1])
        double speed = Double.parseDouble((player.getWalkSpeed()+"").substring(0, 3));
        if(player.getWalkSpeed() < 1 && player.getWalkSpeed() > 0) {
            return speed;
        }
        if(player.getWalkSpeed() > -1 && player.getWalkSpeed() < 0) {
            return speed;
        }
        return speed;
    }
    
    double ldf, ldfs, ldfsb, ldfb, ldw, ldws, ldwsb, ldwb, ldwg;
    
    @EventHandler
    void onHit(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player) {
            User u = CoreAPI.getUser(((Player) e.getEntity()));
            u.getAntiCheatDetector().toggleMoveCheck();
        }
    }
    
    @EventHandler
    void onMove(PlayerMoveEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        Distance d = new Distance(e);
//        double dist = (d.getDist()/speed(u));
//        runChecks(u, d, dist);
//        if(true)return;
        if(u==null)return;
        if(u.getAntiCheatDetector().isCancelMoveCheck()){
            u.getAntiCheatDetector().toggleMoveCheck();
            return;
        }
        CheckResult speed = SpeedCheck.runCheck(d, u);
        CheckResult noslow = NoSlowDown.runCheck(d, u);
        NoSlowDown.registerMove(d, u);
        if(speed.failed() && !u.getAntiCheatDetector().isIgnoreSpeed()) {
            e.setTo(e.getFrom());
            Settings.log(speed, u);
            return;
        }
        if(u.getBase().isSneaking())
            (new BukkitRunnable() {
                @Override
                public void run() {
                    if(noslow.failed()) {
                        e.setTo(e.getFrom());
                        Settings.log(noslow, u);
                    }
                }
            }).runTaskLater(Core.getAPI(), 15);
        else if(noslow.failed()) {
            e.setTo(e.getFrom());
            Settings.log(noslow, u);
        }
    }
    
    void runChecks(User u, Distance d, double dist) {
        if(u.getBase().isFlying())
            if(u.getBase().isSneaking()) {
                if(u.getBase().isBlocking()) {
                    if(dist > ldwb) {
                        ldwb = dist;
                        u.sendMessage("[c]Fly-Sneak-Blocking: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                } else {
                    if(dist > ldfs) {
                        ldfs = dist;
                        u.sendMessage("[c]Fly-Sneak: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                }
            }else{
                if(u.getBase().isBlocking()) {
                    if(dist > ldwb) {
                        ldwb = dist;
                        u.sendMessage("[c]Fly-Blocking: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                }else {
                    if(dist > ldf) {
                        ldf = dist;
                        u.sendMessage("[c]Fly: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                }
            }else {
            if(u.getBase().isSneaking()) {
                if(u.getBase().isBlocking()) {
                    if(dist > ldwsb) {
                        ldwsb = dist;
                        u.sendMessage("[c]Walk-Sneak-Blocking: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                } else {
                    if(dist > ldws) {
                        ldws = dist;
                        u.sendMessage("[c]Walk-Sneak: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                }
            } else {
                if(u.getBase().isBlocking()) {
                    if(dist > ldwb) {
                        ldwb = dist;
                        u.sendMessage("[c]Walk-Blocking: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                    }
                } else {
                    if(!u.getBase().isOnGround()) {
                        if(dist > ldwg) {
                            ldwg = dist;
                            u.sendMessage("[c]Walk-Air: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                        }
                    }else {
                        if(dist > ldw) {
                            ldw = dist;
                            u.sendMessage("[c]Walk: " + speed(u) + " [pc]" + dist + "-" + d.getDist());
                        }
                    }
                }
            }
        }
    }
    
    
}
