package me.perryplaysmc.anticheat.listeners;

import me.perryplaysmc.anticheat.checks.CheckResult;
import me.perryplaysmc.anticheat.checks.CheckType;
import me.perryplaysmc.anticheat.checks.Level;
import me.perryplaysmc.anticheat.checks.Settings;
import me.perryplaysmc.anticheat.checks.movement.FastUse;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.data.AntiCheatDetector;
import me.perryplaysmc.utils.inventory.ItemUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/13/19-2023
 * Package: me.perryplaysmc.anticheat.listeners
 * Path: me.perryplaysmc.anticheat.listeners.PlayerListener
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class PlayerListener implements Listener {
    
    @EventHandler
    void onBlockBreak(BlockBreakEvent e) {
    
    }
    
    @EventHandler
    void onInteract(PlayerInteractEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(u==null)return;
        AntiCheatDetector d = u.getAntiCheatDetector();
        if(!e.getAction().name().contains("RIGHT")) return;
        if(ItemUtil.isAir(u.getItemInHand()))return;
        if(Settings.FOOD.contains(u.getItemInHand().getType())) {
            if(u.hasPermission(CheckType.NOSLOW.getPermission(), "anticheat.bypass.*")) return;
            d.startFood();
            d.resetFoodCount();
        }
        if(u.getItemInHand().getType()==Material.BOW && u.getInventory().contains(Material.ARROW)) {
            d.startBow();
        }
        
    }
    
    @EventHandler
    void foodChange(FoodLevelChangeEvent e) {
        User u = CoreAPI.getUser(e.getEntity().getUniqueId());
        if(u==null)return;
        AntiCheatDetector d = u.getAntiCheatDetector();
        if(ItemUtil.isAir(u.getItemInHand())||!Settings.FOOD.contains(u.getItemInHand().getType()))return;
        if(d.getInvalidFood() != 0) {
            if(u.hasPermission(CheckType.NOSLOW.getPermission(), "anticheat.bypass.*")) return;
            e.setCancelled(true);
            u.teleport(d.getFoodStartLocation());
            Settings.log(new CheckResult(Level.FAILED, CheckType.NOSLOW, "tried to move too fast whilst eating").tooltip(
                    "[c]Speed: ([pc]" + d.speed() + "[c])",
                    "[c]MaxSpeed: ([pc]" + Settings.MAX_XZ_EATING_SPEED + "[c])",
                    "[c]Tries: ([pc]" + d.getInvalidFood()+"[c])",
                    "[c]Max: ([pc]0[c])"
            ), u);
        }
        CheckResult result = FastUse.runFood(u);
        if(result.failed()) {
            e.setCancelled(true);
            Settings.log(result, u);
        }
    }
    
    @EventHandler
    void onShoot(ProjectileLaunchEvent e) {
        if(e.getEntity().getShooter()!=null&&e.getEntity().getShooter() instanceof Player) {
            User u = CoreAPI.getUser(((Player) e.getEntity().getShooter()).getUniqueId());
            if(u==null)return;
            CheckResult result = FastUse.runBow(u);
            if(result.failed()) {
                e.setCancelled(true);
                Settings.log(result, u);
            }
        }
    }
    
    @EventHandler
    void onSwitch(PlayerItemHeldEvent e) {
        User u = CoreAPI.getUser(e.getPlayer().getUniqueId());
        if(u==null)return;
        u.getAntiCheatDetector().stopBow();
    }
    
    
}
