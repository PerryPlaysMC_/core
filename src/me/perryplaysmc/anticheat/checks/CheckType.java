package me.perryplaysmc.anticheat.checks;

import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.checks
 * Path: me.perryplaysmc.anticheat.checks.CheckType
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public enum CheckType {
    SPEED("anticheat.bypass.speed"),
    NOSLOW("anticheat.bypass.noslow"),
    FASTUSE("anticheat.fastuse");
    
    String perm;
    CheckType(String perm) {
        this.perm = (perm.toLowerCase());
        Bukkit.getPluginManager().addPermission(new Permission(perm.toLowerCase()));
    }
    
    public String getName() {
        return StringUtils.getNameFromEnum(this);
    }
    
    public String getPermission() {
        return perm;
    }
    
}
