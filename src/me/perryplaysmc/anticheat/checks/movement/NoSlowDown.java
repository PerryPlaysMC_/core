package me.perryplaysmc.anticheat.checks.movement;

import me.perryplaysmc.anticheat.checks.*;
import me.perryplaysmc.user.User;

import static me.perryplaysmc.anticheat.listeners.MoveListener.speed;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/13/19-2023
 * Package: me.perryplaysmc.anticheat.checks.movement
 * Path: me.perryplaysmc.anticheat.checks.movement.NoSlowDown
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class NoSlowDown {
    
    private static final CheckResult PASS = new CheckResult(Level.PASSED, CheckType.NOSLOW, "Nothing to report");
    
    public static void registerMove(Distance d, User u) {
        double dist = d.getDist()/speed(u);
        if(dist > Settings.MAX_XZ_EATING_SPEED && u.getAntiCheatDetector().getFood()!=null && (System.currentTimeMillis()-u.getAntiCheatDetector().getFood()) > 1200) {
            u.getAntiCheatDetector().setSpeed(dist);
            u.getAntiCheatDetector().addInvalidFood();
        }
    }
    public static CheckResult runCheck(Distance d, User u) {
        if(u.hasPermission(CheckType.NOSLOW.getPermission(), "anticheat.bypass.*")) return new CheckResult(Level.PASSED, CheckType.NOSLOW, "Nothing to report");
        
        Double max = Settings.MAX_XZ_BLOCKING_SPEED;
        Double xz_speed = (d.getxDiff() > d.getzDiff() ? d.getxDiff() : d.getzDiff());
        if(u.getBase().isBlocking() && xz_speed > max) {
            return new CheckResult(Level.FAILED, CheckType.NOSLOW, "tried to move too fast whilst blocking").tooltip("[c]Speed: ([pc]" + xz_speed + "[c])", "[c]Max: ([pc]" + max + "[c])");
        }
        max = Settings.MAX_XZ_SNEAK_FLY_SPEED;
        if(u.getBase().isFlying()&&u.getBase().isSneaking() && xz_speed > max) {
            return new CheckResult(Level.FAILED, CheckType.NOSLOW, "tried to move too fast whilst fly-sneaking").tooltip("[c]Speed: ([pc]" + xz_speed + "[c])", "[c]Max: ([pc]" + max + "[c])");
        }
        max = Settings.MAX_XZ_SNEAK_SPEED;
        if(u.getBase().isSneaking() && xz_speed > max) {
            return new CheckResult(Level.FAILED, CheckType.NOSLOW, "tried to move too fast whilst sneaking").tooltip("[c]Speed: ([pc]" + xz_speed + "[c])", "[c]Max: ([pc]" + max + "[c])");
        }
        return PASS;
    }
}