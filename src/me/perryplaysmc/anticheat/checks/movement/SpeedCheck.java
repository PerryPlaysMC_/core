package me.perryplaysmc.anticheat.checks.movement;

import me.perryplaysmc.anticheat.checks.*;
import me.perryplaysmc.user.User;

import static me.perryplaysmc.anticheat.listeners.MoveListener.speed;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.checks.movement
 * Path: me.perryplaysmc.anticheat.checks.movement.SpeedCheck
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class SpeedCheck {
    private static final CheckResult PASS = new CheckResult(Level.PASSED, CheckType.SPEED, "Nothing to report");
    
    public static CheckResult runCheck(Distance d, User u) {
        if(u.hasPermission(CheckType.SPEED.getPermission(), "anticheat.bypass.*")) return new CheckResult(Level.PASSED, CheckType.SPEED, "Nothing to report");
        
        Double xz_speed = (d.getxDiff() > d.getzDiff() ? d.getxDiff() : d.getzDiff())/speed(u);
        Double max = Settings.MAX_XZ_FLY_SPEED;
        if(u.getBase().isFlying())
            if(xz_speed > max)
                return new CheckResult(Level.FAILED, CheckType.NOSLOW, "tried to move too fast whilst flying").tooltip("[c]Speed: ([pc]" + xz_speed + "[c])", "[c]Max: ([pc]" + max + "[c])");
            else{}
        else {
            max = Settings.MAX_XZ_SPEED;
            if(!u.getBase().isOnGround())
                max = Settings.MAX_XZ_JUMP_SPEED;
            if(xz_speed > max)
                return new CheckResult(Level.FAILED, CheckType.SPEED,
                        "tried to move faster than normal" + (max == Settings.MAX_XZ_JUMP_SPEED ? " whilst jumping" : "")).tooltip("[c]Speed: ([pc]" + xz_speed + "[c])", "[c]Max: ([pc]" + max + "[c])");
        }
        return PASS;
    }
}
