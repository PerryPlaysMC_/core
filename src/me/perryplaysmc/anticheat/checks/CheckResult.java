package me.perryplaysmc.anticheat.checks;

import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.chat.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.checks
 * Path: me.perryplaysmc.anticheat.checks.CheckResult
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CheckResult {
    
    
    private Level level;
    private String message, suggest;
    private List<String> tooltip;
    private CheckType type;
    
    public CheckResult(Level level, CheckType type, String message) {
        this.level = level;
        this.message = StringUtils.translate(message);
        this.type = type;
        tooltip = new ArrayList<>();
    }
    
    public CheckResult tooltip(String... text) {
        tooltip.clear();
        for(String s : text)
            if(!tooltip.contains(s))
                tooltip.add(s);
        return this;
    }
    
    public CheckResult suggest(String text) {
        this.suggest = text;
        return this;
    }
    
    public boolean failed() {
        return level == Level.FAILED;
    }
    
    public Level getLevel() {
        return level;
    }
    
    public CheckType getType() {
        return type;
    }
    
    public String getMessage() {
        return message;
    }
    
    public String getSuggest() {
        return suggest;
    }
    
    public List<String> getTooltip() {
        return tooltip;
    }
    
    
    
}
