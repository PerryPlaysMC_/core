package me.perryplaysmc.exceptions;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/2/19-2023
 * Package: me.perryplaysmc.exceptions
 * Path: me.perryplaysmc.exceptions.InvalidLocationException
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class InvalidLocationException extends Exception {
    
    public InvalidLocationException(String s) {
        super(s);
    }
}