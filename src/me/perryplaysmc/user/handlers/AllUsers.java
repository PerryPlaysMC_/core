package me.perryplaysmc.user.handlers;

import com.mojang.authlib.GameProfile;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.permissions.PermissionUserData;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.data.AntiCheatDetector;
import me.perryplaysmc.utils.inventory.CustomInventory;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.Permission;

import java.util.UUID;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/10/19-2023
 * Package: me.perryplaysmc.user.handlers
 * Path: me.perryplaysmc.user.handlers.AllUsers
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/
public class AllUsers implements User {
    @Override
    public Player getBase() {
        return null;
    }
    
    @Override
    public Player getAsPlayer() {
        return null;
    }
    
    @Override
    public String getName() {
        return "@a";
    }
    
    @Override
    public String getRealName() {
        return "@a";
    }
    
    @Override
    public Config getConfig() {
        return null;
    }
    
    @Override
    public ChatSettings getChatSettings() {
        return null;
    }
    
    @Override
    public boolean isOnline() {
        return true;
    }
    
    @Override
    public UUID getUniqueId() {
        return null;
    }
    
    @Override
    public BankAccount getAccount() {
        return null;
    }
    
    @Override
    public HomeData getHomeData() {
        return null;
    }
    
    @Override
    public ShopHandler getShopHandler() {
        return null;
    }
    
    @Override
    public RankUtils getRankUtils() {
        return null;
    }
    
    @Override
    public PermissionUserData getPermissionData() {
        return null;
    }
    
    @Override
    public PlayerInventory getInventory() {
        return null;
    }
    
    @Override
    public boolean hasPermission(String... permissions) {
        return false;
    }
    
    @Override
    public boolean hasPermission(Permission... permissions) {
        return false;
    }
    
    @Override
    public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendTitle(title, subtitle, fadeIn, stay, fadeOut);
        }
    }
    
    @Override
    public void sendTitle(String title) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendTitle(title);
        }
    }
    
    @Override
    public void sendAction(String message) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendAction(message);
        }
    }
    
    @Override
    public void sendMessage(Object... messages) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendMessage(messages);
        }
    }
    
    @Override
    public void sendMessage(Message... messages) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendMessage(messages);
        }
    }
    
    @Override
    public void sendMessageFormat(String location, Object... objects) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendMessageFormat(location, objects);
        }
    }
    
    @Override
    public void sendMessageFormat(Config cfg, String location, Object... objects) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.sendMessageFormat(cfg, location, objects);
        }
    }
    
    @Override
    public void kick(String reason) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.kick(reason);
        }
    }
    
    @Override
    public void openInventory(Inventory inv) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.openInventory(inv);
        }
    }
    
    @Override
    public void openInventory(CustomInventory inv) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.openInventory(inv);
        }
    }
    
    @Override
    public void updateInventory() {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.updateInventory();
        }
    }
    
    @Override
    public ItemStack getItemInHand() {
        return null;
    }
    
    @Override
    public void setProfile(GameProfile pro) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.setProfile(pro);
        }
    }
    
    @Override
    public GameProfile getProfile() {
        return null;
    }
    
    @Override
    public Class<?> getEntityHumanClass() {
        return null;
    }
    
    @Override
    public void setName(String name) {
    
    }
    
    @Override
    public void toggleVeinMine() {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.toggleVeinMine();
        }
    }
    
    @Override
    public boolean isVeinMine() {
        return false;
    }
    
    @Override
    public Location getLocation() {
        return null;
    }
    
    @Override
    public World getWorld() {
        return null;
    }
    
    @Override
    public void teleport(Location loc) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.teleport(loc);
        }
    }
    
    @Override
    public void teleport(Entity entity) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.teleport(entity);
        }
    }
    
    @Override
    public AntiCheatDetector getAntiCheatDetector() {
        return null;
    }
    
    @Override
    public UUID getOriginalUniqueId() {
        return null;
    }
    
    @Override
    public String getOriginalName() {
        return null;
    }
    
    @Override
    public Player getOriginalBase() {
        return null;
    }
    
    @Override
    public void kill() {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.kill();
        }
    }
    
    @Override
    public void setHealth(double d) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.setHealth(d);
        }
    }
    
    @Override
    public double getHealth() {
        return 0;
    }
    
    @Override
    public void setMaxHealth(double d) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.setMaxHealth(d);
        }
    }
    
    @Override
    public double getMaxHealth() {
        return 0;
    }
    
    @Override
    public boolean isCooldown(String key) {
        return false;
    }
    
    @Override
    public void startCooldown(String key, double time) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.startCooldown(key, time);
        }
    }
    
    @Override
    public double getCooldownRemains(String key) {
        return 0;
    }
    
    @Override
    public boolean isCaptured() {
        return false;
    }
    
    @Override
    public void setCaptured(boolean bool) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.setCaptured(bool);
        }
    }
    
    @Override
    public boolean isCommandSpyEnabled() {
        return false;
    }
    
    @Override
    public void toggleCommandSpy() {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.toggleCommandSpy();
        }
    }
    
    @Override
    public void setCommandSpy(boolean enabled) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.setCommandSpy(enabled);
        }
    }
    
    @Override
    public boolean isNicked() {
        return false;
    }
    
    @Override
    public void setNick(String nick) {
    
    }
    
    @Override
    public void toggleNick() {
    
    }
    
    @Override
    public boolean nicked() {
        return false;
    }
    
    @Override
    public String getNickName() {
        return null;
    }
    
    @Override
    public CommandSource getReplier() {
        return null;
    }
    
    @Override
    public void setReplier(CommandSource s) {
    
    }
    
    @Override
    public boolean isVanished() {
        return false;
    }
    
    @Override
    public void setVanished(boolean isVanished) {
        for(User u : CoreAPI.getOnlineUsers()) {
            u.setVanished(isVanished);
        }
    }
    
    @Override
    public boolean isOp() {
        return false;
    }
    
    @Override
    public void setOp(boolean b) {
    
    }
}
