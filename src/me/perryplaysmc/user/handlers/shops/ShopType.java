package me.perryplaysmc.user.handlers.shops;

public enum ShopType
{
  BUY,  SELL;
  
  private ShopType() {}
}
