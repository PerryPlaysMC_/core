package me.perryplaysmc.user.handlers;

import me.perryplaysmc.user.handlers.shops.Shop;
import me.perryplaysmc.user.handlers.shops.ShopType;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.world.LocationUtil;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class ShopHandler {
    
    Config cfg;
    
    public ShopHandler(OfflineUser u) {
        cfg = u.getConfig();
    }
    
    public ShopHandler(User u) {
        cfg = u.getConfig();
    }
    
    
    
    public List<Shop> buyShops() {
        cfg.reload();
        if(cfg.getSection("ChestShop.Buy") == null || cfg.getSection("ChestShop.Buy").getKeys(false).size() == 0) return new ArrayList<>();
        List<Shop> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            locs.add(new Shop(ShopType.BUY, LocationUtil.stringToLocation(cfg.getString("ChestShop.Buy." + a))));
            
        }
        return locs;
    }
    
    
    public void addBuy(Location loc) {
        int i = cfg.getSection("ChestShop.Buy").getKeys(false).size()+1;
        cfg.set("ChestShop.Buy."+ i, LocationUtil.locationToString(loc));
    }
    
    
    public void removeBuy(Location loc) {
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            if(LocationUtil.isSimilar(LocationUtil.stringToLocation(cfg.getString("ChestShop.Buy." + a)), loc)) {
                cfg.set("ChestShop.Buy."+a, null);
            }
        }
        List<Location> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            locs.add(LocationUtil.stringToLocation(a));
        }
        for(String a : cfg.getSection("ChestShop.Buy").getKeys(false)) {
            cfg.set("ChestShop."+a, null);
        }
        int i = cfg.getSection("ChestShop.Buy").getKeys(false).size()+1;
        for(Location a : locs) {
            cfg.set("ChestShop.Buy."+i, LocationUtil.locationToString(a));
            i++;
        }
    }
    
    public List<Shop> sellShops() {
        cfg.reload();
        if(cfg.getSection("ChestShop.Sell") == null || cfg.getSection("ChestShop.Sell").getKeys(false).size() == 0) return new ArrayList<>();
        List<Shop> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            locs.add(new Shop(ShopType.SELL, LocationUtil.stringToLocation(cfg.getString("ChestShop.Sell."+a))));
        }
        return locs;
    }
    
    
    public void addSell(Location loc) {
        int i = cfg.getSection("ChestShop.Sell").getKeys(false).size()+1;
        cfg.set("ChestShop.Sell."+ i, LocationUtil.locationToString(loc));
    }
    
    public Shop getShop(Location loc) {
        if(sellShops() != null && sellShops().size() > 0)
            for(Shop s : sellShops()) {
                if(LocationUtil.isSimilar(s.getLoc(), loc)) return s;
            }
        if(buyShops() != null && buyShops().size() > 0)
            for(Shop s : buyShops()) {
                if(LocationUtil.isSimilar(s.getLoc(), loc)) return s;
            }
        return null;
    }
    
    
    public void removeSell(Location loc) {
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            if(LocationUtil.isSimilar(LocationUtil.stringToLocation(cfg.getString("ChestShop.Sell."+a)), loc)) {
                cfg.set("ChestShop.Sell."+a, null);
            }
        }
        List<Location> locs = new ArrayList<>();
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            locs.add(LocationUtil.stringToLocation(a));
        }
        for(String a : cfg.getSection("ChestShop.Sell").getKeys(false)) {
            cfg.set("ChestShop."+a, null);
        }
        int i = cfg.getSection("ChestShop.Sell").getKeys(false).size()+1;
        for(Location a : locs) {
            cfg.set("ChestShop.Sell."+i, LocationUtil.locationToString(a));
            i++;
        }
    }
    
    
    
}
