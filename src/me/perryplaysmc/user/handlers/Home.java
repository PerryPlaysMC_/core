package me.perryplaysmc.user.handlers;

import org.bukkit.Location;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/5/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Home  {
    private String name;
    private Location loc;
    
    Home(String name, Location loc) {
        this.name = name;
        this.loc = loc;
    }
    
    public Location getLocation() {
        return loc;
    }
    
    public String getName() {
        return name;
    }
}
