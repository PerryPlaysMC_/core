package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.TimeUtil;
import me.perryplaysmc.permissions.PermissionUserData;
import me.perryplaysmc.utils.helper.ban.BanData;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.UUID;

public class BaseOfflineUser implements OfflineUser {

    private OfflinePlayer base;
    private Config cfg;
    private BankAccount account;
    private RankUtils rankUtils;
    private PermissionUserData data;
    private ShopHandler handler;
    private HomeData homeData;
    private ChatSettings chatSettings;

    public BaseOfflineUser(OfflinePlayer base) {
        this.base = base;
        File f = new File(Config.defaultDirectory, "userData");
        this.cfg = new Config(f, base.getUniqueId().toString()+".yml");
        User u = CoreAPI.getUser(base.getUniqueId());
        if(u!=null) {
            this.cfg = u.getConfig();
        }
        account = new BankAccount(this);
        handler = new ShopHandler(this);
        data = new PermissionUserData(this);
        homeData = new HomeData(this);
        chatSettings = new ChatSettings(this);
        rankUtils = new RankUtils(this);
        if(!CoreAPI.getBalanceTops().contains(getUniqueId()))
            CoreAPI.getBalanceTops().add(getUniqueId());
        (new BukkitRunnable() {
            @Override
            public void run() {
                BanData b = CoreAPI.getBan(getUniqueId());
                if(b != null) {
                    if(b.getExpire() == -1) {
                        return;
                    }
                    if(TimeUtil.formatDateDiff((b.getExpire())).equalsIgnoreCase("Now")) {
                        CoreAPI.pardonPlayer(getUniqueId());
                    }
                }
            }
        }).runTaskTimer(Core.getAPI(), 0, 20);
    }


    @Override
    public OfflinePlayer getBase() {
        return base;
    }

    @Override
    public Player getAsPlayer() {
        return Core.getAPI().loadPlayer(base);
    }
    
    @Override
    public String getName() {
        return !isVanished() ? base.getName() : "Anonymous" ;
    }
    
    @Override
    public String getRealName() {
        return base.getName();
    }
    
    @Override
    public Config getConfig() {
        return CoreAPI.getUser(getName()) != null ? CoreAPI.getUser(getName()).getConfig() : cfg;
    }
    
    @Override
    public ChatSettings getChatSettings() {
        return chatSettings;
    }
    
    @Override
    public boolean isOnline() {
        return base.isOnline();
    }

    @Override
    public UUID getUniqueId() {
        return base.getUniqueId();
    }
    
    @Override
    public BankAccount getAccount() {
        return account;
    }
    
    @Override
    public HomeData getHomeData() {
        return homeData;
    }
    
    @Override
    public ShopHandler getShopHandler() {
        return handler;
    }
    
    @Override
    public RankUtils getRankUtils() {
        return rankUtils;
    }
    
    @Override
    public PermissionUserData getPermissionData() {
        return data;
    }

    @Override
    public PlayerInventory getInventory() {
        return Core.getAPI().loadPlayer(base).getInventory();
    }
    
    @Override
    public boolean isVanished() {
        return cfg.getBoolean("settings.isVanished");
    }
    
    @Override
    public void setVanished(boolean isVanished) {
        cfg.set("settings.isVanished", isVanished);
    }
    
    @Override
    public boolean isOp() {
        return base.isOp();
    }
    
    @Override
    public void setOp(boolean b) {
        base.setOp(b);
    }
}
