package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.CustomInventory;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

@SuppressWarnings("all")
public class Inventory {
    
    private OfflineUser u;
    private PlayerInventory inv;
    private CustomInventory inv2;
    private ItemStack helmet = ItemBuilder.
            createItem(new ItemStack(Material.BARRIER, 1)).setName("&cHelmet Item").buildItem(),
            chestplate = ItemBuilder.
                    createItem(new ItemStack(Material.BARRIER, 1)).setName("&cChestplate Item").buildItem(),
            leggings = ItemBuilder.
                    createItem(new ItemStack(Material.BARRIER, 1)).setName("&cLeggings Item").buildItem(),
            boots = ItemBuilder.
                    createItem(new ItemStack(Material.BARRIER, 1)).setName("&cBoots Item").buildItem(),
            offhand = ItemBuilder.
                    createItem(new ItemStack(Material.BARRIER, 1)).setName("&cOffHand Item").buildItem(),
            mainhand = ItemBuilder.
                    createItem(new ItemStack(Material.BARRIER, 1)).setName("&cMainHand Item").buildItem();
    
    public Inventory(OfflineUser u) {
        inv2 = new CustomInventory(45, u.getRealName());
        inv = u.getInventory();
        this.u = u;
        setArmor();
        update(false);
    }
    
    public void openInventory(User u) {
        u.openInventory(inv2);
    }
    
    public void onClick(InventoryClickEvent e) {
        if(e.getClickedInventory() != null && e.getClickedInventory().getType() == InventoryType.PLAYER) {
            if(e.getClickedInventory() instanceof PlayerInventory && this.inv != null) {
                PlayerInventory inv = (PlayerInventory) e.getClickedInventory();
                if(inv.getHolder().getName() == this.inv.getHolder().getName() &&e.getSlot()<=36&& inv2 != null && inv2.getViewers().size()>0) {
                    update(false);
                }
            }
            return;
        }
        if(!(e.getClickedInventory()!=null&&e.getClickedInventory().getType()==inv2.getInventory().getType())) return;
        if(!e.getView().getTitle().equalsIgnoreCase(inv2.getName())) return;
        User u = CoreAPI.getUser(e.getWhoClicked().getName());
        if((e.isShiftClick()&&e.getSlot()<=8) || !u.hasPermission("inventorysee.interact")){
            e.setCancelled(true);
            return;
        }
        if((!Version.isVersionHigherThan(true, Versions.v1_9)&&e.getSlot()==1)||e.getSlot()==2||e.getSlot()==3||e.getSlot()==4) {
            e.setCancelled(true);
        }
        ItemStack is2 = e.getCursor();
        ItemStack ci = e.getCurrentItem();
        
        if(!is2.isSimilar(mainhand)&&e.getSlot()==0)
            e.setCancelled(true);
        if(!is2.isSimilar(offhand)&&(!Version.isVersionHigherThan(true, Versions.v1_9)&&e.getSlot()==1))
            inv.setItemInOffHand(is2);
        if(!is2.isSimilar(helmet)&&e.getSlot()==5)
            inv.setHelmet(is2);
        if(!is2.isSimilar(chestplate)&&e.getSlot()==6)
            inv.setChestplate(is2);
        if(!is2.isSimilar(leggings)&&e.getSlot()==7)
            inv.setLeggings(is2);
        if(!is2.isSimilar(boots)&&e.getSlot()==8)
            inv.setBoots(is2);

        (new BukkitRunnable(){
            @Override
            public void run() {
                if(e.getCursor().isSimilar(mainhand))
                    e.setCursor(null);
                if(e.getCursor().isSimilar(offhand))
                    e.setCursor(null);
                if(e.getCursor().isSimilar(helmet))
                    e.setCursor(null);
                if(e.getCursor().isSimilar(chestplate))
                    e.setCursor(null);
                if(e.getCursor().isSimilar(leggings))
                    e.setCursor(null);
                if(e.getCursor().isSimilar(boots))
                    e.setCursor(null);
            }

        }).runTaskLater(Core.getAPI(), 2L);
        (new BukkitRunnable(){
            @Override
            public void run() {
                ItemStack item = e.getClickedInventory().getItem(e.getSlot());
                if(item == null || item.getType().name().contains("AIR")) {
                    if(e.getSlot()==0)
                        e.getClickedInventory().setItem(e.getSlot(), mainhand);
                    if((!Version.isVersionHigherThan(true, Versions.v1_9)&&e.getSlot()==1))
                        e.getClickedInventory().setItem(e.getSlot(), offhand);
                    if(e.getSlot()==5)
                        e.getClickedInventory().setItem(e.getSlot(), helmet);
                    if(e.getSlot()==6)
                        e.getClickedInventory().setItem(e.getSlot(), chestplate);
                    if(e.getSlot()==7)
                        e.getClickedInventory().setItem(e.getSlot(), leggings);
                    if(e.getSlot()==8)
                        e.getClickedInventory().setItem(e.getSlot(), boots);
                }
            }
        }).runTaskLater(Core.getAPI(), 4L);
        if(e.getSlot()>8)
            update(true);
    }
    public void onDrag(InventoryDragEvent e) {
        (new BukkitRunnable() {
            public void run() {
                if(inv2.getViewers().size() > 0) {
                    if(e.getView().getTitle().equalsIgnoreCase(inv2.getName()))
                        update(true);
                    if(e.getView().getBottomInventory() instanceof PlayerInventory)
                        update(false);
                }
            }
        }).runTaskLater(Core.getAPI(), 10);
    }
    
    public void onSwap(PlayerSwapHandItemsEvent e) {
        if(inv2.getViewers().size()>0)
            update(false);
    }
    
    public void onScroll(PlayerItemHeldEvent e) {
        if(inv2.getViewers().size()>0)
            update(false);
    }
    
    private void update(boolean reverse) {
        if(!reverse) {
            for(int i = 0; i < 9; i++) {
                inv2.setItem(36+i, inv.getItem(i));
            }
            for(int i = 9; i < 36; i++) {
                inv2.setItem(i, inv.getItem(i));
            }
            //inv2.setItem(1, ItemBuilder.createItemStack());
            (new BukkitRunnable() {
                @Override
                public void run() {
                    for(int i = 0; i < 9; i++) {
                        inv2.setItem(36+i, inv.getItem(i));
                    }
                    for(int i = 9; i < 36; i++) {
                        inv2.setItem((i), inv.getItem(i));
                    }
                }
            }).runTaskLater(Core.getAPI(), 5L);
            setArmor();
            (new BukkitRunnable() {
                @Override
                public void run() {
                    for(Player p : inv2.getViewersAsPlayers())p.updateInventory();
                }
            }).runTaskLater(Core.getAPI(), 6L);
        }else {
            for(int i = 0; i < 9; i++) {
                inv.setItem(i, inv2.getItem(i+36));
            }
            for(int i = 9; i < 36; i++) {
                inv.setItem(i, inv2.getItem(i));
            }
            (new BukkitRunnable() {
                @Override
                public void run() {
                    for(int i = 0; i < 9; i++) {
                        inv.setItem(i, inv2.getItem(i+36));
                    }
                    for(int i = 9; i < 36; i++) {
                        inv.setItem(i, inv2.getItem(i));
                    }
                }
            }).runTaskLater(Core.getAPI(), 5L);
            setArmor();
            (new BukkitRunnable() {
                @Override
                public void run() {
                    for(HumanEntity p : inv.getViewers())((Player)p).updateInventory();
                }
            }).runTaskLater(Core.getAPI(), 6L);
        }
    }
    
    private void setArmor() {
        int index = 0;
        if(Version.isVersionHigherThan(true, Versions.v1_9)) {
            if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
               || (inv.getItemInOffHand() == null || inv.getItemInMainHand().getType().name().contains("AIR"))) {
                inv2.setItem(index, mainhand);
            } else inv2.setItem(index, inv.getItemInMainHand());
            index = 1;
            if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
               || (inv.getItemInOffHand() == null || inv.getItemInOffHand().getType().name().contains("AIR"))) {
                inv2.setItem(index, offhand);
            } else inv2.setItem(index, inv.getItemInOffHand());
        }else {
            if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
               || (inv.getItemInHand() == null || inv.getItemInHand().getType().name().contains("AIR"))) {
                inv2.setItem(index, mainhand);
            } else inv2.setItem(index, inv.getItemInHand());
        }
        
        index = 5;
        if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
           || (inv.getHelmet() == null || inv.getHelmet().getType().name().contains("AIR"))) {
            inv2.setItem(index, helmet);
        }else inv2.setItem(index, inv.getHelmet());
        index = 6;
        if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
           || (inv.getChestplate() == null || inv.getChestplate().getType().name().contains("AIR"))) {
            inv2.setItem(index, chestplate);
        }else inv2.setItem(index, inv.getChestplate());
        index = 7;
        if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
           || (inv.getLeggings() == null || inv.getLeggings().getType().name().contains("AIR"))) {
            inv2.setItem(index, leggings);
        }else inv2.setItem(index, inv.getLeggings());
        index = 8;
        if((inv2.getItem(index) == null || inv2.getItem(index).getType().name().contains("AIR"))
           || (inv.getBoots() == null || inv.getBoots().getType().name().contains("AIR"))) {
            inv2.setItem(index, boots);
        }else inv2.setItem(index, inv.getBoots());
        (new BukkitRunnable() {
            @Override
            public void run() {
                for(HumanEntity p : inv2.getViewers())((Player)p).updateInventory();
            }
        }).runTaskLater(Core.getAPI(), 6L);
        if(!Version.isVersionHigherThan(true, Versions.v1_9)) {
            inv2.setItem(1, ItemBuilder.createGlass(ItemBuilder.ColorType.LIGHT_GRAY, true).setName("").buildItem());
        }
        inv2.setItem(2, ItemBuilder.createGlass(ItemBuilder.ColorType.LIGHT_GRAY, true).setName("").buildItem());
        inv2.setItem(3, ItemBuilder.createGlass(ItemBuilder.ColorType.LIGHT_GRAY, true).setName("").buildItem());
        inv2.setItem(4, ItemBuilder.createGlass(ItemBuilder.ColorType.LIGHT_GRAY, true).setName("").buildItem());
    }
    
    public PlayerInventory getInv() {
        return inv;
    }
    
}
