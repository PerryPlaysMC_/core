package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.io.File;

@SuppressWarnings("all")
public class BaseCommandSource implements CommandSource {
    
    private CommandSender s;
    private Config cfg;
    private CommandSource replier;
    
    public BaseCommandSource(CommandSender s) {
        this.s = s;
        if(!(s instanceof Player)) {
            File f = new File(Config.defaultDirectory, "userData");
            this.cfg = new Config(f, "!Console.yml");
        }else cfg = getUser().getConfig();
    }
    
    
    @Override
    public CommandSender getBase() {
        return s;
    }
    
    @Override
    public String getName() {
        return isPlayer() ? getUser().getName() : "Console" ;
    }
    
    @Override
    public String getRealName() {
        return isNicked() ? getNickName() : s.getName();
    }
    
    @Override
    public boolean isPlayer() {
        return s instanceof Player;
    }
    
    @Override
    public boolean hasPermission(String... permissions) {
        String cmdPrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.command");
        String featurePrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.feature");
        String pre = CoreAPI.getSettings().getString("settings.main-name").toLowerCase();
        for(String perm : permissions) {
            if ((s.hasPermission("*")) ||
                (s.hasPermission(perm + ".*")) ||
                (s.hasPermission(cmdPrefix+"." + perm + ".*")) ||
                (s.hasPermission(cmdPrefix+"." + perm + ".*")) ||
                (s.hasPermission(pre+".*")) ||
                (s.hasPermission(perm)) ||
                (s.hasPermission(pre+"." + perm)) ||
                (s.hasPermission(featurePrefix+"." + perm)) ||
                (s.hasPermission(cmdPrefix+"." + perm))||s.isOp()) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean hasPermission(Permission... permissions) {
        for(Permission perm : permissions) {
            if(hasPermission(perm.getName()))return true;
        }
        return false;
    }
    
    @Override
    public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        if(isPlayer()) {
            getUser().sendTitle(title, subtitle, fadeIn, stay, fadeOut);
        }
    }
    
    @Override
    public void sendTitle(String title) {
        if(isPlayer()) {
            getUser().sendTitle(title);
        }
    }
    
    @Override
    public void sendAction(String message) {
        if(isPlayer()) {
            getUser().sendAction(message);
        }
        
    }
    
    @Override
    public void sendMessage(Object... messages) {
        for(Object msg : messages) {
            s.sendMessage(StringUtils.translate(msg.toString()));
        }
    }
    
    @Override
    public void sendMessage(Message... messages) {
        for(Message msg : messages) {
            msg.send(this);
        }
    }
    
    @Override
    public void sendMessageFormat(String location, Object... objects) {
        sendMessage(CoreAPI.format(location, objects));
    }
    
    @Override
    public void sendMessageFormat(Config cfg, String location, Object... objects) {
        sendMessage(CoreAPI.format(cfg, location, objects));
    }
    
    @Override
    public User getUser() {
        return CoreAPI.getUser(((Player)s).getUniqueId());
    }
    
    @Override
    public boolean isVanished() {
        return cfg.getBoolean("Settings.isVanished");
    }
    
    @Override
    public void setVanished(boolean isVanished) {
        cfg.set("Settings.isVanished", isVanished);
        if(!isPlayer())return;
        Player base = getUser().getBase();
        if(isVanished) {
            for(Player p : Bukkit.getOnlinePlayers()) {
                p.hidePlayer(Core.getAPI(), base);
            }
            base.setPlayerListName(ColorUtil.translateChatColor("&7Vanished &c- " + getName()));
        }else {
            for(Player p : Bukkit.getOnlinePlayers()) {
                p.showPlayer(Core.getAPI(), base);
            }
            base.setPlayerListName(getName());
        }
    }
    
    @Override
    public boolean isNicked() {
        return cfg.isSet("settings.NickName.name") && cfg.getBoolean("settings.NickName.isEnabled");
    }
    
    @Override
    public void setNick(String nick) {
        if(!isPlayer()) return;
        Config cfg = getUser().getConfig();
        cfg.set("settings.NickName.name", nick);
    }
    
    @Override
    public void toggleNick() {
        cfg.set("settings.NickName.isEnabled", !nicked());
    }
    
    @Override
    public boolean nicked() {
        return cfg.getBoolean("settings.NickName.isEnabled");
    }
    
    @Override
    public String getNickName() {
        return isNicked() ? cfg.getString("settings.NickName.name") : s.getName();
    }
    
    @Override
    public boolean isCommandSpyEnabled() {
        return cfg.getBoolean("settings.CommandSpy");
    }
    
    @Override
    public void toggleCommandSpy() {
        setCommandSpy(!isCommandSpyEnabled());
    }
    
    @Override
    public void setCommandSpy(boolean enabled) {
        cfg.set("settings.CommandSpy", enabled);
    }
    
    
    @Override
    public CommandSource getReplier() {
        return replier;
    }
    
    @Override
    public void setReplier(CommandSource s) {
        replier = s;
    }
}
