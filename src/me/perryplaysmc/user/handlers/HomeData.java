package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/5/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class HomeData {
    
    Config cfg;
    User u;
    
    public HomeData(OfflineUser u) {
        cfg = u.getConfig();
    }
    
    public HomeData(User u) {
        cfg = u.getConfig();
        this.u = u;
    }
    
    
    public List<Home> getHomes() {
        List<Home> homes = new ArrayList<>();
        if(cfg.isSet("Homes"))
            for (String s : cfg.getSection("Homes").getKeys(false)) {
                homes.add(new Home(s, cfg.getLocation("Homes." + s + ".location")));
            }
        return homes;
    }
    
    public Home setHome(String name) {
        cfg.set("Homes." + name+".location", u.getLocation());
        return new Home(name, u.getLocation());
    }
    
    public Home setHome(String name, Location loc) {
        cfg.set("Homes." + name+".location", loc);
        return new Home(name, loc);
    }
    
    
    public void delHome(String name) {
        cfg.set("Homes." + name, null);
    }
    
    public Home getHome(String name) {
        for (Home h : getHomes()) {
            if(h.getName().equalsIgnoreCase(name)) return h;
        }
        return null;
    }
    
    
}