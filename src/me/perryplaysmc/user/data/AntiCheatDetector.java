package me.perryplaysmc.user.data;

import me.perryplaysmc.user.User;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.user.data
 * Path: me.perryplaysmc.user.data.AntiCheat
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class AntiCheatDetector {
    
    private User u;
    private Long foodStart, bowStart;
    private int invalidFood=0;
    private Location foodStartLocation, bowStartLocation;
    private double speed;
    private boolean ignoreSpeed;
    
    public boolean isCancelMoveCheck() {
        return cancelMoveCheck;
    }
    
    
    public void toggleMoveCheck() {
        cancelMoveCheck = true;
    }
    
    private boolean cancelMoveCheck;
    private List<Material> DIAMOND = new ArrayList<>();
    
    public AntiCheatDetector(User u) {
        this.u = u;
    }
    
    public void setSpeed(double d) {
        this.speed = d;
    }
    
    public double speed() {
        return speed;
    }
    
    public void setIgnoreSpeed(boolean ignore) {
        ignoreSpeed = ignore;
    }
    
    public boolean isIgnoreSpeed() {
        return ignoreSpeed;
    }
    
    
    //Bow
    
    public Long getBowStart() {
        return bowStart;
    }
    
    public boolean hasBowStarted() {
        return bowStart != null;
    }
    
    public void startBow() {
        this.bowStart = System.currentTimeMillis();
    }
    
    public void stopBow() {
        this.bowStart = null;
    }
    
    
    
    //Food
    
    public void startFood() {
        foodStart = System.currentTimeMillis();
        foodStartLocation = u.getLocation();
    }
    
    
    public Long getFood() {
        return foodStart;
    }
    
    public boolean hasFoodStarted() {
        return foodStart!=null;
    }
    
    public void addInvalidFood() {
        invalidFood++;
    }
    
    public int getInvalidFood() {
        return invalidFood;
    }
    
    
    public Location getFoodStartLocation() {
        return foodStartLocation;
    }
    
    
    public void resetFoodCount() {
        invalidFood = 0;
    }
}
