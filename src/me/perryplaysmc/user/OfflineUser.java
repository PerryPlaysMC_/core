package me.perryplaysmc.user;

import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.handlers.*;
import me.perryplaysmc.user.instances.Vanishable;
import me.perryplaysmc.permissions.PermissionUserData;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.ServerOperator;

import java.util.UUID;

public interface OfflineUser extends Vanishable, ServerOperator {


    OfflinePlayer getBase();

    Player getAsPlayer();

    String getName();
    
    String getRealName();

    Config getConfig();
    
    ChatSettings getChatSettings();

    boolean isOnline();

    UUID getUniqueId();

    BankAccount getAccount();
    
    HomeData getHomeData();
    
    ShopHandler getShopHandler();
    
    RankUtils getRankUtils();

    PermissionUserData getPermissionData();

    PlayerInventory getInventory();

}
