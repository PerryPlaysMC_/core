package me.perryplaysmc.user.instances;

public interface Vanishable {
    
    boolean isVanished();
    
    void setVanished(boolean isVanished);
}
