package me.perryplaysmc.prison.ah;

import me.perryplaysmc.core.configuration.Config;

import java.util.*;

public class AuctionHouseManager {

    private static AuctionHouseManager inst = new AuctionHouseManager();
    private static Set<AuctionHouse> ahs = new HashSet<>();
    private static HashMap<String, Config> configs = new HashMap<>();
    private static List<String> groups = new ArrayList<>();

    public static AuctionHouse getAH(String group) {
        for(AuctionHouse ah : ahs) {
            if(ah.getGroup().equals(group.toLowerCase()))
                return ah;
        }
        return null;
    }
    
    

    public static List<String> getGroups() {
        for(AuctionHouse ah : ahs) {
            if(groups.contains(ah.getGroup().toLowerCase()))continue;
            groups.add(ah.getGroup().toLowerCase());
        }
        return groups;
    }

}
