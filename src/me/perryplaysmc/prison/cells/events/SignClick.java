package me.perryplaysmc.prison.cells.events;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.prison.cells.PrisonCell;
import me.perryplaysmc.prison.cells.PrisonCellManager;
import me.perryplaysmc.user.User;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/4/19-2023
 * Package: me.perryplaysmc.prison.cells.events
 * Path: me.perryplaysmc.prison.cells.events.SignClick
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class SignClick implements Listener {

    private List<String> users = new ArrayList<>();

    @EventHandler
    void onClick(PlayerInteractEvent e){
        User u = CoreAPI.getUser(e.getPlayer());
        if(users.contains(e.getPlayer().getName()))return;
        else users.add(e.getPlayer().getName());
        (new BukkitRunnable(){public void run(){users.remove(e.getPlayer().getName());}}).runTaskLater(Core.getAPI(),5);
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if(!(e.getClickedBlock().getState() instanceof Sign))return;
        Sign s = (Sign)e.getClickedBlock().getState();
        PrisonCell cell = PrisonCellManager.getCell(s);
        if(cell != null) {
            if(cell.getOwner()!=null&&u.getUniqueId().toString().equalsIgnoreCase(cell.getOwner().toString())
            || e.getPlayer().getName().equalsIgnoreCase(cell.getOwnerName())) {
                if(!u.getAccount().hasEnough(cell.getRenewPrice())) {
                    u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.notEnoughMoneyRenew",
                            cell.getRenewPrice(), cell.getGroup().toUpperCase());
                    return;
                }
                u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.renew",
                        cell.getRenewPrice(), cell.getGroup().toUpperCase());
                cell.rent(u.getBase());
                u.getAccount().removeBalance(cell.getRenewPrice());
                cell.recalculatePrice(true);
                return;
            }
            if(cell.getOwner()!=null) {
                u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.alreadyRented", cell.getGroup().toUpperCase());
                return;
            }
            if(!u.hasPermission("cells.cellblock." + cell.getGroup())) {
                u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.noPerm", cell.getGroup().toUpperCase());
                return;
            }
            if(PrisonCellManager.getCell(e.getPlayer().getUniqueId())!=null&&
                    PrisonCellManager.getCell(e.getPlayer().getUniqueId()).getGroup().equalsIgnoreCase(cell.getGroup())) {
                u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.alreadyHas", cell.getGroup().toUpperCase());
                return;
            }
            if(!u.getAccount().hasEnough(cell.getBuyCost())) {
                u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.notEnoughMoneyBuy", cell.getBuyCost(), cell.getGroup().toUpperCase());
                return;
            }
            cell.rent(e.getPlayer());
            cell.recalculatePrice(true);
            u.sendMessageFormat(CoreAPI.getMessages(),"Commands.Cells.SignClick.buy",
                    cell.getBuyCost(), cell.getGroup().toUpperCase());
            u.getAccount().removeBalance(cell.getBuyCost());
        }
    }
    

}
