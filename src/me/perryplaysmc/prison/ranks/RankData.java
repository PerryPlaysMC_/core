package me.perryplaysmc.prison.ranks;

import me.perryplaysmc.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;

import java.util.Arrays;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/8/19-2023
 * Package: me.perryplaysmc.prison.ranks
 * Path: me.perryplaysmc.prison.ranks.RankData
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class RankData {
    
    
    private String rankName, toRank;
    private double cost;
    private String[] commands;
    private int index;
    private boolean isLastRank;
    
    
    public RankData(String rankName, String toRank, int index, double cost, String... commands) {
        this.rankName = rankName;
        this.toRank = toRank;
        this.index = index;
        this.cost = cost;
        this.commands = commands;
    }
    
    public int getIndex() {
        return index;
    }
    
    public boolean isLastRank() {
        return isLastRank;
    }
    
    
    public String getRank() {
        return rankName;
    }
    
    public String getNextRank() {
        return toRank;
    }
    
    public void setNextRank(String nextRank) {
        this.toRank = nextRank;
    }
    
    public double cost() {
        return cost;
    }
    
    public void runCommands() {
        for(String s : commands) {
            String[] sp = s.split(" ");
            String command = sp[0].substring(1);
            String[] args = Arrays.copyOfRange(sp, 1, sp.length);
            Command cmd = Core.getAPI().getCommandMap().getCommand(command);
            if(cmd != null) {
                cmd.execute(Bukkit.getConsoleSender(), command, args);
            }
        }
    }
    
}
