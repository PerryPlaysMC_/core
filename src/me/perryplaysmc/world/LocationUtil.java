package me.perryplaysmc.world;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.text.DecimalFormat;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/2/19-2023
 * Package: me.perryplaysmc.world
 * Path: me.perryplaysmc.world.LocationUtil
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class LocationUtil {
    
    private static String s = "/";
    
    
    public static boolean isSimilar(Location loc1, Location loc2) {
        if(loc1 == null || loc2 == null){
            System.out.println("Locatons null: " + (loc1==null) + " " + (loc2==null));
            return false;
        }
        return loc1.getWorld().getName().equalsIgnoreCase(loc2.getWorld().getName()) &&
                loc1.getBlockX() == loc2.getBlockX() &&
                loc1.getBlockY() == loc2.getBlockY()&&
                loc1.getBlockZ() == loc2.getBlockZ();
    }
    
    public static String locationToString(Location l) {
        DecimalFormat f = new DecimalFormat("###.###");
        return l.getWorld().getName() + s + f.format(l.getX()) + s + f.format(l.getY()) + s + f.format(l.getZ()) + s + l.getYaw() + s + l.getPitch(); }
    
    public static Location stringToLocation(String l) {
        if (!l.contains(s)) return null;
        if(l.split(s).length != 6) return null;
        String[] x = l.split(s);
        return new Location(Bukkit.getWorld(x[0]),
                Double.parseDouble(x[1]),
                Double.parseDouble(x[2]),
                Double.parseDouble(x[3]),
                Float.parseFloat(x[4]),
                Float.parseFloat(x[5]));
    }
    
    
    
}
