package me.perryplaysmc.world;

import me.perryplaysmc.prison.cells.Region;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.world
 * Path: me.perryplaysmc.world.BlockHelper
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class BlockHelper {
    
    
    
    
    public Set<Block> getConnectedBlocks(int max, Block start, List<Material> allowedMaterials) {
        return getConnectedBlocks(max, start, allowedMaterials, new HashSet<>());
    }
    
    private Set<Block> getConnectedBlocks(int max, Block start, List<Material> allowedMaterials, Set<Block> blocks) {
        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {
                for (int z = -1; z < 2; z++) {
                    if ((blocks.size() >= max) && (max != -1)) {
                        return blocks;
                    }
                    Block block = start.getLocation().clone().add(x, y, z).getBlock();
                    if ((!blocks.contains(block)) && (allowedMaterials.size() == 0 ? true : allowedMaterials.contains(block.getType())) && (block.getType() == start.getType())) {
                        blocks.add(block);
                        if ((blocks.size() >= max) && (max != -1)) {
                            return blocks;
                        }
                        try {
                            String a = getConnectedBlocks(max, block, allowedMaterials, blocks).size() + "/" + blocks.size();
                            blocks.addAll(getConnectedBlocks(max, block, allowedMaterials, blocks));
                        }
                        catch (Exception localException) {}
                    }
                }
            }
        }
        return blocks;
    }
    List<Block> ignore = new ArrayList<>();
    
    public List<Block> getIgnore() {
        return ignore;
    }
    
    public Set<Block> getConnectedBlocks(int max, Region region, Block start) {
        ignore = new ArrayList<>();
        return getConnectedBlocks(max, start, region, new HashSet<>());
    }
    
    private Set<Block> getConnectedBlocks(int max, Block start, Region region, Set<Block> blocks) {
        Y:for (int y = -1; y < 2; y++) {
            X:for (int x = -1; x < 2; x++) {
                Z:for (int z = -1; z < 2; z++) {
                    if ((blocks.size() >= max) && (max != -1)) {
                        return blocks;
                    }
                    int trueX = start.getLocation().getBlockX()+x;
                    int trueZ = start.getLocation().getBlockX()+z;
                    Block block = start.getLocation().clone().add(x, y, z).getBlock();
                    if ((!blocks.contains(block)) && (region.isWithinBoarder(block.getLocation()))) {
                        if(block.getType() == Material.BARRIER || (blocks.size()>0 && blocks.toArray(new Block[blocks.size()])[blocks.size()-1].getType()==Material.BARRIER)) {
                            if(!ignore.contains(block))
                                ignore.add(block);
                            continue;
                        }
                        blocks.add(block);
                        if ((blocks.size() >= max) && (max != -1)) {
                            return blocks;
                        }
                        try {
                            String a = getConnectedBlocks(max, block, region, blocks).size() + "/" + blocks.size();
                            blocks.addAll(getConnectedBlocks(max, block, region, blocks));
                        }
                        catch (Exception localException) {}
                    }
                }
            }
        }
        return blocks;
    }
    
}
