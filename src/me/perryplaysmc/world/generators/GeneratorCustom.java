package me.perryplaysmc.world.generators;

import me.perryplaysmc.world.generators.biome.BiomeGenerator;
import me.perryplaysmc.world.generators.biome.Biomes;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.*;

public class GeneratorCustom extends ChunkGenerator {
    private static final int SEA_LEVEL = 63;
    private static final int MAGNITUDE = 32;//how much we multiply the value between -1 and 1. It will determine how "steep" the hills will be.
    private static final int OVERHANGS_MAGNITUDE = 16; //used when we generate the noise for the tops of the overhangs
    private static final int MIDDLE = 70; // the "middle" of the road
    private static final double SCALE = 32.0; //how far apart the tops of the hills are
    private static final double THRESHOLD = 10.0; // the cutoff point for terrain
    private static int CHUNKS = 0;
    private static int CHANGES = 0;
    private static int LAST_MAXHEIGHT = 0;
    
    @Override
    public ChunkData generateChunkData(World world, Random rand, int chunkX, int chunkZ, BiomeGrid biomeGrid) {
        ChunkData chunk = createChunkData(world);
        Biomes.setWorld(world);
        BiomeGenerator biomeGenerator = new BiomeGenerator(world);
    
        for (int x=0; x<16; x++) {
            for (int z=0; z<16; z++) {
                int realX = x + chunkX * 16;
                int realZ = z + chunkZ * 16;
            
                //We get the 3 closest biome's to the temperature and rainfall at this block
                HashMap<Biomes, Double> biomes = biomeGenerator.getBiomes(realX, realZ);
                //And tell bukkit (who tells the client) what the biggest biome here is
                biomeGrid.setBiome(x, z, getDominantBiome(biomes));
            
                // for illustrative purposes, we colour the biomes differently
               List<Material> material = getBiomeMaterials(biomeGrid.getBiome(x, z));
            
                // To make it more maintainable, we've abstracted finding height
                // and density values
                int bottomHeight = getHeight(realX, realZ, biomes);
            
                // This has been lowered to 10 to avoid massive performance issues.
                // We take (10 vertical blocks * 3 (closest) biomes * 16 columns)
                // noise values per chunk! In the next tutorial, I will show you how
                // to use interpolation to "guess" the values between, so that you
                // can avoid expensive calls to the noise generator
                int maxHeight = bottomHeight + 10;
                double threshold = 0.3;
            
                for (int y=0; y<maxHeight; y++) {
                    if (y > bottomHeight) {
                        double density = getDensity(realX, y, realZ, biomes);
                    
                        if (density > threshold) setBlocks(x,y,z,chunk,material);
                    
                    } else {
                        setBlocks(x,y,z,chunk,material);
                    }
                }
            
                setBlock(x,bottomHeight,z,chunk,getBiomeMaterial(biomeGrid.getBiome(x, z)));
            
                for (int y=bottomHeight + 1; y>bottomHeight && y < maxHeight; y++ ) {
                    Material thisblock = getBlock(x, y, z, chunk);
                    Material blockabove = getBlock(x, y+1, z, chunk);
                
                    if(thisblock != Material.AIR && blockabove == Material.AIR) {
                        setBlocks(x, y, z, chunk, material);
                        if(getBlock(x, y-1, z, chunk) != Material.AIR)
                            setBlocks(x, y-1, z, chunk, material);
                        if(getBlock(x, y-2, z, chunk) != Material.AIR)
                            setBlocks(x, y-2, z, chunk, material);
                    }
                }
            
            }
        }
        return chunk;
    }
    
    void setBlocks(int x, int y, int z, ChunkData c, List<Material> mats) {
        for(int i = 0; i < mats.size(); i++) {
            c.setBlock(x, y+-i, z, mats.get(i));
        }
    }
    
    Material getBlock(int x, int y, int z, ChunkData c) {
        return c.getBlockData(x,y,z).getMaterial();
    }
    
    //This would normaly be in an enum, but it's only so you can see biome lines
    private Material getBiomeMaterial(Biome biome) {
        switch (biome) {
            case DESERT: return Material.SANDSTONE;
            case FOREST: return Material.GRASS_BLOCK;
            case PLAINS: return Material.DIRT;
            case SWAMP: return Material.MYCELIUM;
            case MOUNTAINS: return Material.STONE;
            case RIVER: return Material.WATER;
            default: return Material.GLASS;
        }
    }
    
    private List<Material> getBiomeMaterials(Biome biome) {
        switch (biome) {
            case DESERT: return Arrays.asList(Material.SAND, Material.SAND, Material.SAND, Material.SANDSTONE);
            case FOREST: return Arrays.asList(Material.GRASS_BLOCK, Material.DIRT, Material.DIRT, Material.DIRT);
            case PLAINS: return Arrays.asList(Material.GRASS_BLOCK, Material.DIRT, Material.DIRT, Material.DIRT);
            case SWAMP: return Arrays.asList(Material.MYCELIUM);
            case MOUNTAINS: return Arrays.asList(Material.STONE);
            case RIVER: return Arrays.asList(Material.WATER, Material.WATER, Material.DIRT, Material.DIRT, Material.DIRT, Material.STONE);
            default: return Arrays.asList(Material.GLASS);
        }
    }
    
    //We get the closest biome to send to the client (using the biomegrid)
    private Biome getDominantBiome(HashMap<Biomes, Double> biomes) {
        double maxNoiz = 0.0;
        Biomes maxBiome = null;
        
        for (Biomes biome : biomes.keySet()) {
            if (biomes.get(biome) >= maxNoiz) {
                maxNoiz = biomes.get(biome);
                maxBiome = biome;
            }
        }
        return maxBiome.biome;
    }
    
    private double getDensity(int x, int y, int z, HashMap<Biomes, Double> biomes) {
        double noise = 0.0;
        for (Biomes biome : biomes.keySet()) {
            double weight = biomes.get(biome);
            noise += biome.generator.get3dNoise(x, y, z)*weight;
        }
        return noise;
    }
    
    private int getHeight(int x, int z, HashMap<Biomes, Double> biomes) {
        double noise = 0.0;
        for (Biomes biome : biomes.keySet()) {
            double weight = biomes.get(biome);
            noise += biome.generator.get2dNoise(x, z)*weight;
        }
        return (int) (noise + 64);
    }
    
    void setBlock(ChunkData c, int x, int y, int z, Material mat) {
        c.setBlock(x, y, z, mat);
    }
    
    void setBlock(int x, int y, int z, ChunkData c, Material mat) {
        setBlock(c, x, y, z, mat);
    }
    
    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }
    
    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        List<BlockPopulator> populators = new ArrayList<>();
        return populators;
    }
}
