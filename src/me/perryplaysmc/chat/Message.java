package me.perryplaysmc.chat;

import me.perryplaysmc.chat.json.JsonRepresentedObject;
import me.perryplaysmc.chat.json.command.JsonHandler;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.ItemUtil;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.chat.json.FancyMessage;
import me.perryplaysmc.chat.json.JsonString;
import me.perryplaysmc.chat.json.command.Run;
import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class Message {
    
    private FancyMessage base;
    public String last = "";
    public String original = "";
    private int latestSize;
    private CommandSource s;
    private ChatColor[] styles;
    List<FancyMessage> toSend;
    
    public Message(FancyMessage msg) {
        this.base = msg;
    }
    
    public Message() {
        this("");
    }
    
    public Message(String text) {
        this(null, text);
    }
    public Message(CommandSource u, String text) {
        this.toSend = new ArrayList<>();
        base = new FancyMessage("");
        then(u, StringUtils.translate(text));
        this.s = u;
    }
    
    public Message clone() {
        return new Message(base);
    }
    
    public Message then(String txtt) {
        return then(null, txtt, TextOptions.KEEP_COLORS);
    }
    
    public Message then(String txtt, TextOptions options) {
        return then(null, txtt, options);
    }
    
    public Message then(String perm, String txtt) {
        if(s!=null) {
            if(s.hasPermission(perm)) return then(txtt);
            else {
                return then("");
            }
        }
        return then(txtt);
    }
    
    public int size() {
        return base.getParts().size();
    }
    
    public Message then(boolean doLink, String txtt) {
        return then(doLink, null, txtt, TextOptions.KEEP_COLORS);
    }
    
    public Message breakLine() {
        return then(null, "\n", TextOptions.KEEP_COLORS);
    }
    
    public Message br() {
        return breakLine();
    }
    
    
    public Message then(boolean doLink, CommandSource u, String txtt) {
        return then(doLink, u, txtt, TextOptions.KEEP_COLORS);
    }
    
    public Message then(CommandSource u, String txtt) {
        return then(u, txtt, TextOptions.KEEP_COLORS);
    }
    public Message then() {
        return then(null,"", TextOptions.KEEP_COLORS);
    }
    
    public Message then(CommandSource u, String txtt, TextOptions options) {
        return then(false, u, txtt, options);
    }
    public Message then(boolean doLink, CommandSource u, String txtt, TextOptions options) {
        return thenRaw(doLink, u != null && u.isPlayer() ? StringUtils.translateForUser(u.getUser(), original, txtt) : StringUtils.translate(txtt), options);
    }
    
    private FancyMessage thenX(String text) {
        if(!base.latest().hasText())
            return base.text(text);
        else
            return base.then(text);
    }
    
    private Message thenRaw(boolean doLink, String txtt, TextOptions options) {
        String[] split = txtt.split(" ");
        latestSize = 0;
        if(split.length == 0){
            if(!txtt.isEmpty())
                thenX(txtt);
            return this;
        }
        for(int i = 0; i < split.length; i++) {
            String msg = split[i];
            String m = ColorUtil.changeChatColorTo('&', msg).replace(original.replace("§", "&"), original.replace("&", "§"));
            String og = ColorUtil.removeColor(m);
            String x1 = og.startsWith("https://") | og.startsWith("http://") ? og : "https://" + og;
            if(x1.startsWith("https://") || x1.startsWith("http://")) {
                og = x1;
            } else if(!x1.startsWith("https://") && !x1.startsWith("http://")) {
                og = "https://" + x1;
            }
            setupText(last+msg, options);
            if(StringUtils.checkForDomain(og)&&doLink) {
                hover("[c]Click to go to: ", "[pc]" + og).link(og);
            }
            if(styles !=null&&styles.length>0)
                base.style(styles);
            if(i != (split.length+-1)) {
                thenX(" ");
                latestSize++;
            }
            latestSize++;
        }
        if(txtt.endsWith(" ")) {
            thenX(" ");
        }
        return this;
    }
    
    private void setupText(String message, TextOptions options) {
        char[] chars = message.toCharArray();
        String m = "";
        for(int i = 0; i < chars.length; i++) {
            char current = chars[i];
            char prev = i > 0 ? chars[i+-1] : '`';
            char next = i < chars.length+-1 ? chars[i+1] : '`';
            char next3 = i < chars.length+-3 ? chars[i+3] : '`';
            m+=current;
            if(current == '§') {
                if(!last.endsWith((""+current)+(""+next))) {
                    if(next=='r') {
                        last+=original;
                    }else {
                        last += ("" + current) + ("" + next);
                        if(ColorUtil.isColor(("" + current) + ("" + next)) || next == 'r') {
                            last = ("" + current) + ("" + next);
                        }
                    }
                }
            }
            if(i == chars.length+-1) {
                String nm=m;//reverse(m);
                if(last.isEmpty()) {
                    last=original;
                }
                ChatColor color = ColorUtil.locateChatColor(last);
                thenX(nm);
                if(color!=null)
                    if(color.isColor()) {
                        base.color(color);
                    }else if(color.isFormat())
                        base.style(color);
                m = "";
            }
        }
    }
    
    private String reverse(String x) {
        String ret = "";
        char[] chars = x.toCharArray();
        for(int i = chars.length+-1; i>-1;i--) {
            ret+=chars[i];
        }
        return ret;
    }
    
    private void set(String last, boolean addBold, boolean addStrikeThrough, boolean addUnderLine, boolean addItalic, boolean addMagic) {
        if(last.toLowerCase().contains("l"))
            addBold = true;
        if(last.toLowerCase().contains("m"))
            addStrikeThrough = true;
        if(last.toLowerCase().contains("n"))
            addUnderLine = true;
        if(last.toLowerCase().contains("o"))
            addItalic = true;
        if(last.toLowerCase().contains("k"))
            addMagic = true;
    }
    
    private void set(ChatColor c, boolean addBold, boolean addStrikeThrough, boolean addUnderLine, boolean addItalic, boolean addMagic) {
        if(c != null) base.color(c);
        if(addBold)base.style(ChatColor.BOLD);
        if(c != null) base.color(c);
        if(addStrikeThrough)base.style(ChatColor.STRIKETHROUGH);
        if(c != null) base.color(c);
        if(addUnderLine)base.style(ChatColor.UNDERLINE);
        if(c != null) base.color(c);
        if(addItalic)base.style(ChatColor.ITALIC);
        if(c != null) base.color(c);
        if(addMagic)base.style(ChatColor.MAGIC);
        if(c != null) base.color(c);
    }
    
    public Message command(String text) {
        if(!text.startsWith("/")) text = "/" + text;
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }
    
    public Message command(Run r, String... args) {
        String text = UUID.randomUUID().toString();
        while(JsonHandler.getCommand(text) != null) {
            text = UUID.randomUUID().toString();
        }
        JsonHandler.addCommand(text, r);
        String arguments = "";
        if(args.length > 0) {
            arguments = " ";
            for(int i = 0; i < args.length; i++) {
                arguments+=args[i]+" ";
            }
            arguments.substring(0, arguments.length()+-1);
        }
        text += arguments;
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }
    
    public Message forceChat(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }
    
    public Message suggest(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "suggest_command";
        }
        return this;
    }
    
    public Message insert(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).insertionData = ColorUtil.removeColor(text);
        }
        return this;
    }
    
    public Message link(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "open_url";
        }
        return this;
    }
    
    public Message tooltip(ItemStack item) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).hoverActionData = new JsonString(ItemUtil.convertItemStackToJson(item));
            base.getParts().get(i).hoverActionName = "show_item";
        }
        return this;
    }
    public Message hover(ItemStack item) {
        return tooltip(item);
    }
    
    public Message hover(String... text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            StringBuilder builder = new StringBuilder();
            for (int ii = 0; ii < text.length; ii++) {
                builder.append(text[ii]);
                if (ii != text.length - 1) {
                    builder.append('\n');
                }
            }
            base.getParts().get(i).hoverActionData = new JsonString(StringUtils.translate(builder.toString()));
            base.getParts().get(i).hoverActionName = "show_text";
        }
        return this;
    }
    
    public Message removeLinkData() {
        for(int i = 0; i < base.getParts().size(); i++) {
            if(base.getParts().get(i)!=null&&!StringUtils.isEmpty(base.getParts().get(i).clickActionName)&&base.getParts().get(i).clickActionName.equalsIgnoreCase("open_url")) {
                base.getParts().get(i).hoverActionData = null;
                base.getParts().get(i).hoverActionName = null;
                base.getParts().get(i).clickActionData = null;
                base.getParts().get(i).clickActionName = null;
            }
        }
        return this;
    }
    
    public String toString() {
        return base.toJSONString().replace("},", "},\n");
    }
    
    public Message tooltip(String... text) {
        return hover(text);
    }
    
    public Message tooltip(List<String> text) {
        return hover(text.toArray(new String[text.size()]));
    }
    
    public void send(User u){
        base.send(u);
    }
    
    public void send(OfflineUser u){
        base.send(u);
    }
    
    
    public void send(CommandSource u){
        base.send(u.getBase());
    }
    
    public void send(Player u) {
        base.send(u);
    }
    
    
    
}
