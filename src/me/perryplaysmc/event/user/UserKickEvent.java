package me.perryplaysmc.event.user;

import me.perryplaysmc.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UserKickEvent extends Event implements Cancellable {

  private static final HandlerList handlers = new HandlerList();
  
  private User user;
  private String msg;
  private boolean cancelled = false;
  
  public UserKickEvent(User user, String msg) {
    this.msg = msg;
  }
  
  public void setMessage(String newMessage) {
    msg = newMessage;
  }
  
  public String getMessage() {
    return msg;
  }
  
  public boolean isCancelled()
  {
    return cancelled;
  }
  
  public void setCancelled(boolean cancel)
  {
    cancelled = cancel;
  }
  
  public User getUser() {
    return user;
  }
  
  public Player getPlayer() {
    return user.getBase();
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public static HandlerList getHandlerList() {
    return handlers;
  }
}
