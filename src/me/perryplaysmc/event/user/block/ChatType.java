package me.perryplaysmc.event.user.block;

public enum ChatType {
    CHAT, NORMAL_CHAT, STAFF_CHAT, SIRI_CHAT, NULL;
}
