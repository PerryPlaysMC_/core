package me.perryplaysmc.event.user.block;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2023
 * Package: me.perryplaysmc.event.user
 * Path: me.perryplaysmc.event.user.move.Action
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/
public enum Action {
    INTERACT,
    RIGHT_CLICK_BLOCK,  LEFT_CLICK_BLOCK,
    RIGHT_CLICK_AIR,  LEFT_CLICK_AIR, NULL;

}