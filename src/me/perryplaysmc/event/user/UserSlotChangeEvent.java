//package net.miniverse.event.user;
//
//import User;
//import org.bukkit.event.Cancellable;
//import org.bukkit.event.Event;
//import org.bukkit.event.HandlerList;
//
//public class UserSlotChangeEvent extends Event implements Cancellable {
//
//  private static final HandlerList handlers = new HandlerList();
//  private User u;
//  private Integer newSlot;
//  private Integer prevSlot;
//  private Integer current;
//  private boolean isCancelled;
//
//  public UserSlotChangeEvent(User u, int newSlot, int prevSlot) { this.u = u;
//    this.prevSlot = prevSlot;
//    this.newSlot = newSlot;
//    current = Integer.valueOf(u.getInventory().getHeldItemSlot());
//    isCancelled = false;
//  }
//
//  public void setSlot(int slot) {
//   // u.getInventory().setHeldItemSlot(slot);
//    prevSlot = newSlot;
//    newSlot = Integer.valueOf(slot);
//  }
//
//  public int getSlot() {
//    return current == null ? u.getInventory().getHeldItemSlot() : current.intValue();
//  }
//
//  public int getPrevSlot() {
//    return prevSlot.intValue();
//  }
//
//  public int getNewSlot() {
//    return newSlot.intValue();
//  }
//
//  public User getUser() {
//    return u;
//  }
//
//  public org.bukkit.entity.Player getPlayer() {
//    return u.getBase();
//  }
//
//  public boolean isCancelled()
//  {
//    return isCancelled;
//  }
//
//  public void setCancelled(boolean cancel)
//  {
//    isCancelled = cancel;
//  }
//
//
//  public HandlerList getHandlers()
//  {
//    return handlers;
//  }
//
//  public static HandlerList getHandlerList() {
//    return handlers;
//  }
//}
